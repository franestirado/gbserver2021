package com.fran.gestb.aacommon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fran.gestb.aaexceptions.EmptyFieldException;
import com.fran.gestb.aaexceptions.EntityAlreadyExistsException;
import com.fran.gestb.dbbarc.model.GbViews;
import com.fran.gestb.dbbarc.repo.GbViewsRepository;

@Service
public class GbViewsCreator {
	@Autowired
	private GbViewsRepository gbViewsRepository;
	
	public GbViews[] createOneGbView(String gbViewName, String gbViewLang, ValueRecord[] gbViewNewTextList) {
		GbViews mainGbView [] = new GbViews[50];
		switch (gbViewName) {
        	case "gbviews": 	
        		mainGbView = createGbViewsGbView(gbViewLang,gbViewNewTextList);
            	break;	
        	case "gbfields":  	
        		mainGbView = createFieldsGbView(gbViewLang,gbViewNewTextList);
            	break;	
        	case "gbprovs":  
        		mainGbView = createProvsGbView(gbViewLang,gbViewNewTextList);
				break;	
            default: 
            	System.out.printf("->GbViewsCreator->createOneGbView->..Default...................\n");
                break;		
			}
		return mainGbView;
	}
	//---------------------------------PROVS GB VIEW--------------------------------------------------------
	@Transactional
	public GbViews[] createProvsGbView(String gbViewLang, ValueRecord[] gbViewNewTextList) throws EmptyFieldException, EntityAlreadyExistsException {
		System.out.printf("->GbViewsCreator->createProvsGbView->..Entering...................\n");
		GbViews provsGbView [] = new GbViews[28];
		if (gbViewLang == "APP") {
	//---------------------------------------gbViewName----objType---------objSet---objName-------objLang---objText--------------------
			provsGbView  [0] =  new GbViews(  "gbprovs",  "SCREEN",  "formScreen","screen"       ,  "APP","Providers");
			//-----------------------------------------------TITLES--------------------------------------------------------------------
			provsGbView  [1] =  new GbViews(  "gbprovs",   "TITLE",  "formTitles","provs"        ,  "APP","Providers");
			provsGbView  [2] =  new GbViews(  "gbprovs",   "TITLE",  "formTitles","provCreate"   ,  "APP","Create Provider");
			provsGbView  [3] =  new GbViews(  "gbprovs",   "TITLE",  "formTitles","provEdit"     ,  "APP","Modify Provider");
			provsGbView  [4] =  new GbViews(  "gbprovs",   "TITLE",  "formTitles","provMoreData" ,  "APP","Provider Detailed Data");
			provsGbView  [5] =  new GbViews(  "gbprovs",   "TITLE",  "formTitles","provsList"    ,  "APP","Providers List");
			provsGbView  [6] =  new GbViews(  "gbprovs",   "TITLE",  "formTitles","provTitle1"   ,  "APP","Providers Title 1");
			provsGbView  [7] =  new GbViews(  "gbprovs",   "TITLE",  "formTitles","provTitle2"   ,  "APP","Providers Title 2");
			//-----------------------------------------------LABELS--------------------------------------------------------------------
			provsGbView  [8] =  new GbViews(  "gbprovs",   "LABEL",  "formLabels","provId"       ,  "APP","Id");
			provsGbView  [9] =  new GbViews(  "gbprovs",   "LABEL",  "formLabels","provCifType"  ,  "APP","CIF Type");
			provsGbView [10] =  new GbViews(  "gbprovs",   "LABEL",  "formLabels","provCif"      ,  "APP","CIF");
			provsGbView [11] =  new GbViews(  "gbprovs",   "LABEL",  "formLabels","provName"     ,  "APP","Name");
			provsGbView [12] =  new GbViews(  "gbprovs",   "LABEL",  "formLabels","provLongName" ,  "APP","Long Name");
			provsGbView [13] =  new GbViews(  "gbprovs",   "LABEL",  "formLabels","provLabel1"   ,  "APP","Providers Label 1");
			provsGbView [14] =  new GbViews(  "gbprovs",   "LABEL",  "formLabels","provLabel2"   ,  "APP","Providers Label 2");
			//-----------------------------------------------VALIDATION MESSAGES-------------------------------------------------------
			provsGbView [15] =  new GbViews(  "gbprovs",   "ERROR", "provCifType","required"     ,  "APP","Type is required");
			provsGbView [16] =  new GbViews(  "gbprovs",   "ERROR",     "provCif","required"     ,  "APP","CIF is required");
			provsGbView [17] =  new GbViews(  "gbprovs",   "ERROR",     "provCif","minlength"    ,  "APP","CIF must be greater than 5 Characters");
			provsGbView [18] =  new GbViews(  "gbprovs",   "ERROR",     "provCif","maxlength"    ,  "APP","CIF must be less than 15 Characters");
			provsGbView [19] =  new GbViews(  "gbprovs",   "ERROR",     "provCif","invalidCIF"   ,  "APP","CIF must have format X99999999");
			provsGbView [20] =  new GbViews(  "gbprovs",   "ERROR",     "provCif","invalidNIF"   ,  "APP","NIF must have format 99999999X");
			provsGbView [21] =  new GbViews(  "gbprovs",   "ERROR",     "provCif","invalidNIE"   ,  "APP","NIE must have format X");
			provsGbView [22] =  new GbViews(  "gbprovs",   "ERROR",    "provName","required"     ,  "APP","Name is required");
			provsGbView [23] =  new GbViews(  "gbprovs",   "ERROR",    "provName","minlength"    ,  "APP","Name must be greater than 5 Characters");
			provsGbView [24] =  new GbViews(  "gbprovs",   "ERROR",    "provName","maxlength"    ,  "APP","Name must be less than 15 Characters");
			provsGbView [25] =  new GbViews(  "gbprovs",   "ERROR","provLongName","required"     ,  "APP","Long Name is required");
			provsGbView [26] =  new GbViews(  "gbprovs",   "ERROR","provLongName","minlength"    ,  "APP","Long Name must be greater than 5 Characters");
			provsGbView [27] =  new GbViews(  "gbprovs",   "ERROR","provLongName","maxlength"    ,  "APP","Long Name must be less than 15 Characters");	
		} else {
			provsGbView  [0] =  new GbViews(  "gbprovs",  "SCREEN",  "formScreen","screen"       ,  gbViewLang,gbViewNewTextList [0].getValue());
			provsGbView  [1] =  new GbViews(  "gbprovs",   "TITLE",  "formTitles","provs"        ,  gbViewLang,gbViewNewTextList [1].getValue());
			provsGbView  [2] =  new GbViews(  "gbprovs",   "TITLE",  "formTitles","provCreate"   ,  gbViewLang,gbViewNewTextList [2].getValue());
			provsGbView  [3] =  new GbViews(  "gbprovs",   "TITLE",  "formTitles","provEdit"     ,  gbViewLang,gbViewNewTextList [3].getValue());
			provsGbView  [4] =  new GbViews(  "gbprovs",   "TITLE",  "formTitles","provMoreData" ,  gbViewLang,gbViewNewTextList [4].getValue());
			provsGbView  [5] =  new GbViews(  "gbprovs",   "TITLE",  "formTitles","provList"     ,  gbViewLang,gbViewNewTextList [5].getValue());
			provsGbView  [6] =  new GbViews(  "gbprovs",   "TITLE",  "formTitles","provTitle1"   ,  gbViewLang,gbViewNewTextList [6].getValue());
			provsGbView  [7] =  new GbViews(  "gbprovs",   "TITLE",  "formTitles","provTitle2"   ,  gbViewLang,gbViewNewTextList [7].getValue());
			provsGbView  [8] =  new GbViews(  "gbprovs",   "LABEL",  "formLabels","provId"       ,  gbViewLang,gbViewNewTextList [8].getValue());
			provsGbView  [9] =  new GbViews(  "gbprovs",   "LABEL",  "formLabels","provCifType"  ,  gbViewLang,gbViewNewTextList [9].getValue());
			provsGbView [10] =  new GbViews(  "gbprovs",   "LABEL",  "formLabels","provCif"      ,  gbViewLang,gbViewNewTextList[10].getValue());
			provsGbView [11] =  new GbViews(  "gbprovs",   "LABEL",  "formLabels","provName"     ,  gbViewLang,gbViewNewTextList[11].getValue());
			provsGbView [12] =  new GbViews(  "gbprovs",   "LABEL",  "formLabels","provLongName" ,  gbViewLang,gbViewNewTextList[12].getValue());
			provsGbView [13] =  new GbViews(  "gbprovs",   "LABEL",  "formLabels","provLabel1"   ,  gbViewLang,gbViewNewTextList[13].getValue());
			provsGbView [14] =  new GbViews(  "gbprovs",   "LABEL",  "formLabels","provLabel2"   ,  gbViewLang,gbViewNewTextList[14].getValue());
			provsGbView [15] =  new GbViews(  "gbprovs",   "ERROR", "provCifType","required"     ,  gbViewLang,gbViewNewTextList[15].getValue());
			provsGbView [16] =  new GbViews(  "gbprovs",   "ERROR",     "provCif","required"     ,  gbViewLang,gbViewNewTextList[16].getValue());
			provsGbView [17] =  new GbViews(  "gbprovs",   "ERROR",     "provCif","minlength"    ,  gbViewLang,gbViewNewTextList[17].getValue());
			provsGbView [18] =  new GbViews(  "gbprovs",   "ERROR",     "provCif","maxlength"    ,  gbViewLang,gbViewNewTextList[18].getValue());
			provsGbView [19] =  new GbViews(  "gbprovs",   "ERROR",     "provCif","invalidCIF"   ,  gbViewLang,gbViewNewTextList[19].getValue());
			provsGbView [20] =  new GbViews(  "gbprovs",   "ERROR",     "provCif","invalidNIF"   ,  gbViewLang,gbViewNewTextList[20].getValue());
			provsGbView [21] =  new GbViews(  "gbprovs",   "ERROR",     "provCif","invalidNIE"   ,  gbViewLang,gbViewNewTextList[21].getValue());
			provsGbView [22] =  new GbViews(  "gbprovs",   "ERROR",    "provName","required"     ,  gbViewLang,gbViewNewTextList[22].getValue());
			provsGbView [23] =  new GbViews(  "gbprovs",   "ERROR",    "provName","minlength"    ,  gbViewLang,gbViewNewTextList[23].getValue());
			provsGbView [24] =  new GbViews(  "gbprovs",   "ERROR",    "provName","maxlength"    ,  gbViewLang,gbViewNewTextList[24].getValue());
			provsGbView [25] =  new GbViews(  "gbprovs",   "ERROR","provLongName","required"     ,  gbViewLang,gbViewNewTextList[25].getValue());
			provsGbView [26] =  new GbViews(  "gbprovs",   "ERROR","provLongName","minlength"    ,  gbViewLang,gbViewNewTextList[26].getValue());
			provsGbView [27] =  new GbViews(  "gbprovs",   "ERROR","provLongName","maxlength"    ,  gbViewLang,gbViewNewTextList[27].getValue());	
		}
		for (int i=0;i<provsGbView.length;i++){
			gbViewsRepository.save(provsGbView[i]);
		}
		return provsGbView;
	}
	//---------------------------------FIELDS AND VALUES GB VIEW--------------------------------------------------------
	@Transactional
	public GbViews[] createFieldsGbView(String gbViewLang, ValueRecord[] gbViewNewTextList) throws EmptyFieldException, EntityAlreadyExistsException {
		System.out.printf("->GbViewsCreator->createFieldsGbView->..Entering...................\\n\n");
		GbViews fieldsGbView [] = new GbViews[20];
		if (gbViewLang == "APP") {
			//-------------------------------gbViewName----objType---------objSet---objName-------objLang---objText--------------------
			fieldsGbView  [0] =  new GbViews( "gbfields",  "SCREEN",  "formScreen","screen"       ,  "APP","Fields");
			//-----------------------------------------------TITLES--------------------------------------------------------------------
			fieldsGbView  [1] =  new GbViews( "gbfields",   "TITLE",  "formTitles","fields"       ,  "APP","Fields");
			//-----------------------------------------------LABELS--------------------------------------------------------------------
			fieldsGbView  [8] =  new GbViews( "gbfields",   "LABEL",  "formLabels","fieldId"      ,  "APP","Id");
			//-----------------------------------------------VALIDATION MESSAGES-------------------------------------------------------
			fieldsGbView [15] =  new GbViews( "gbfields",   "ERROR", "provCifType","required"     ,  "APP","Type is required");
		} else {
			fieldsGbView  [0] =  new GbViews( "gbfields",  "SCREEN",  "formScreen","screen"       ,gbViewLang,gbViewNewTextList[0].getValue());

		}
		for (int i=0;i<fieldsGbView.length;i++){
			gbViewsRepository.save(fieldsGbView[i]);
		}
		return fieldsGbView;
	}
	//---------------------------------GB VIEWS GB VIEW--------------------------------------------------------
	@Transactional
	public GbViews[] createGbViewsGbView(String gbViewLang, ValueRecord[] gbViewNewTextList) throws EmptyFieldException, EntityAlreadyExistsException {
		System.out.printf("->GbViewsCreator->createGbViewsGbView->..Entering...................\\n");
		GbViews gbView [] = new GbViews[19];
		if (gbViewLang == "APP") {
			//-------------------------------gbViewName----objType---------objSet---objName-------objLang---objText--------------------
			gbView  [0] =  new GbViews(  "gbviews",  "SCREEN",  "formScreen","screen"       ,  "APP","GB Views");
			//-----------------------------------------------TITLES--------------------------------------------------------------------
			gbView  [1] =  new GbViews(  "gbviews",   "TITLE",  "formTitles","gbviews"      ,  "APP","GB Views");
			//-----------------------------------------------LABELS--------------------------------------------------------------------
			gbView  [8] =  new GbViews(  "gbviews",   "LABEL",  "formLabels","gbViewId"     ,  "APP","Id");
			//-----------------------------------------------VALIDATION MESSAGES-------------------------------------------------------
			gbView [15] =  new GbViews(  "gbviews",   "ERROR", "provCifType","required"     ,  "APP","Type is required");
		} else {
			gbView  [0] =  new GbViews( "gbfields",  "SCREEN",  "formScreen","screen"       ,gbViewLang,gbViewNewTextList[0].getValue());

		}

		for (int i=0;i<gbView.length;i++){
			gbViewsRepository.save(gbView[i]);
		}
		return gbView;
	}
}
