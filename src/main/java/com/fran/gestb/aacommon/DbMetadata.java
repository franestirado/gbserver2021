package com.fran.gestb.aacommon;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fran.gestb.dbbarc.model.Fields;
import com.fran.gestb.dbbarc.model.Values;
import com.fran.gestb.dbbarc.service.FieldsService;

@Service
public class DbMetadata {
	@Autowired
	private FieldsService fieldsService;

	public void getMetadata(String url, String database, String userName, String password) {
		Fields databaseField,newField, otherField;
		long databaseFieldId,tableFieldId;
		Values newValue,otherValue;
		String tableName, columnName;
		
        try (Connection connection = DriverManager.getConnection(url, userName, password)) {
        	
        	System.out.printf("---------->DbMetada....Connecte to DATABASE..."+database+"......\n");
        	
        	DatabaseMetaData metadata = connection.getMetaData();
			ResultSet resultSet = metadata.getTables(null, null, null, new String[]{"TABLE"});
			if (resultSet == null) {
				System.out.printf("...DbMetada....No TABLES retrieved..."+database+"......\n");
				return;
			}
			newField = new Fields();
			newField.setFieldName("databaseTables");
			newField.setFieldExplan("List of tables in database...."+database);
			databaseField=fieldsService.addOneField(newField);
			databaseFieldId = databaseField.getFieldId();
			
			System.out.printf("---------->DbMetada....FIELD created......."+newField.getFieldName()+"......\n");
			while(resultSet.next())
			{
			    //Print
			    tableName = resultSet.getString("TABLE_NAME");
			    newValue = new Values();
			    newValue.setFieldId(databaseFieldId);
			    newValue.setValueName(tableName);
			    otherValue = fieldsService.addOneValue(newValue);
			    System.out.printf("---------->DbMetada....VALUE created......."+newValue.getValueName()+"......\n");
			    
				ResultSet columns = metadata.getColumns(null,null, tableName, null);
				if (columns == null) {
					System.out.printf("...DbMetada....No COLUMNS retrieved..."+tableName+"......\n");
				} else {
					newField = new Fields();
					newField.setFieldName(tableName);
					newField.setFieldExplan("List of columns in table...."+tableName);
					otherField= fieldsService.addOneField(newField);
					tableFieldId = otherField.getFieldId();
					System.out.printf("---------->DbMetada....FIELD created......."+newField.getFieldName()+"......\n");	
					while(columns.next())
					{
					    columnName = columns.getString("COLUMN_NAME");
					    newValue = new Values();
					    newValue.setFieldId(tableFieldId);
					    newValue.setValueName(columnName);
					    otherValue = fieldsService.addOneValue(newValue);
					    System.out.printf("---------->DbMetada....VALUE created......."+otherValue.getValueName()+"......\n");
					}
					fieldsService.setValuesStringInField(otherField);
				}
			}
			fieldsService.setValuesStringInField(databaseField);
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
}
/*
String columnName = columns.getString("COLUMN_NAME");
String type = columns.getString("TYPE_NAME");
String datatype = columns.getString("DATA_TYPE");
String columnsize = columns.getString("COLUMN_SIZE");
String decimaldigits = columns.getString("DECIMAL_DIGITS");
String isNullable = columns.getString("IS_NULLABLE");
String is_autoIncrment = columns.getString("IS_AUTOINCREMENT");
*/
