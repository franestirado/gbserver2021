package com.fran.gestb.dbbarc.repo;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

import com.fran.gestb.dbbarc.model.GbViews;

public interface GbViewsRepository extends JpaRepository<GbViews, Long>{
	
	GbViews findByGbViewId(Long gbViewId);
	List<GbViews> findAll();
	List<GbViews> findByGbViewName(String gbViewName);
	List<GbViews> findByGbViewNameAndObjLang(String gbViewName, String objLang);
	List<GbViews> findByGbViewNameAndObjTypeAndObjLang(String gbViewName, String objType, String objLang);
	List<GbViews> findByGbViewNameAndObjTypeAndObjSetAndObjLang(String gbViewName, String objType, String objSet, String objLang);
	List<GbViews> findByObjName(String objName);
	Long removeByGbViewName(String gbViewName);
	Long removeByGbViewNameAndObjLang(String gbViewName, String objLang);
}
