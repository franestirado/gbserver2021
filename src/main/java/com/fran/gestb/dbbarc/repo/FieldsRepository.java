package com.fran.gestb.dbbarc.repo;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

import com.fran.gestb.dbbarc.model.Fields;

public interface FieldsRepository extends JpaRepository<Fields, Long> {

	Fields findByFieldId(Long fieldId);
	List<Fields> findAll();
	Fields findByFieldName(String fieldName);

}
