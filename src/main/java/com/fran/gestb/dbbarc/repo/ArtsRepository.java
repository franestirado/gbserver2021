package com.fran.gestb.dbbarc.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fran.gestb.dbbarc.model.Arts;


public interface ArtsRepository extends JpaRepository<Arts, Long> {

	List<Arts> findAll();
	List<Arts> findAllByArtType(String artType);
	Arts findByArtId(Long artId);
	Arts findByArtName(String artName);
	Arts findByArtReference(String artReference);

}

