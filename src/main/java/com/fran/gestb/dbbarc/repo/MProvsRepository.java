package com.fran.gestb.dbbarc.repo;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

import com.fran.gestb.dbbarc.model.MProvs;

public interface MProvsRepository extends JpaRepository<MProvs, Long> {

	List<MProvs> findAll();
	List<MProvs> findByMprovProvId(Long mprovProvId);
	MProvs findByMprovId(Long mprovId);
	int deleteAllByMprovProvId(Long mprovProvId);

}

