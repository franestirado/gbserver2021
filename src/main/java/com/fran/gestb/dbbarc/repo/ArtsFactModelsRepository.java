package com.fran.gestb.dbbarc.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fran.gestb.dbbarc.model.ArtsFactModels;

public interface ArtsFactModelsRepository extends JpaRepository<ArtsFactModels, Long> {

	ArtsFactModels findByArtsFactModelsId(Long artsFactModelsId);
	List<ArtsFactModels> findAll();
	int deleteByFactModelsId(Long factModelsId);
	List<ArtsFactModels> findByFactModelsId(Long factModelsId);
	List<ArtsFactModels> findByArtId(Long artId);

}