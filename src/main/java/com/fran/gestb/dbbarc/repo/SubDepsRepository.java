package com.fran.gestb.dbbarc.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fran.gestb.dbbarc.model.SubDeps;


public interface SubDepsRepository extends JpaRepository<SubDeps, Long> {
		List<SubDeps> findAll();
		List<SubDeps> findAllBySubDepDepId(Long subDepDepId);
		SubDeps findBySubDepId(Long subDepId);
		SubDeps findBySubDepName(String subDepName);
		SubDeps findBySubDepTag(String SubDepTag);
}
