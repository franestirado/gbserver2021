package com.fran.gestb.dbbarc.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fran.gestb.dbbarc.model.ArtTypes;

public interface ArtTypesRepository extends JpaRepository<ArtTypes, Long> {
		List<ArtTypes> findAll();
		List<ArtTypes> findAllByArtTypeSubDepId(Long artTypeSubDepId);
		ArtTypes findByArtTypeId(Long artTypeId);
		ArtTypes findByArtTypeName(String artTypeName);
		ArtTypes findByArtTypeTag(String ArtTypeTag);

}
