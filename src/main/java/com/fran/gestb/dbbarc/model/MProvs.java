package com.fran.gestb.dbbarc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "mprovs")
public class MProvs {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "mprovId", updatable = false, nullable = false)
	private Long mprovId;

	@NotNull	
	@Column(nullable=false)
	private Long mprovProvId;
	
	@NotNull
	@Size(max = 64)
	@Column(nullable=false)
	private String mprovType;
	
	@NotNull
	@Column(nullable=false)
	@Size(max = 128)
	private String mprovData1;
	
	@NotNull
	@Column(nullable=true)
	@Size(max = 128)
	private String mprovData2;
	
	// Checking attributes
	public boolean checkMProvAtributes() {
		if (this.mprovType.length() == 0) return false;
		if ( (this.mprovType.length() < 3) | (this.mprovType.length() >64) )return false;
		if (this.mprovData1.length() == 0) return false;
		if ( (this.mprovData1.length() < 3) | (this.mprovData1.length() >128) )return false;
		if (this.mprovData2.length() != 0) {
			if ( (this.mprovData2.length() < 3) |( this.mprovData2.length() >128) ) return false;
		}
		return true;
	}
	// constructors
	public MProvs() {		
	}
	public MProvs(Long mprovId, Long mprovProvId, String mprovType, String mprovData1, String mprovData2) {
		super();
		this.mprovId = mprovId;
		this.mprovProvId = mprovProvId;
		this.mprovType = mprovType;
		this.mprovData1 = mprovData1;
		this.mprovData2 = mprovData2;
	}	
	//setters and getters

	public Long getMprovId() {
		return mprovId;
	}

	public void setMprovId(Long mprovId) {
		this.mprovId = mprovId;
	}

	public Long getMprovProvId() {
		return mprovProvId;
	}

	public void setMprovProvId(Long mprovProvId) {
		this.mprovProvId = mprovProvId;
	}

	public String getMprovType() {
		return mprovType;
	}

	public void setMprovType(String mprovType) {
		this.mprovType = mprovType;
	}

	public String getMprovData1() {
		return mprovData1;
	}

	public void setMprovData1(String mprovData1) {
		this.mprovData1 = mprovData1;
	}

	public String getMprovData2() {
		return mprovData2;
	}

	public void setMprovData2(String mprovData2) {
		this.mprovData2 = mprovData2;
	}
	
	
}
