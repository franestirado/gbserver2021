package com.fran.gestb.dbbarc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "arttypes")
public class ArtTypes {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "artTypeId", updatable = false, nullable = false)
	private Long artTypeId;		
	@NotNull	
	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date artTypeDate;	
	@NotNull
	@Size(max = 32)
	@Column(unique = true, nullable=false)
	private String artTypeName;		
	@NotNull
	@Size(max = 64)
	@Column(nullable=false)
	private String artTypeDescription;		
	@NotNull
	@Column(nullable=false)
	@Size(max = 16)
	private String artTypeTag;	
	@NotNull
	@Column(nullable=false)	
	private Long artTypeSequence;
	@NotNull
	@Column(nullable=false)
	@Size(max = 32)
	private String artTypeSubDepName;
	@NotNull
	@Column(nullable=false)	
	private Long artTypeSubDepId;
	@Column(nullable=false)	
	private boolean artTypeStatus;	
	// Checking attributes
	public boolean checkArtTypeAtributes() {	
		if (this.artTypeDate == null) return false;
		if ( (this.artTypeName.length() < 1) | (this.artTypeName.length() >32) )return false;		
		if ( (this.artTypeDescription.length() < 1) | (this.artTypeDescription.length() >64) )return false;
		if ( (this.artTypeTag.length() < 1) | (this.artTypeTag.length() >16) )return false;
		return true;
	}
	// constructors
	public ArtTypes() {}
	public ArtTypes(Long artTypeId, Date artTypeDate, String artTypeName, String artTypeDescription, String artTypeTag,
			 		Long artTypeSequence, String artTypeSubDepName, Long artTypeSubDepId, boolean artTypeStatus) {
		super();
		this.artTypeId = artTypeId;
		this.artTypeDate = artTypeDate;
		this.artTypeName = artTypeName;
		this.artTypeDescription = artTypeDescription;
		this.artTypeTag = artTypeTag;
		this.artTypeSequence = artTypeSequence;
		this.artTypeSubDepName = artTypeSubDepName;
		this.artTypeSubDepId = artTypeSubDepId;
		this.artTypeStatus = artTypeStatus;
	}
	//setters and getters
	public Long getArtTypeId() {
		return artTypeId;
	}
	public void setArtTypeId(Long artTypeId) {
		this.artTypeId = artTypeId;
	}
	public Date getArtTypeDate() {
		return artTypeDate;
	}
	public void setArtTypeDate(Date artTypeDate) {
		this.artTypeDate = artTypeDate;
	}
	public String getArtTypeName() {
		return artTypeName;
	}
	public void setArtTypeName(String artTypeName) {
		this.artTypeName = artTypeName;
	}
	public String getArtTypeDescription() {
		return artTypeDescription;
	}
	public void setArtTypeDescription(String artTypeDescription) {
		this.artTypeDescription = artTypeDescription;
	}
	public String getArtTypeTag() {
		return artTypeTag;
	}
	public void setArtTypeTag(String artTypeTag) {
		this.artTypeTag = artTypeTag;
	}
	public Long getArtTypeSequence() {
		return artTypeSequence;
	}
	public void setArtTypeSequence(Long artTypeSequence) {
		this.artTypeSequence = artTypeSequence;
	}
	public String getArtTypeSubDepName() {
		return artTypeSubDepName;
	}
	public void setArtTypeSubDepName(String artTypeSubDepName) {
		this.artTypeSubDepName = artTypeSubDepName;
	}
	public Long getArtTypeSubDepId() {
		return artTypeSubDepId;
	}
	public void setArtTypeSubDepId(Long artTypeSubDepId) {
		this.artTypeSubDepId = artTypeSubDepId;
	}
	public boolean isArtTypeStatus() {
		return artTypeStatus;
	}
	public void setArtTypeStatus(boolean artTypeStatus) {
		this.artTypeStatus = artTypeStatus;
	}	
}