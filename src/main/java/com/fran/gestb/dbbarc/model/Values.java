package com.fran.gestb.dbbarc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "values")
public class Values {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "valueId", updatable = false, nullable = false)
	private Long valueId;
	
	@NotNull
	@Size(max = 128)
	private String valueName;

	@NotNull
	private Long fieldId;
	
	// Checking attributes
	public boolean checkValueAtributes() {
		if (this.fieldId == null) return false;
		if (this.valueName.length() == 0) return false;
		if ( (this.valueName.length() < 1) | (this.valueName.length() >128) )return false;
		return true;
	}
	//constructors
	public Values() {
	}
	public Values(String valueName, Long fieldId) {
		this.valueName = valueName;
		this.fieldId = fieldId;
	}
	public Values(Long valueId, String valueName, Long fieldId) {
		this.valueId = valueId;
		this.valueName = valueName;
		this.fieldId = fieldId;
	}
	//setters and getters
	public Long getValueId() {
		return valueId;
	}
	public void setValueId(Long valueId) {
		this.valueId = valueId;
	}
	public String getValueName() {
		return valueName;
	}
	public void setValueName(String valueName) {
		this.valueName = valueName;
	}
	public Long getFieldId() {
		return fieldId;
	}
	public void setFieldId(Long fieldId) {
		this.fieldId = fieldId;
	}
	
}