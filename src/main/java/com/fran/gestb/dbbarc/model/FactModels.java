package com.fran.gestb.dbbarc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "factmodels")
public class FactModels {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(updatable = false, nullable = false)
	private Long factModelsId;
	@NotNull	
	@Temporal(TemporalType.DATE)
	@Column
	private Date factModelsDate;
	@NotNull
	@Size(max = 64)
	@Column	
	private String factModelsName;
	@NotNull
	@Column
	private Long factModelsProvId;
	@NotNull
	@Size(max = 64)
	@Column	
	private String factModelsProvName;
	@NotNull
	@Size(max = 16)
	@Column	
	private String factModelsFactType;
	@NotNull
	@Size(max = 16)
	@Column	
	private String factModelsPayType;
	@NotNull
	@Column	
	private boolean factModelsNote;
	// Checking attributes
	public boolean checkFactModelsAtributes() {
		if (this.factModelsDate == null) return false;
		if ( (this.factModelsName == null) 	|| (this.factModelsName.length() == 0)) return false;
		if ( (this.factModelsName.length() < 1) || (this.factModelsName.length() >64) )return false;
		if ( (this.factModelsProvName == null) || (this.factModelsProvName.length() == 0)) return false;
		if ( (this.factModelsProvName.length() < 1) || (this.factModelsProvName.length() >64) )return false;
		if ( (this.factModelsFactType == null) || (this.factModelsFactType.length() == 0)) return false;
		if ( (this.factModelsFactType.length() < 1) || (this.factModelsFactType.length() >16) )return false;
		if ( (this.factModelsPayType == null) || (this.factModelsPayType.length() == 0)) return false;
		if ( (this.factModelsPayType.length() < 1) || (this.factModelsPayType.length() >16) )return false;

		return true;
	}
	// constructors
	public FactModels() {}
	public FactModels(	Long factModelsId, Date factModelsDate, String factModelsName, Long factModelsProvId, 
						String factModelsProvName, String factModelsFactType, String factModelsPayType,
						boolean factModelsNote) {
		super();
		this.factModelsId = factModelsId;
		this.factModelsDate = factModelsDate;
		this.factModelsName = factModelsName;
		this.factModelsProvId = factModelsProvId;
		this.factModelsProvName = factModelsProvName;
		this.factModelsFactType = factModelsFactType;
		this.factModelsPayType = factModelsPayType;
		this.factModelsNote = factModelsNote;
	}
	//setters and getters
	public Long getFactModelsId() {
		return factModelsId;
	}
	public void setFactModelsId(Long factModelsId) {
		this.factModelsId = factModelsId;
	}
	public Date getFactModelsDate() {
		return factModelsDate;
	}
	public void setFactModelsDate(Date factModelsDate) {
		this.factModelsDate = factModelsDate;
	}
	public String getFactModelsName() {
		return factModelsName;
	}
	public void setFactModelsName(String factModelsName) {
		this.factModelsName = factModelsName;
	}
	public Long getFactModelsProvId() {
		return factModelsProvId;
	}
	public void setFactModelsProvId(Long factModelsProvId) {
		this.factModelsProvId = factModelsProvId;
	}
	public String getFactModelsProvName() {
		return factModelsProvName;
	}
	public void setFactModelsProvName(String factModelsProvName) {
		this.factModelsProvName = factModelsProvName;
	}
	public String getFactModelsFactType() {
		return factModelsFactType;
	}
	public void setFactModelsFactType(String factModelsFactType) {
		this.factModelsFactType = factModelsFactType;
	}
	public String getFactModelsPayType() {
		return factModelsPayType;
	}
	public void setFactModelsPayType(String factModelsPayType) {
		this.factModelsPayType = factModelsPayType;
	}
	public boolean isFactModelsNote() {
		return factModelsNote;
	}
	public void setFactModelsNote(boolean factModelsNote) {
		this.factModelsNote = factModelsNote;
	}

}
