package com.fran.gestb.dbbarc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fran.gestb.aacommon.ConsoleLog;
import com.fran.gestb.aaexceptions.EmptyFieldException;
import com.fran.gestb.aaexceptions.UserAlreadyExistsException;
import com.fran.gestb.dbbarc.model.Arts;
import com.fran.gestb.dbbarc.model.ArtsFactModels;
import com.fran.gestb.dbbarc.model.FactModels;
import com.fran.gestb.dbbarc.model.Provs;
import com.fran.gestb.dbbarc.service.FactModelsService;

@RestController
@RequestMapping("/modelFacts")
public class FactModelsController {

	@Autowired
	private FactModelsService factModelsService;
	@Autowired
	private ConsoleLog consoleLog; 
	private boolean showLogMsg = true;

	// -------------------Model Facts------------------------------------------------
	// -------------------Model Facts----->getAllFactModels-------------------------------------------
	@RequestMapping(value = "/getAllModels", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public List<FactModels> getAllFactModels() throws UserAlreadyExistsException, EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->FactsController->getAllFactModels","Entering","..>>...................");
		List<FactModels> modelFactsList = factModelsService.getAllFactModels();
		if (modelFactsList == null) throw new EmptyFieldException("No all Model Facts found");
		consoleLog.logMsg(showLogMsg,"->FactsController->getAllFactModels","Exiting","...................>>..");
		return modelFactsList; 
	}
	// -------------------Model Facts----->getModelsProv-------------------------------------------
	@RequestMapping(value = "/getModelsProv", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public List<FactModels> getFactModelsProv(@RequestBody Provs selectedProv) throws UserAlreadyExistsException, EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->FactsController->getFactModelsProv","Entering","..>>...................");
		List<FactModels> modelFactsList = factModelsService.getFactModelsProv(selectedProv);
		if (modelFactsList == null) throw new EmptyFieldException("No Prov Model Facts found");
		consoleLog.logMsg(showLogMsg,"->FactsController->getFactModelsProv","Exiting","...................>>..");
		return modelFactsList; 
	}
	// -------------------Model Facts----->createOneFactModel------------------------------------------------
	@RequestMapping(value = "/createOneModel", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public FactModels createOneFactModel(@RequestBody FactModels newFactModels) {
		consoleLog.logMsg(showLogMsg,"->FactsController->createOneFactModel","Entering","..>>...................");
		FactModels checkFactModels = factModelsService.createOneFactModel(newFactModels);
		consoleLog.logMsg(showLogMsg,"->FactsController->createOneFactModel","Exiting","...................>>..");
		return checkFactModels;	
	}
	// -------------------Model Facts----->createProvFactModel------------------------------------------------
	@RequestMapping(value = "/createProvFactModel", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public FactModels createProvFactModel(@RequestBody String provNameFactModel) {
		consoleLog.logMsg(showLogMsg,"->FactsController->createProvFactModel","Entering","..>>...................");
		FactModels checkFactModels = factModelsService.createProvFactModel(provNameFactModel);
		consoleLog.logMsg(showLogMsg,"->FactsController->createProvFactModel","Exiting","...................>>..");
		return checkFactModels;	
	}
	// -------------------Model Facts----->getOneFactModel------------------------------------------------
	@RequestMapping(value="/getOneModel", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public FactModels getOneFactModel(@RequestBody FactModels modFactModels) {
		consoleLog.logMsg(showLogMsg,"->FactsController->getOneFactModel","Entering","..>>...................");
		factModelsService.getOneFactModel(modFactModels);
		consoleLog.logMsg(showLogMsg,"->FactsController->getOneFactModel","Exiting","...................>>..");
		return modFactModels;
	}
	// -------------------Model Facts----->delOneFactModel------------------------------------------------
	@RequestMapping(value="/delOneModel", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public void delOneFactModel(@RequestBody FactModels delFactModels) {
		consoleLog.logMsg(showLogMsg,"->FactsController->delOneFactModel","Entering","..>>...................");
		factModelsService.delOneFactModel(delFactModels);
		consoleLog.logMsg(showLogMsg,"->FactsController->delOneFactModel","Exiting","...................>>..");
		return;
	}
	// -------------------Model Facts----->delAllFactModels------------------------------------------------
	@RequestMapping(value="/delAllModels", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public void delAllFactModels(@RequestBody int delFlag ) {
		consoleLog.logMsg(showLogMsg,"->FactsController->delAllFactModels","Entering","..>>...................");
		factModelsService.deleteAllFactModels();
		consoleLog.logMsg(showLogMsg,"->FactsController->delAllFactModels","Exiting","...................>>..");
		return;
	}
	// -------------------Arts Model Facts------------------------------------------------
	// -------------------Arts Model Facts----->createArtsFactModel------------------------------------------------
	@RequestMapping(value = "/createArtsModel", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public List<Arts> createArtsFactModel(@RequestBody List<Arts> artsFactModels) throws EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->FactsController->createArtsFactModel","Entering","..>>...................");
		// Check mandatory parameters in newField
		if (artsFactModels==null) throw new EmptyFieldException("Wrong Field parameters");
		List<Arts> readArtsFactModels = factModelsService.createArtsFactModel(artsFactModels);
		consoleLog.logMsg(showLogMsg,"->FactsController->createArtsFactModel","Exiting","...................>>..");
		return readArtsFactModels;		
	}
	// -------------------Arts Model Facts----->getArtsFactModel------------------------------------------------
	@RequestMapping(value = "/getArtsModel", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)	
	public List<Arts> getArtsFactModel(@RequestBody FactModels updFactModels) throws EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->FactsController->getArtsFactModel","Entering","..>>...................");
		// Check mandatory parameters in newField
		if (updFactModels==null) throw new EmptyFieldException("Wrong Field parameters");		
		List<Arts> readArtsFactModels = factModelsService.getArtsFactModel(updFactModels);
		consoleLog.logMsg(showLogMsg,"->FactsController->getArtsFactModel","Exiting","...................>>..");
		return readArtsFactModels;		
	}
	// -------------------Arts Model Facts----->getArtFactModelsWithArt------------------------------------------------
	@RequestMapping(value = "/getArtFacModelsWithArt", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)	
	public List<ArtsFactModels> getArtFactModelsWithArt(@RequestBody Arts selArt) throws EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->FactsController->getArtFactModelsWithArt","Entering","..>>...................");
		// Check mandatory parameters in newField
		if (selArt==null) throw new EmptyFieldException("Wrong Field parameters");		
		List<ArtsFactModels> readArtsFactModels = factModelsService.getArtFactModelsWithArt(selArt);
		consoleLog.logMsg(showLogMsg,"->FactsController->getArtFactModelsWithArt","Exiting","...................>>..");
		return readArtsFactModels;		
	}
}
