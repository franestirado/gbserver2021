package com.fran.gestb.dbbarc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fran.gestb.aacommon.ConsoleLog;
import com.fran.gestb.aaexceptions.EmptyFieldException;
import com.fran.gestb.aaexceptions.UserAlreadyExistsException;
import com.fran.gestb.dbbarc.model.MProvs;
import com.fran.gestb.dbbarc.model.Provs;
import com.fran.gestb.dbbarc.service.ProvsService;

@RestController
@RequestMapping("/provs")
public class ProvsController {

	@Autowired
	private ProvsService provsService;
	@Autowired
	private ConsoleLog consoleLog; 
	private boolean showLogMsg = true;
	
	// -------------------Provs----->getAllProvs-------------------------------------------
	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public List<Provs> getAllProvs() throws UserAlreadyExistsException, EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->ProvsController->getAllProvs","Entering","..>>...................");
		List<Provs> provsList = provsService.getAllProvs();
		if (provsList == null) throw new EmptyFieldException("No allProvs found");
		consoleLog.logMsg(showLogMsg,"->ProvsController->getAllProvs","Exiting","...................>>..");
		return provsList; 
	}
	// -------------------Provs----->getAllProvsWithFactType------------------------------------------------
	@RequestMapping(value = "/withFactType", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public List<Provs>  getAllProvsWithFactType(@RequestBody String selFactType) {
		consoleLog.logMsg(showLogMsg,"->ProvsController->getAllProvsWithFactType","Entering","..>>...................");
		List<Provs> provsList = provsService.getAllProvsWithFactType(selFactType);
		consoleLog.logMsg(showLogMsg,"->ProvsController->getAllProvsWithFactType","Exiting","...................>>..");
		return provsList;	
	}
	// -------------------Provs----->getAllProvsWithPayType------------------------------------------------
	@RequestMapping(value = "/withPayType", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public List<Provs>  getAllProvsWithPayType(@RequestBody String selPayType) {
		consoleLog.logMsg(showLogMsg,"->ProvsController->getAllProvsWithPayType","Entering","..>>...................");
		List<Provs> provsList = provsService.getAllProvsWithPayType(selPayType);
		consoleLog.logMsg(showLogMsg,"->ProvsController->getAllProvsWithPayType","Exiting","...................>>..");
		return provsList;	
	}
	// -------------------Provs----->getAllProvsWithPayType------------------------------------------------
	@RequestMapping(value = "/withFactOrNote", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public List<Provs>  getAllProvsWithFactOrNote(@RequestBody String factOrNote) {
		consoleLog.logMsg(showLogMsg,"->ProvsController->getAllProvsWithFactOrNote","Entering","..>>...................");
		List<Provs> provsList = provsService.getAllProvsWithFactOrNote(factOrNote);
		consoleLog.logMsg(showLogMsg,"->ProvsController->getAllProvsWithFactOrNote","Exiting","...................>>..");
		return provsList;	
	}
	// -------------------Provs----->addOneProv------------------------------------------------
	@RequestMapping(value = "/post", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public Provs addOneProv(@RequestBody Provs newProv) {
		consoleLog.logMsg(showLogMsg,"->ProvsController->addOneProv","Entering","..>>...................");
		Provs checkProv = provsService.saveProv(newProv);
		consoleLog.logMsg(showLogMsg,"->ProvsController->addOneProv","Exiting","...................>>..");
		return checkProv;	
	}
	// -------------------Provs----->getOneProvById------------------------------------------------
	@RequestMapping(value="/getOneById", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public Provs getOneProvById(@RequestBody long updProvId) {
		consoleLog.logMsg(showLogMsg,"->ProvsController->getOneProvById","Entering","..>>...................");
		Provs readProv = provsService.getOneProvById(updProvId);
		consoleLog.logMsg(showLogMsg,"->ProvsController->getOneProvById","Exiting","...................>>..");
		return readProv;
	}
	// -------------------Provs----->getOneProv------------------------------------------------
	@RequestMapping(value="/getOne", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public Provs getOneProv(@RequestBody Provs modProv) {
		consoleLog.logMsg(showLogMsg,"->ProvsController->getOneProv","Entering","..>>...................");
		provsService.getOneProv(modProv);
		consoleLog.logMsg(showLogMsg,"->ProvsController->getOneProv","Exiting","...................>>..");
		return modProv;
	}
	// -------------------Provs----->updateOneProv------------------------------------------------
	@RequestMapping(value = "/updOne", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public Provs updateOneProv(@RequestBody Provs updProv) throws EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->ProvsController->updateOneProv","Entering","..>>...................");
		// Check mandatory parameters in newField
		if ((updProv.getProvName()=="") | (updProv==null)) throw new EmptyFieldException("Wrong Field parameters");	
		Provs checkProv = provsService.updateOneProv(updProv);
		consoleLog.logMsg(showLogMsg,"->ProvsController->updateOneProv","Exiting","...................>>..");
		return checkProv;		
	}
	// -------------------Provs----->updateOneProvSpecial------------------------------------------------
	@RequestMapping(value = "/updOneSpecial", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public Provs updateOneProvSpecial(@RequestBody Provs updProv) throws EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->ProvsController->updateOneProvSpecial","Entering","..>>...................");
		// Check mandatory parameters in newField
		if ((updProv.getProvName()=="") | (updProv==null)) throw new EmptyFieldException("Wrong Field parameters");	
		Provs checkProv = provsService.updateOneProvSpecial(updProv);
		consoleLog.logMsg(showLogMsg,"->ProvsController->updateOneProvSpecial","Exiting","...................>>..");
		return checkProv;		
	}
	// -------------------Provs----->delOneProv------------------------------------------------
	@RequestMapping(value="/delOne", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public void delOneProv(@RequestBody Provs delProv) {
		consoleLog.logMsg(showLogMsg,"->ProvsController->delOneProv","Entering","..>>...................");
		provsService.deleteOneProv(delProv);
		consoleLog.logMsg(showLogMsg,"->ProvsController->delOneProv","Exiting","...................>>..");
		return;
	}
	// -------------------Provs----->delAllProvs------------------------------------------------
	@RequestMapping(method=RequestMethod.DELETE)
	public void delAllProvs() {
		consoleLog.logMsg(showLogMsg,"->ProvsController->delAllProvs","Entering","..>>...................");
		provsService.deleteAllProvs();
		consoleLog.logMsg(showLogMsg,"->ProvsController->delAllProvs","Exiting","...................>>..");
		return;
	}
	// -------------------More Provs------------------------------------------------
	// -------------------MProvs----->addMoreProv------------------------------------------------
	@PostMapping(value = "/addMoreProvList")
	@ResponseBody	
	public List<MProvs> createMoreProvList(@RequestBody List<MProvs> newMoreProvList) {
		consoleLog.logMsg(showLogMsg,"->ProvsController->createMoreProvList","Entering","..>>...................");
		List<MProvs>  createdMoreProvList = provsService.createMoreProvList(newMoreProvList);
		consoleLog.logMsg(showLogMsg,"->ProvsController->createMoreProvList","Exiting","...................>>..");
		return createdMoreProvList;	
	}
	// -------------------MProvs----->updateMoreProvList------------------------------------------------
	@PostMapping(value = "/updateMoreProvList")
	@ResponseBody	
	public List<MProvs> updateMoreProvList(@RequestBody List<MProvs> newMoreProvList) {
		consoleLog.logMsg(showLogMsg,"->ProvsController->updateMoreProvList","Entering","..>>...................");
		List<MProvs>  createdMoreProvList = provsService.updateMoreProvList(newMoreProvList);
		consoleLog.logMsg(showLogMsg,"->ProvsController->updateMoreProvList","Exiting","...................>>..");
		return createdMoreProvList;	
	}
	// -------------------MProvs----->getAllMProvsProvd-------------------------------------------
	@RequestMapping(value = "/getAllMoreProv", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public List<MProvs> getAllMoreProv(@RequestBody Provs newProv) throws EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->ProvsController->getAllMoreProv","Entering","..>>...................");
		List<MProvs> mProvsList = provsService.getAllMoreProv(newProv);
		if (mProvsList == null) throw new EmptyFieldException("No all MProvs found");
		consoleLog.logMsg(showLogMsg,"->ProvsController->getAllMoreProv","Exiting","...................>>..");
		return mProvsList; 
	}
	
}
