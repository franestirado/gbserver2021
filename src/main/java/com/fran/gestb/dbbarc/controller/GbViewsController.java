package com.fran.gestb.dbbarc.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fran.gestb.aacommon.ConsoleLog;
import com.fran.gestb.aacommon.GbViewsCreator;
import com.fran.gestb.aacommon.ValueRecord;
import com.fran.gestb.aaexceptions.EmptyFieldException;
import com.fran.gestb.aaexceptions.EntityAlreadyExistsException;
import com.fran.gestb.aaexceptions.UserAlreadyExistsException;
import com.fran.gestb.dbbarc.model.GbViews;
import com.fran.gestb.dbbarc.service.GbViewsService;

@RestController
@RequestMapping("/gbViews")
public class GbViewsController {

	@Autowired
	private GbViewsService gbViewsService;
	@Autowired
	private GbViewsCreator gbViewsCreator;
	@Autowired
	private ConsoleLog consoleLog; 
	private boolean showLogMsg = true;
	
	private GbViews[] newGbView,otherGbView;
	private List<GbViews> allGbViews,gbViewsList;
	private GbViews mainGbView[];
	private String gbViewName, gbViewLang;
	private int index;
	
	// -------------------GbViews----->RetrieveAll-------------------------------------------
	@RequestMapping(value = "/get", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public List<GbViews> getAllGbViews() throws UserAlreadyExistsException, EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->GbViewsController->getAllGbViews","Entering","");
		allGbViews = gbViewsService.findByObjName("screen");
		if (allGbViews == null) throw new EmptyFieldException("->GbViewsController->getAllGbViews->No SCREEN GbViews found");
		consoleLog.logMsg(showLogMsg,"->GbViewsController->getAllGbViews","Exiting","");
		return allGbViews; 
	}
	// -------------------GbViews----->RetrieveOne------------------------------------------------
	// -------------------GbViews----->Create GbView---Add Several Records------------------------------------------------
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public GbViews[] createGbView(@RequestBody String gbViewName) throws EmptyFieldException, EntityAlreadyExistsException  {
		consoleLog.logMsg(showLogMsg,"->GbViewsController->createGbView","Entering","");
		if (gbViewName == null) throw new EmptyFieldException("GbViews create, empty string");
		gbViewsList= gbViewsService.findByGbViewName(gbViewName);
		if (gbViewsList.size() != 0) throw new EntityAlreadyExistsException("GbViews create, gbView already created");
		mainGbView= gbViewsCreator.createOneGbView(gbViewName,"APP",null);
		consoleLog.logMsg(showLogMsg,"->GbViewsController->createGbView","Exiting","");
		return mainGbView;	
	}
	// -------------------GbViews----->Get All GbView Data---Get Several Records------------------------------------------------
	@RequestMapping(value = "/getdata", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public List<GbViews> getGbViewData(@RequestBody GbViews editGbView) throws EmptyFieldException, EntityAlreadyExistsException  {
		consoleLog.logMsg(showLogMsg,"->GbViewsController->getGbViewData","Entering","");
		if ((editGbView.getGbViewName()=="") | (editGbView==null)) throw new EmptyFieldException("Wrong GbView parameters");
		gbViewName = editGbView.getGbViewName();
		gbViewLang = editGbView.getObjLang();
		gbViewsList= gbViewsService.findByGbViewNameAndObjLang(gbViewName, gbViewLang);
		if (gbViewsList.size() == 0) throw new EntityAlreadyExistsException("Get All GbView data, gbView not created yet");
		consoleLog.logMsg(showLogMsg,"->GbViewsController->getGbViewData","Exiting","");
		return gbViewsList;	
	}
	// -------------------GbViews----->AddOne------------------------------------------------
	@RequestMapping(value = "/newview", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public GbViews[] addNewGbView(@RequestBody ValueRecord[] gbViewNewTextList) throws EmptyFieldException, EntityAlreadyExistsException  {
		consoleLog.logMsg(showLogMsg,"->GbViewsController->addNewGbView","Entering","");
		if (gbViewNewTextList == null) throw new EmptyFieldException("addNewGbView, empty data");
		index = gbViewNewTextList.length;
		gbViewName = gbViewNewTextList[index-1].getValue();
		gbViewLang = gbViewNewTextList[index-2].getValue();
		newGbView= gbViewsCreator.createOneGbView(gbViewName,gbViewLang,gbViewNewTextList);
		consoleLog.logMsg(showLogMsg,"->GbViewsController->addNewGbView","Exiting","");
		return newGbView;	
	}
	// -------------------GbViews----->DeleteOne------------------------------------------------
	@RequestMapping(value="/delOne", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public void delOneGbView(@RequestBody GbViews delGbView) throws EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->GbViewsController->delOneGbView","Entering","");
		if ((delGbView.getGbViewName()=="") | (delGbView==null)) throw new EmptyFieldException("Wrong GbView parameters");	
		gbViewsService.delete(delGbView);
		consoleLog.logMsg(showLogMsg,"->GbViewsController->delOneGbView","Exiting","");
	}
	// -------------------GbViews----->DeleteOne------------------------------------------------
	@RequestMapping(value="/getView", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public HashMap<String, Object> getGbViews(@RequestBody String viewName) throws EmptyFieldException, JsonParseException, JsonMappingException, IOException {
		consoleLog.logMsg(showLogMsg,"->GbViewsController->getGbViews","Entering","");
		if ((viewName=="") | (viewName==null)) throw new EmptyFieldException("Wrong GbView parameters");
		HashMap<String, Object> completeMessage;
		completeMessage = gbViewsService.getGbViews(viewName);
		consoleLog.logMsg(showLogMsg,"->GbViewsController->getGbViews","Exiting","");
		return completeMessage;
	}
	// -------------------GbViews----->UpdateOne------------------------------------------------
	@RequestMapping(value = "/put", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public GbViews updateOneGbView(@RequestBody GbViews updGbView) throws EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->GbViewsController->updateOneGbView","Entering","");
		// Check mandatory parameters in newGbView
		if ((updGbView.getGbViewName()=="") | (updGbView==null)) throw new EmptyFieldException("Wrong GbView parameters");	
		gbViewsService.update(updGbView);
		consoleLog.logMsg(showLogMsg,"->GbViewsController->updateOneGbView","Exiting","");
		return otherGbView[0];
		
	}
	// -------------------GbViews----->DeleteAll------------------------------------------------
	@RequestMapping(method=RequestMethod.DELETE)
	public void delAllGbViews() {
		consoleLog.logMsg(showLogMsg,"->GbViewsController->delAllGbViews","Entering","");
		gbViewsService.deleteAll();
		consoleLog.logMsg(showLogMsg,"->GbViewsController->delAllGbViews","Exiting","");
	}
	
}
