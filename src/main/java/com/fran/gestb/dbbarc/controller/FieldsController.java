package com.fran.gestb.dbbarc.controller;

import java.sql.SQLException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fran.gestb.aacommon.ConsoleLog;
import com.fran.gestb.aacommon.ValueRecord;
import com.fran.gestb.aaexceptions.EmptyFieldException;
import com.fran.gestb.aaexceptions.UserAlreadyExistsException;
import com.fran.gestb.dbbarc.model.Fields;
import com.fran.gestb.dbbarc.model.Values;
import com.fran.gestb.dbbarc.service.FieldsService;

@RestController
@RequestMapping("/fields")
public class FieldsController {

	@Autowired
	private FieldsService fieldsService;
	@Autowired
	private ConsoleLog consoleLog; 
	private boolean showLogMsg = true;

	private Fields otherField;
	private List<Fields> allFields;
	
	private List<List<ValueRecord>> valueRecordList;
	
	//----------------------------- Fields-------------------------------------------------------------------
	// -------------------Fields----->getAllFields-----------------------------------------------------------
	@RequestMapping(value = "/getAllFields", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public List<Fields> getAllFields() throws UserAlreadyExistsException, EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->FieldsController->getAllFields","Entering","..>>...................");
		allFields = fieldsService.getAllFields();
		if (allFields == null) throw new EmptyFieldException("No Fields found");
		consoleLog.logMsg(showLogMsg,"->FieldsController->getAllFields","Exiting","..>>...................");
		return allFields; 
	}
	// -------------------Fields----->getOneField-----------------------------------------------------------
	@RequestMapping(value = "/getOneField", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public Fields getOneField (@RequestBody Fields newField) {
		consoleLog.logMsg(showLogMsg,"->FieldsController->getOneField","Entering","..>>...................");
		Fields readField = fieldsService.getOneField(newField);
		if (readField == null) throw new EmptyFieldException("No Fields found");
		consoleLog.logMsg(showLogMsg,"->FieldsController->getOneField","Exiting","..>>...................");
		return readField; 
	}
	// -------------------Fields----->addOneField----------------------------------------------------------------
	@RequestMapping(value = "/addOneField", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public Fields addOneField(@RequestBody Fields newField) {
		consoleLog.logMsg(showLogMsg,"->FieldsController->addOneField","Entering","..>>...................");
		otherField= fieldsService.addOneField(newField);
		consoleLog.logMsg(showLogMsg,"->FieldsController->addOneField","Exiting","..>>...................");
		return otherField;	
	}
	// -------------------Fields----->updateOneField----------------------------------------------------------------
	@RequestMapping(value = "/updateOneField", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public Fields updateOneField(@RequestBody Fields updField) throws EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->FieldsController->updateOneField","Entering","..>>...................");
		// Check mandatory parameters in newField
		if ((updField.getFieldName()=="") | (updField==null)) throw new EmptyFieldException("Wrong Field parameters");	
		fieldsService.updateOneField(updField);
		consoleLog.logMsg(showLogMsg,"->FieldsController->updateOneField","Exiting","..>>...................");
		return otherField;		
	}
	// -------------------Fields----->updateOneFieldStatus----------------------------------------------------------------
	@RequestMapping(value = "/updateOneFieldStatus", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public Fields updateOneFieldStatus(@RequestBody Fields updField){
		consoleLog.logMsg(showLogMsg,"->FieldsController->updateOneFieldStatus","Entering","..>>...................");
		fieldsService.updateOneFieldStatus(updField);
		consoleLog.logMsg(showLogMsg,"->FieldsController->updateOneFieldStatus","Exiting","..>>...................");
		return otherField;		
	}	
	// -------------------Fields----->loadDefaultFieldsAndValues----------------------------------------------------------------
	@RequestMapping(value = "/loadDefault", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public Fields loadDefaultFieldsAndValues(){
		consoleLog.logMsg(showLogMsg,"->FieldsController->loadDefaultFieldsAndValues","Entering","..>>...................");
		fieldsService.startCreateFieldsAndValues();
		consoleLog.logMsg(showLogMsg,"->FieldsController->loadDefaultFieldsAndValues","Exiting","..>>...................");
		return otherField;		
	}	
	// -------------------Fields----->deleteOneField Field----------------------------------------------------------------
	@RequestMapping(value = "/deleteOneField", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public Fields deleteOneField(@RequestBody Fields delField) {
		consoleLog.logMsg(showLogMsg,"->FieldsController->deleteOneField","Entering","..>>...................");
		// Check mandatory parameters in newField
		fieldsService.deleteOneField(delField);
		consoleLog.logMsg(showLogMsg,"->FieldsController->deleteOneField","Exiting","..>>...................");
		return otherField;		
	}	
	// -------------------Fields----->delAllFields----------------------------------------------------------------
	@RequestMapping(method=RequestMethod.DELETE)
	public void delAllFields() {
		consoleLog.logMsg(showLogMsg,"->FieldsController->delAllFields","Entering","..>>...................");
		fieldsService.delAllFields();
		consoleLog.logMsg(showLogMsg,"->FieldsController->delAllFields","Exiting","..>>...................");
	}
	//----------------------------- Values-------------------------------------------------------------------
	// -------------------Values----->getOneFieldValuesList------------------------------------------------
	@RequestMapping(value = "/getFieldValues", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public List<Values>  getOneFieldValuesList(@RequestBody Fields newField) {
		consoleLog.logMsg(showLogMsg,"->FieldsController->getOneFieldValuesList","Exiting","..>>...................");
		List<Values> readFieldValues=fieldsService.getOneFieldValuesList(newField);
		consoleLog.logMsg(showLogMsg,"->FieldsController->getOneFieldValuesList","Exiting","..>>...................");
		return readFieldValues;
	}
	// -------------------Values----->createOneFieldValuesList------------------------------------------------
	@RequestMapping(value = "/addFieldValues", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public List<Values> createOneFieldValuesList(@RequestBody List<Values> newFieldValuesList) {
		consoleLog.logMsg(showLogMsg,"->FieldsController->createOneFieldValuesList","Exiting","..>>...................");
		List<Values> readFieldValues=fieldsService.createOneFieldValuesList(newFieldValuesList);
		consoleLog.logMsg(showLogMsg,"->FieldsController->createOneFieldValuesList","Exiting","..>>...................");
		return readFieldValues;
	}
	// -------------------Values----->updateOneFieldValuesList------------------------------------------------
	@RequestMapping(value = "/updFieldValues", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public List<Values> updateOneFieldValuesList(@RequestBody List<Values> newFieldValuesList) {
		consoleLog.logMsg(showLogMsg,"->FieldsController->updateOneFieldValuesList","Exiting","..>>...................");
		List<Values> readFieldValues=fieldsService.updateOneFieldValuesList(newFieldValuesList);
		consoleLog.logMsg(showLogMsg,"->FieldsController->updateOneFieldValuesList","Exiting","..>>...................");
		return readFieldValues;
	}
	// -------------------Values----->getFieldAndValues------------------------------------------------
	@RequestMapping(value="/getFieldAndValues",method=RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public List<List<ValueRecord>> getFieldAndValues()  {
		consoleLog.logMsg(showLogMsg,"->FieldsController->getFieldAndValues","Entering","..>>...................");
		valueRecordList = fieldsService.getFieldAndValues();
		consoleLog.logMsg(showLogMsg,"->FieldsController->getFieldAndValues","Exiting","..>>...................");
		return valueRecordList;
	}
	
}
