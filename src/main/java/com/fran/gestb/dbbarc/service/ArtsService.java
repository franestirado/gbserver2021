package com.fran.gestb.dbbarc.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fran.gestb.aacommon.ConsoleLog;
import com.fran.gestb.aaexceptions.DeleteNotAllowedException;
import com.fran.gestb.aaexceptions.EmptyFieldException;
import com.fran.gestb.aaexceptions.EntityAlreadyExistsException;
import com.fran.gestb.aaexceptions.EntityNotFoundException;
import com.fran.gestb.aaexceptions.WrongParametersException;
import com.fran.gestb.dbbarc.model.ArtTypes;
import com.fran.gestb.dbbarc.model.Arts;
import com.fran.gestb.dbbarc.model.Deps;
import com.fran.gestb.dbbarc.model.FactModels;
import com.fran.gestb.dbbarc.model.Provs;
import com.fran.gestb.dbbarc.model.SubDeps;
import com.fran.gestb.dbbarc.repo.ArtTypesRepository;
import com.fran.gestb.dbbarc.repo.ArtsRepository;
import com.fran.gestb.dbbarc.repo.DepsRepository;
import com.fran.gestb.dbbarc.repo.FactModelsRepository;
import com.fran.gestb.dbbarc.repo.ProvsRepository;
import com.fran.gestb.dbbarc.repo.SubDepsRepository;
import com.fran.gestb.dbbarxx.model.ArtFacts;
import com.fran.gestb.dbbarxx.model.Facts;
import com.fran.gestb.dbbarxx.repo.ArtFactsRepository;
import com.fran.gestb.dbbarxx.repo.FactsRepository;

@Service
public class ArtsService {
	@Autowired
	private ArtsRepository artsRepository;
	@Autowired
	private ArtFactsRepository artsFactRepository;
	@Autowired
	private DepsRepository depsRepository;
	@Autowired
	private SubDepsRepository subDepsRepository;
	@Autowired
	private ArtTypesRepository artTypesRepository;
	@Autowired
	private ProvsRepository provsRepository;
	@Autowired
	private FactsRepository factsRepository;
	@Autowired
	private FactModelsRepository factModelsRepository;
	@Autowired
	private ConsoleLog consoleLog; 
	private boolean showLogMsg = true;
	// ------------------------------------Arts-----------------------------------------------	
	// -------------------Arts----->getAllArts-------------------------------------------	
	@Transactional(readOnly = true)
	public List<Arts> getAllArts() {
		consoleLog.logMsg(showLogMsg,"->ArtsService->getAllArts","Entering","..>>...................");
		List<Arts> artsList = artsRepository.findAll();
		consoleLog.logMsg(showLogMsg,"->ArtsService->getAllArts","Exiting","...................>>..");
		return artsList;
	}
	// -------------------Arts----->getAllArtsInArtType-------------------------------------------	
	@Transactional(readOnly = true)
	public List<Arts> getAllArtsInArtType(ArtTypes selArtType) {
		consoleLog.logMsg(showLogMsg,"->ArtsService->getAllArtsInArtType","Entering","..>>...................");
		List<Arts> artsList = artsRepository.findAllByArtType(selArtType.getArtTypeTag());
		consoleLog.logMsg(showLogMsg,"->ArtsService->getAllArtsInArtType","Exiting","...................>>..");
		return artsList;
	}
	// -------------------Arts----->getAllArtsWithArtTypeTag-------------------------------------------	
	@Transactional(readOnly = true)
	public List<Arts> getAllArtsWithArtTypeTag(String selArtTypeTag) {
		consoleLog.logMsg(showLogMsg,"->ArtsService->getAllArtsWithArtTypeTag","Entering","..>>...................");
		List<Arts> artsList = artsRepository.findAllByArtType(selArtTypeTag);
		consoleLog.logMsg(showLogMsg,"->ArtsService->getAllArtsWithArtTypeTag","Exiting","...................>>..");
		return artsList;
	}
	// -------------------Arts----->createOne------------------------------------------------
	@Transactional
	public Arts createOneArt(Arts newArt) throws EmptyFieldException, EntityAlreadyExistsException {
		consoleLog.logMsg(showLogMsg,"->ArtsService->createOneArt","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((newArt==null) | (newArt.getArtName()=="")) 
			throw new EmptyFieldException("->ArtsService->createOneArt->Null Object or Empty Mandatoy Parameters");
		/* Checking attributes of new object */
		if (newArt.checkArtAtributes()==false) 
			throw new WrongParametersException("--Art Name-->"+newArt.getArtName()+"--Art Long Name-->"+newArt.getArtLongName());
		/* Checking there no other object with same name */
		Arts readArt = artsRepository.findByArtName(newArt.getArtName());
		if (readArt != null) throw new EntityAlreadyExistsException("--Art Name-->"+readArt.getArtName());
		/* Checking there no other object with same reference */
		readArt = artsRepository.findByArtReference(newArt.getArtReference());
		if (readArt != null) throw new EntityAlreadyExistsException("--Art Reference-->"+readArt.getArtReference());
		readArt = artsRepository.save(newArt);
		consoleLog.logMsg(showLogMsg,"->ArtsService->createOneArt","Exiting","...................>>..");
		return readArt;
	}
	// -------------------Arts----->GetOne------------------------------------------------
	@Transactional
	public Arts getOneArt(Arts modArt) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->ArtsService->getOneArt","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((modArt==null) | (modArt.getArtName()=="")) throw new EmptyFieldException("->ArtsService->getOneArt->Null Object or Empty Mandatoy Parameters");	
		Arts readArt = artsRepository.findByArtId(modArt.getArtId());
		if (readArt == null) throw new EntityNotFoundException("--Art Name-->"+modArt.getArtName());
		consoleLog.logMsg(showLogMsg,"->ArtsService->getOneArt","Exiting","...................>>..");
		return readArt;
	}
	// -------------------Arts----->getOneArtById------------------------------------------------
	@Transactional
	public Arts getOneArtById(long artId) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->ArtsService->getOneArtById","Entering","..>>...................");
		Arts readArt = artsRepository.findByArtId(artId);
		//if (readArt == null) throw new EntityNotFoundException("--Art with this id not found-->");
		consoleLog.logMsg(showLogMsg,"->ArtsService->getOneArtById","Exiting","...................>>..");
		return readArt;
	}
	// -------------------Arts----->UpdateOne------------------------------------------------
	@Transactional
	public Arts updateOneArt(Arts modArt) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->ArtsService->updateOneArt","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((modArt==null) | (modArt.getArtName()=="")) throw new EmptyFieldException("->ArtsService->updateOneArt->Null Object or Empty Mandatoy Parameters");	
		Arts readArt = artsRepository.findByArtId(modArt.getArtId());
		if (readArt == null) throw new EntityNotFoundException("--Prov Name-->"+modArt.getArtName());
		readArt = artsRepository.save(modArt);
		consoleLog.logMsg(showLogMsg,"->ArtsService->updateOneArt","Exiting","...................>>..");
		return readArt;
	}
	// -------------------Arts----->updateOneArtSpecial------------------------------------------------
	@Transactional
	public Arts updateOneArtSpecial(Arts modArt) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->ArtsService->updateOneArtSpecial","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((modArt==null) | (modArt.getArtName()=="")) throw new EmptyFieldException("->ArtsService->updateOneArtSpecial->Null Object or Empty Mandatoy Parameters");	
		Arts readArt = artsRepository.findByArtId(modArt.getArtId());
		if (readArt == null) throw new EntityNotFoundException("--Art Name-->"+modArt.getArtName());
		// update all attributes in art
		readArt = artsRepository.save(modArt);
		// update artName and artType in all artFacts having this artId
		List<ArtFacts> readArtsFactList = artsFactRepository.findAllByArtFactArtId(readArt.getArtId());
		ArtFacts readArtFact;
		for(int j=0; j<readArtsFactList.size(); j++) {
			readArtFact = readArtsFactList.get(j);
			readArtFact.setArtFactArtName(readArt.getArtName());
			readArtFact.setArtFactArtType(readArt.getArtType());
			artsFactRepository.save(readArtFact);
			consoleLog.logMsg(showLogMsg,"->ArtsService->updateOneArtSpecial","Updating artFact....",readArtFact.getArtFactId().toString());
		}
		consoleLog.logMsg(showLogMsg,"->ArtsService->updateOneArtSpecial","Exiting","...................>>..");
		return readArt;
	}	
	// -------------------Arts----->updateArtWithArtFactData------------------------------------------------
	@Transactional
	public Arts updateArtWithArtFactData(Arts updArtFact) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->ArtsService->updateArtWithArtFactData","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((updArtFact==null) | (updArtFact.getArtName()=="")) throw new EmptyFieldException("->ArtsService->updateArtWithArtFactData->Null Object or Empty Mandatoy Parameters");	
		Arts readArt = artsRepository.findByArtId(updArtFact.getArtId());
		if (readArt == null) throw new EntityNotFoundException("--Art Name-->"+updArtFact.getArtName());
		// changes readArt with corresponding attributes of received ArtFact and save o DB
		readArt.setArtDate(updArtFact.getArtDate());
		readArt.setArtUnitPrice(updArtFact.getArtUnitPrice());
		readArt.setArtPackagePrice(updArtFact.getArtPackagePrice());
		readArt.setArtTax(updArtFact.getArtTax()); 
		/*
		readArt.setArtMeasurement(updArtFact.getArtMeasurement());
		readArt.setArtPackageUnits(updArtFact.getArtPackageUnits());
		readArt.setArtSize(updArtFact.getArtSize());*/		
		if ((readArt.checkArtAtributes()==false)) throw new EmptyFieldException("->ArtsService->updateArtWithArtFactData->Wrong Attributes");	
		Arts savedArt = artsRepository.save(readArt);
		consoleLog.logMsg(showLogMsg,"->ArtsService->updateArtWithArtFactData","Exiting","...................>>..");
		return savedArt;
	}
	
	// -------------------Arts----->DeleteOne------------------------------------------------
	@Transactional
	public void deleteOneArt(Arts delArt) {
		consoleLog.logMsg(showLogMsg,"->ArtsService->deleteOneArt","Entering","..>>...................");
		/* Check if the field exist */
		artsRepository.deleteById(delArt.getArtId());
		consoleLog.logMsg(showLogMsg,"->ArtsService->deleteOneArt","Exiting","...................>>..");
	}
	// -------------------Arts----->DeleteAll------------------------------------------------
	@Transactional
	public void deleteAllArts() {
		consoleLog.logMsg(showLogMsg,"->ArtsService->deleteAllArts","Entering","..>>...................");
		artsRepository.deleteAll();
		consoleLog.logMsg(showLogMsg,"->ArtsService->deleteAllArts","Exiting","...................>>..");
	}
	// ------------------------------------DEPS--------------------------------------------------------------	
	// ------------------------------------setNewDepTag--------------------------------------------------------------	
	// Based on DepName return DepTag with 4 capital letters from DepName, preferred first ones
	public String setNewDepTag(String newDepName) {	
		if (newDepName.length()<6) return null;
		String newDepNameUpper = newDepName.toUpperCase();
		String newDepTagIni = newDepNameUpper.substring(0,3);
		String newDepTag;
		Deps readDep;
		int firstChars = 4;
		int indexNextChar = 3;
		do {
			if (indexNextChar >= newDepNameUpper.length()) {
				indexNextChar = 3;
				firstChars = firstChars-1;
				newDepTagIni = newDepNameUpper.substring(0,firstChars-1)+newDepNameUpper.substring(firstChars,4);;
			}
			newDepTag = newDepTagIni+newDepNameUpper.substring(indexNextChar,indexNextChar+1);
			consoleLog.logMsg(showLogMsg,"->ArtsService->createOneDep","newDepTag...................>>..",newDepTag);
			readDep = depsRepository.findByDepTag(newDepTag);
			if (readDep == null) return newDepTag;
			indexNextChar = indexNextChar + 1;
		} while (firstChars > 1);
		
		return null;
	}
	// -------------------Deps----->getAllDeps---------------------------------------------------------------	
	@Transactional(readOnly = true)
	public List<Deps> getAllDeps() {
		consoleLog.logMsg(showLogMsg,"->ArtsService->getAllDeps","Entering","..>>...................");
		List<Deps> depsList = depsRepository.findAll();
		consoleLog.logMsg(showLogMsg,"->ArtsService->getAllDeps","Exiting","...................>>..");
		return depsList;
	}
	// -------------------Deps----->createOne------------------------------------------------
	@Transactional
	public Deps createOneDep(Deps newDep) throws EmptyFieldException, EntityAlreadyExistsException {
		consoleLog.logMsg(showLogMsg,"->ArtsService->createOneDep","Entering","..>>...................");
		// Checking null new object or empty mandatory attributes  
		if ((newDep==null) | (newDep.getDepName()=="")) 
			throw new EmptyFieldException("->ArtsService->createOneDep->Null Object or Empty Mandatoy Parameters");
		if (newDep.checkDepAtributes()==false) 
			throw new WrongParametersException("--Dep Name-->"+newDep.getDepName()+"--Dep Long Name-->"+newDep.getDepName());
		// Checking there no other object with same name 
		Deps readDep = depsRepository.findByDepName(newDep.getDepName());
		if (readDep != null) throw new EntityAlreadyExistsException("--Dep Name-->"+readDep.getDepName());
		// Assign a TAG with the 4 first letters of DepName and check if it exists
		String newDepTag = this.setNewDepTag(newDep.getDepName());
		if (newDepTag == null) throw new EntityAlreadyExistsException("--Dep TAG-->"+newDep.getDepName());
		newDep.setDepTag(newDepTag);
		if (newDep.checkDepAtributes()==false) 
					throw new WrongParametersException("--Dep Name-->"+newDep.getDepName()+"--Dep TAG-->"+newDep.getDepTag());
		readDep = depsRepository.save(newDep);
		consoleLog.logMsg(showLogMsg,"->ArtsService->createOneDep","Exiting","...................>>..");
		return readDep;
	}
	// -------------------Deps----->GetOne------------------------------------------------
	@Transactional
	public Deps getOneDep(Deps modDep) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->ArtsService->getOneDep","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((modDep == null) | (modDep.getDepName()=="")) throw new EmptyFieldException("->DepsService->getOneDep->Null Object or Empty Mandatoy Parameters");	
		Deps readDep = depsRepository.findByDepId(modDep.getDepId());
		if (readDep == null) throw new EntityNotFoundException("--Dep Name-->"+modDep.getDepName());
		consoleLog.logMsg(showLogMsg,"->ArtsService->getOneDep","Exiting","...................>>..");
		return readDep;
	}
	// -------------------Deps----->UpdateOne------------------------------------------------
	@Transactional
	public Deps updateOneDep(Deps modDep) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->ArtsService->updateOneDep","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((modDep == null) | (modDep.getDepName()=="")) throw new EmptyFieldException("->DepsService->updateOneDep->Null Object or Empty Mandatoy Parameters");	
		Deps readDep = depsRepository.findByDepId(modDep.getDepId());
		if (readDep == null) throw new EntityNotFoundException("--Dep Name-->"+modDep.getDepName());
		readDep = depsRepository.save(modDep);
		consoleLog.logMsg(showLogMsg,"->ArtsService->updateOneDep","Exiting","...................>>..");
		return readDep;
	}
	// -------------------Deps----->updateOneDepSpecial------------------------------------------------
	@Transactional
	public Deps updateOneDepSpecial(Deps modDep) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->ArtsService->updateOneDepSpecial","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((modDep == null) | (modDep.getDepName()=="")) throw new EmptyFieldException("->DepsService->updateOneDepSpecial->Null Object or Empty Mandatoy Parameters");	
		Deps readDep = depsRepository.findByDepId(modDep.getDepId());
		if (readDep == null) throw new EntityNotFoundException("--Dep Name-->"+modDep.getDepName());
		// Check if Dep name has been modified, then updated SubDeps of this Dep with new Dep Name
		String oldDepNameValue = readDep.getDepName();
		String newDepNameValue = modDep.getDepName();
		if ( oldDepNameValue.equals(newDepNameValue) ) { return readDep; }
		// Set Tag for newDepName
		String newDepTag = this.setNewDepTag(newDepNameValue);
		if (newDepTag == null) throw new EntityAlreadyExistsException("--Dep TAG-->"+modDep.getDepName());
		modDep.setDepTag(newDepTag);
		if (modDep.checkDepAtributes()==false) 
					throw new WrongParametersException("--Dep Name-->"+modDep.getDepName()+"--Dep TAG-->"+modDep.getDepTag());
		readDep = depsRepository.save(modDep);
		List<SubDeps> subDepsList = subDepsRepository.findAllBySubDepDepId(modDep.getDepId());
		SubDeps readSubDep;
		for(int j=0; j<subDepsList.size(); j++) {
			readSubDep = subDepsList.get(j);
			readSubDep.setSubDepDepName(newDepNameValue);
			subDepsRepository.save(readSubDep);
		}
		consoleLog.logMsg(showLogMsg,"->ArtsService->updateOneDepSpecial","Exiting","...................>>..");
		return readDep;
	}
	// -------------------Deps----->DeleteOne------------------------------------------------
	@Transactional
	public void deleteOneDep(Deps delDep) throws  DeleteNotAllowedException{
		consoleLog.logMsg(showLogMsg,"->ArtsService->deleteOneDep","Entering","..>>...................");
		// Check if Dep is having SudDep associated
		List<SubDeps> subDepsList = subDepsRepository.findAllBySubDepDepId(delDep.getDepId());
		if ((subDepsList.size() != 0)) {
			SubDeps readSubDep;
			String subDepsStringList = "..Department has SubDeparments--> ";
			for(int j=0; j<subDepsList.size(); j++) {
				readSubDep = subDepsList.get(j);
				subDepsStringList = subDepsStringList + readSubDep.getSubDepName() + "+";
				consoleLog.logMsg(showLogMsg,"->ArtsService->deleteOneDep","Dep has subDep",readSubDep.getSubDepName());
			}
			throw new DeleteNotAllowedException(delDep.getDepName()+subDepsStringList);
		}
		depsRepository.deleteById(delDep.getDepId());
		consoleLog.logMsg(showLogMsg,"->ArtsService->deleteOneDep","Exiting","...................>>..");
	}
	// -------------------Deps----->DeleteAll------------------------------------------------
	@Transactional
	public void deleteAllDeps(Deps delDep) {
		consoleLog.logMsg(showLogMsg,"->ArtsService->deleteAllDeps","Entering","..>>...................");
		// Read all Deps and delete only the ones that have no subdepartment 
		List<Deps> depsList = depsRepository.findAll();
		Deps readDep = new Deps();
		List<SubDeps> readSubDep;
		for(int j=0; j<depsList.size(); j++) {
			readDep = depsList.get(j);
			readSubDep = subDepsRepository.findAllBySubDepDepId(readDep.getDepId());
			if (readSubDep.size() == 0) depsRepository.deleteById(readDep.getDepId());
			else consoleLog.logMsg(showLogMsg,"->ArtsService->deleteAllDeps",readDep.getDepName()+"..Dep has subDep",readSubDep.get(0).getSubDepName());
		}
		consoleLog.logMsg(showLogMsg,"->ArtsService->deleteAllDeps","Exiting","...................>>..");
	}
	// ------------------------------------SUBDEPS--------------------------------------------------------------	
	// ------------------------------------setNewSubDepTag--------------------------------------------------------------	
	// Based on newSubDepName and depTag return subDepTag with 10 capital letters from DepTag, 4, and 5 from subDepName, preferred first ones
	public String setNewSubDepTag(SubDeps newSubDep) {	
		String newSubDepName = newSubDep.getSubDepName();
		if (newSubDepName.length()<6) return null;
		
		String newSubDepNameUpper = newSubDepName.toUpperCase();
		String newSubDepTagIni = newSubDepNameUpper.substring(0,3);
		String newSubDepTag;
		Deps readDep;
		int firstChars = 4;
		int indexNextChar = 3;
		do {
			if (indexNextChar >= newSubDepNameUpper.length()) {
				indexNextChar = 3;
				firstChars = firstChars-1;
				newSubDepTagIni = newSubDepNameUpper.substring(0,firstChars-1)+newSubDepNameUpper.substring(firstChars,4);;
			}
			newSubDepTag = newSubDepTagIni+newSubDepNameUpper.substring(indexNextChar,indexNextChar+1);
			consoleLog.logMsg(showLogMsg,"->ArtsService->createOneDep","newDepTag...................>>..",newSubDepTag);
			readDep = depsRepository.findByDepTag(newSubDepTag);
			if (readDep == null) return newSubDepTag;
			indexNextChar = indexNextChar + 1;
		} while (firstChars > 1);
		
		return null;
	}
	// -------------------SubDeps----->getAllSubDeps---------------------------------------------------------------	
	@Transactional(readOnly = true)
	public List<SubDeps> getAllSubDeps() {
		consoleLog.logMsg(showLogMsg,"->ArtsService->getAllSubDeps","Entering","..>>...................");
		List<SubDeps> subDepsList = subDepsRepository.findAll();
		consoleLog.logMsg(showLogMsg,"->ArtsService->getAllSubDeps","Exiting","...................>>..");
		return subDepsList;
	}
	// -------------------SubDeps----->createOne------------------------------------------------
	@Transactional
	public SubDeps createOneSubDep(SubDeps newSubDep) throws EmptyFieldException, EntityAlreadyExistsException {
		consoleLog.logMsg(showLogMsg,"->ArtsService->createOneSubDep","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((newSubDep==null) | (newSubDep.getSubDepName()=="")) 
			throw new EmptyFieldException("->ArtsService->createOneSubDep->Null Object or Empty Mandatoy Parameters");
		/* Checking attributes of new object */
		if (newSubDep.checkSubDepAtributes()==false) 
			throw new WrongParametersException("--SubDep Name-->"+newSubDep.getSubDepName()+"--SubDep Long Name-->"+newSubDep.getSubDepName());
		/* Checking there no other object with same name */
		SubDeps readSubDep = subDepsRepository.findBySubDepName(newSubDep.getSubDepName());
		if (readSubDep != null) throw new EntityAlreadyExistsException("--SubDep Name-->"+readSubDep.getSubDepName());
		/* Checking there no other object with same tag */
		readSubDep = subDepsRepository.findBySubDepTag(newSubDep.getSubDepTag());
		if (readSubDep != null) throw new EntityAlreadyExistsException("--SubDep TAG-->"+readSubDep.getSubDepName());
		readSubDep = subDepsRepository.save(newSubDep);
		// Create generic art type for created subdepartment
		this.createSubDepGenericArtType(readSubDep);
		consoleLog.logMsg(showLogMsg,"->ArtsService->createOneSubDep","Exiting","...................>>..");
		return readSubDep;
	}
	// -------------------SubDeps----->GetOne------------------------------------------------
	@Transactional
	public SubDeps getOneSubDep(SubDeps modSubDep) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->ArtsService->getOneSubDep","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((modSubDep==null) | (modSubDep.getSubDepName()=="")) throw new EmptyFieldException("->SubDepsService->getOneSubDep->Null Object or Empty Mandatoy Parameters");	
		SubDeps readSubDep = subDepsRepository.findBySubDepId(modSubDep.getSubDepId());
		if (readSubDep == null) throw new EntityNotFoundException("--SubDep Name-->"+modSubDep.getSubDepName());
		consoleLog.logMsg(showLogMsg,"->ArtsService->getOneSubDep","Exiting","...................>>..");
		return readSubDep;
	}
	// -------------------SubDeps----->UpdateOne------------------------------------------------
	@Transactional
	public SubDeps updateOneSubDep(SubDeps modSubDep) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->ArtsService->updateOneSubDep","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((modSubDep==null) | (modSubDep.getSubDepName()=="")) throw new EmptyFieldException("->SubDepsService->updateOneSubDep->Null Object or Empty Mandatoy Parameters");	
		SubDeps readSubDep = subDepsRepository.findBySubDepId(modSubDep.getSubDepId());
		if (readSubDep == null) throw new EntityNotFoundException("--Prov Name-->"+modSubDep.getSubDepName());
		readSubDep = subDepsRepository.save(modSubDep);
		consoleLog.logMsg(showLogMsg,"->ArtsService->updateOneSubDep","Exiting","...................>>..");
		return readSubDep;
	}
	// -------------------SubDeps----->updateOneSubDepSpecial------------------------------------------------
	@Transactional
	public SubDeps updateOneSubDepSpecial(SubDeps modSubDep) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->ArtsService->updateOneSubDepSpecial","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((modSubDep==null) | (modSubDep.getSubDepName()=="")) throw new EmptyFieldException("->SubDepsService->updateOneSubDepSpecial->Null Object or Empty Mandatoy Parameters");	
		SubDeps readSubDep = subDepsRepository.findBySubDepId(modSubDep.getSubDepId());
		if (readSubDep == null) throw new EntityNotFoundException("--Prov Name-->"+modSubDep.getSubDepName());
		// Check if SubDepName and SubDepTaghas been modified, then updated ArtTypes in first case, and Provs, Facts, FactModels in second case
		String oldSubDepNameValue = readSubDep.getSubDepName();
		String newSubDepNameValue = modSubDep.getSubDepName();
		String oldSubDepTagValue  = readSubDep.getSubDepTag();
		String newSubDepTagValue  = modSubDep.getSubDepTag();	
		readSubDep = subDepsRepository.save(modSubDep);
		// SubDepName changed, update ArtTypes with this new SubDepName
		if ( oldSubDepNameValue.equals(newSubDepNameValue) != true ) {  
			consoleLog.logMsg(showLogMsg,"->ArtsService->updateOneSubDepSpecial",".....Updating ArtTpyes","...................>>..");
			List<ArtTypes> artTypesList = artTypesRepository.findAllByArtTypeSubDepId(modSubDep.getSubDepId());
			ArtTypes readArtType;
			for(int j=0; j<artTypesList.size(); j++) {
				readArtType = artTypesList.get(j);
				readArtType.setArtTypeSubDepName(newSubDepNameValue);
				artTypesRepository.save(readArtType);
			}			
		}
		// SubDepTag changed, update Provs, Facts and FactModels with this new SubDepTag
		if ( oldSubDepTagValue.equals(newSubDepTagValue) != true ) {  
			consoleLog.logMsg(showLogMsg,"->ArtsService->updateOneSubDepSpecial",".....Updating Provs","...................>>..");
			List<Provs> provsList = provsRepository.findAllByProvFactType(modSubDep.getSubDepTag());
			Provs readProv;
			for(int j=0; j<provsList.size(); j++) {
				readProv = provsList.get(j);
				readProv.setProvFactType(newSubDepTagValue);
				provsRepository.save(readProv);
			}	
			consoleLog.logMsg(showLogMsg,"->ArtsService->updateOneSubDepSpecial",".....Updating Facts","...................>>..");
			List<Facts> factsList = factsRepository.findAllByFactType(modSubDep.getSubDepTag());
			Facts readFact;
			for(int j=0; j<factsList.size(); j++) {
				readFact = factsList.get(j);
				readFact.setFactType(newSubDepTagValue);
				factsRepository.save(readFact);
			}	
			consoleLog.logMsg(showLogMsg,"->ArtsService->updateOneSubDepSpecial",".....Updating FactModels","...................>>..");
			List<FactModels> factModelsList = factModelsRepository.findAllByFactModelsFactType(modSubDep.getSubDepTag());
			FactModels readFactModel;
			for(int j=0; j<factModelsList.size(); j++) {
				readFactModel = factModelsList.get(j);
				readFactModel.setFactModelsFactType(newSubDepTagValue);
				factModelsRepository.save(readFactModel);
			}
		}		
		consoleLog.logMsg(showLogMsg,"->ArtsService->updateOneSubDepSpecial","Exiting","...................>>..");
		return readSubDep;
	}
	// -------------------SubDeps----->DeleteOne------------------------------------------------
	@Transactional
	public void deleteOneSubDep(SubDeps delSubDep) {
		consoleLog.logMsg(showLogMsg,"->ArtsService->deleteOneSubDep","Entering","..>>...................");
		// Check if SubDep is having ArtType associated
		List<ArtTypes> artTypesList = artTypesRepository.findAllByArtTypeSubDepId(delSubDep.getSubDepId());
		if ((artTypesList.size() != 0)) {
			ArtTypes readArtType;
			String artTypesStringList = "..SubDepartment has ArtTypes--> ";
			for(int j=0; j<artTypesList.size(); j++) {
				readArtType = artTypesList.get(j);
				artTypesStringList = artTypesStringList + readArtType.getArtTypeName() + "+";
				consoleLog.logMsg(showLogMsg,"->ArtsService->deleteOneSubDep","SubDep has ArtTypes",readArtType.getArtTypeName());
			}
			throw new DeleteNotAllowedException(delSubDep.getSubDepName()+artTypesStringList);
		}
		subDepsRepository.deleteById(delSubDep.getSubDepId());
		consoleLog.logMsg(showLogMsg,"->ArtsService->deleteOneSubDep","Exiting","...................>>..");
	}
	// -------------------SubDeps----->DeleteAll------------------------------------------------
	@Transactional
	public void deleteAllSubDeps(SubDeps delSubDep) {
		consoleLog.logMsg(showLogMsg,"->ArtsService->deleteAllSubDeps","Entering","..>>...................");
		// Read all SubDeps and delete only the ones that have no ArtTypes 
		List<SubDeps> subDepsList = subDepsRepository.findAll();
		SubDeps readSubDep = new SubDeps();
		List<ArtTypes> readArtTypes;
		for(int j=0; j<subDepsList.size(); j++) {
			readSubDep = subDepsList.get(j);
			readArtTypes = artTypesRepository.findAllByArtTypeSubDepId(readSubDep.getSubDepId());
			if (readArtTypes.size() == 0) subDepsRepository.deleteById(readSubDep.getSubDepId());
			else consoleLog.logMsg(showLogMsg,"->ArtsService->deleteAllSubDeps",readSubDep.getSubDepName()+"..SubDep has ArtTypes",readArtTypes.get(0).getArtTypeName());
		}			
		consoleLog.logMsg(showLogMsg,"->ArtsService->deleteAllSubDeps","Exiting","...................>>..");
	}		
	// -------------------SubDeps----->getAllSubDepsInDep---------------------------------------------------------------	
	@Transactional(readOnly = true)
	public List<SubDeps> getAllSubDepsInDep(Deps selectedDep) {
		consoleLog.logMsg(showLogMsg,"->ArtsService->getAllSubDepsInDep","Entering","..>>...................");
		List<SubDeps> SubDepsList = subDepsRepository.findAllBySubDepDepId(selectedDep.getDepId());
		consoleLog.logMsg(showLogMsg,"->ArtsService->getAllSubDepsInDep","Exiting","...................>>..");
		return SubDepsList;
	}	
	// ------------------------------------ARTTYPES--------------------------------------------------------------	
	// -------------------ArtTypes----->getAllArtTypes---------------------------------------------------------------	
	@Transactional(readOnly = true)
	public List<ArtTypes> getAllArtTypes() {
		consoleLog.logMsg(showLogMsg,"->ArtsService->getAllArtTypes","Entering","..>>...................");
		List<ArtTypes> artTypesList = artTypesRepository.findAll();
		consoleLog.logMsg(showLogMsg,"->ArtsService->getAllArtTypes","Exiting","...................>>..");
		return artTypesList;
	}
	// -------------------ArtTypes----->createOne------------------------------------------------
	@Transactional
	public ArtTypes createOneArtType(ArtTypes newArtType) throws EmptyFieldException, EntityAlreadyExistsException {
		consoleLog.logMsg(showLogMsg,"->ArtsService->createOneArtType","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((newArtType==null) | (newArtType.getArtTypeName()=="")) 
			throw new EmptyFieldException("->ArtsService->createOneArtType->Null Object or Empty Mandatoy Parameters");
		/* Checking attributes of new object */
		if (newArtType.checkArtTypeAtributes()==false) 
			throw new WrongParametersException("--ArtType Name-->"+newArtType.getArtTypeName()+"--ArtType Long Name-->"+newArtType.getArtTypeName());
		/* Checking there no other object with same name */
		ArtTypes readArtType = artTypesRepository.findByArtTypeName(newArtType.getArtTypeName());
		if (readArtType != null) throw new EntityAlreadyExistsException("--ArtType Name-->"+readArtType.getArtTypeName());
		/* Checking there no other object with same reference */
		readArtType = artTypesRepository.save(newArtType);
		consoleLog.logMsg(showLogMsg,"->ArtsService->createOneArtType","Exiting","...................>>..");
		return readArtType;
	}
	// -------------------ArtTypes----->GetOne------------------------------------------------
	@Transactional
	public ArtTypes getOneArtType(ArtTypes modArtType) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->ArtsService->getOneArtType","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((modArtType==null) | (modArtType.getArtTypeName()=="")) throw new EmptyFieldException("->ArtTypesService->getOneArtType->Null Object or Empty Mandatoy Parameters");	
		ArtTypes readArtType = artTypesRepository.findByArtTypeId(modArtType.getArtTypeId());
		if (readArtType == null) throw new EntityNotFoundException("--ArtType Name-->"+modArtType.getArtTypeName());
		consoleLog.logMsg(showLogMsg,"->ArtsService->getOneArtType","Exiting","...................>>..");
		return readArtType;
	}
	// -------------------ArtTypes----->UpdateOne------------------------------------------------
	@Transactional
	public ArtTypes updateOneArtType(ArtTypes modArtType) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->ArtsService->updateOneArtType","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((modArtType==null) | (modArtType.getArtTypeName()=="")) throw new EmptyFieldException("->ArtTypesService->updateOneArtType->Null Object or Empty Mandatoy Parameters");	
		ArtTypes readArtType = artTypesRepository.findByArtTypeId(modArtType.getArtTypeId());
		if (readArtType == null) throw new EntityNotFoundException("--Prov Name-->"+modArtType.getArtTypeName());
		readArtType = artTypesRepository.save(modArtType);
		consoleLog.logMsg(showLogMsg,"->ArtsService->updateOneArtType","Exiting","...................>>..");
		return readArtType;
	}
	// -------------------ArtTypes----->updateOneArtTypeSpecial------------------------------------------------
	@Transactional
	public ArtTypes updateOneArtTypeSpecial(ArtTypes modArtType) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->ArtsService->updateOneArtTypeSpecial","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((modArtType==null) | (modArtType.getArtTypeName()=="")) throw new EmptyFieldException("->ArtTypesService->updateOneArtTypeSpecial->Null Object or Empty Mandatoy Parameters");	
		ArtTypes readArtType = artTypesRepository.findByArtTypeId(modArtType.getArtTypeId());
		if (readArtType == null) throw new EntityNotFoundException("--Prov Name-->"+modArtType.getArtTypeName());
		// leo el ArtType antes de actualizarlo para buscar los articulos que tenian asignado el tag antes de modificar este arttype
		readArtType = artTypesRepository.findByArtTypeId(modArtType.getArtTypeId());
		String oldArtTypeTagValue = readArtType.getArtTypeTag();
		String newArtTypeTagValue = modArtType.getArtTypeTag();
		// actualizo la base de datos con el ArtType modificado
		readArtType = artTypesRepository.save(modArtType);	
		// si el tag del ArtType no ha cambiado, hemos terminado
		if ( oldArtTypeTagValue.equals(newArtTypeTagValue) ) { return readArtType; }
		// si el tag del artType si que ha cambiado, lo actualizamos en todos los Arts y ArtsFacts que utilizaban el antiguo tag
		List<Arts> readArtsList = artsRepository.findAllByArtType(oldArtTypeTagValue);
		Arts readArt,checkArt;
		for(int k=0; k<readArtsList.size(); k++) {
			readArt = readArtsList.get(k);
			readArt.setArtType(newArtTypeTagValue);
			checkArt = artsRepository.save(readArt);
			consoleLog.logMsg(showLogMsg,"->ArtsService->updateOneArtTypeSpecial","Updating arts....>>>",readArt.getArtId().toString());
			// Leemos todos los ArtsFact de este Art y actualizamos el ArtType
			List<ArtFacts> readArtsFactList = artsFactRepository.findAllByArtFactArtId(readArt.getArtId());
			ArtFacts readArtFact;
			for(int j=0; j<readArtsFactList.size(); j++) {
				readArtFact = readArtsFactList.get(j);
				readArtFact.setArtFactArtType(newArtTypeTagValue);
				artsFactRepository.save(readArtFact);
				consoleLog.logMsg(showLogMsg,"->ArtsService->updateOneArtTypeSpecial","Updating artFact....",readArtFact.getArtFactId().toString());
			}		
		}
		consoleLog.logMsg(showLogMsg,"->ArtsService->updateOneArtTypeSpecial","Exiting","...................>>..");
		return readArtType;
	}
	// -------------------ArtTypes----->DeleteOne------------------------------------------------
	@Transactional
	public void deleteOneArtType(ArtTypes delArtType) {
		consoleLog.logMsg(showLogMsg,"->ArtsService->deleteOneArtType","Entering","..>>...................");
		// Check if ArtType is being used in any Art
		List<Arts> artsList = artsRepository.findAllByArtType(delArtType.getArtTypeTag() );
		if ((artsList.size() != 0)) {
			Arts readArt;
			String artsStringList = "..ArtType in Arts--> ";
			for(int j=0; j<artsList.size(); j++) {
				readArt = artsList.get(j);
				artsStringList = artsStringList + readArt.getArtName() + "+";
				consoleLog.logMsg(showLogMsg,"->ArtsService->deleteOneSubDep","ArtType has Arts",readArt.getArtName());
			}
			throw new DeleteNotAllowedException(delArtType.getArtTypeName()+artsStringList);
		}
		artTypesRepository.deleteById(delArtType.getArtTypeId());
		consoleLog.logMsg(showLogMsg,"->ArtsService->deleteOneArtType","Exiting","...................>>..");
	}
	// -------------------ArtTypes----->DeleteAll------------------------------------------------
	@Transactional
	public void deleteAllArtTypes(ArtTypes delArtType) {
		consoleLog.logMsg(showLogMsg,"->ArtsService->deleteAllArtTypes","Entering","..>>...................");
		// Read all ArtTypes and delete only the ones that are not being used in any Art 
		List<ArtTypes> artTypesList = artTypesRepository.findAll();
		ArtTypes readArtType = new ArtTypes();
		List<Arts> readArts;
		for(int j=0; j<artTypesList.size(); j++) {
			readArtType = artTypesList.get(j);
			readArts = artsRepository.findAllByArtType(readArtType.getArtTypeTag());
			if (readArts.size() == 0) artTypesRepository.deleteById(readArtType.getArtTypeId());
			else consoleLog.logMsg(showLogMsg,"->ArtsService->deleteAllSubDeps",readArtType.getArtTypeName()+"..ArtType in Arts",readArts.get(0).getArtName());
		}
		consoleLog.logMsg(showLogMsg,"->ArtsService->deleteAllArtTypes","Exiting","...................>>..");
	}		
	// -------------------ArtTypes----->getAllArtTypesInSubDep---------------------------------------------------------------	
	@Transactional(readOnly = true)
	public List<ArtTypes> getAllArtTypesInSubDep(SubDeps selectedSubDep) {
		consoleLog.logMsg(showLogMsg,"->ArtsService->getAllArtTypesInSubDep","Entering","..>>...................");
		List<ArtTypes> artTypesList = artTypesRepository.findAllByArtTypeSubDepId(selectedSubDep.getSubDepId());
		consoleLog.logMsg(showLogMsg,"->ArtsService->getAllArtTypesInSubDep","Exiting","...................>>..");
		return artTypesList;
	}		
	// -------------------ArtTypes----->checkGenericArtTypes---------------------------------------------------------------	
	@Transactional
	public List<ArtTypes> checkGenericArtTypes() {
		consoleLog.logMsg(showLogMsg,"->ArtsService->checkGenericArtTypes","Entering","..>>...................");
		// get all subdepartments
		List<SubDeps> subDepsList = subDepsRepository.findAll();
		// check if exist the generic art type of every subdepartment, if not, create generic art type
		SubDeps readSubDep = new SubDeps();
		for(int j=0; j<subDepsList.size(); j++) {
			readSubDep = subDepsList.get(j);
			// consoleLog.logMsg(showLogMsg,"->ArtsService->checkGenericArtTypes","readSubDep",readSubDep.getSubDepDepName());
			this.createSubDepGenericArtType(readSubDep);
		}
		List<ArtTypes> ArtTypesList = artTypesRepository.findAll();
		consoleLog.logMsg(showLogMsg,"->ArtsService->checkGenericArtTypes","Exiting","...................>>..");
		return ArtTypesList;
	}
	// -------------------ArtTypes----->createSubDepGenericArtType---------------------------------------------------------------	
	public void createSubDepGenericArtType(SubDeps readSubDep) {
		consoleLog.logMsg(showLogMsg,"->ArtsService->createSubDepGenericArtType","Entering","..>>...................");
		Date currentDate = new Date();
		String artTypeGenTag = readSubDep.getSubDepTag()+"-GEN";
		ArtTypes readArtType = artTypesRepository.findByArtTypeTag(artTypeGenTag);
		if (readArtType == null) {
			readArtType = new ArtTypes();
			readArtType.setArtTypeDate(currentDate);
			String artTypeGenDescription = readSubDep.getSubDepDescription()+"-Generic";
			if(artTypeGenDescription.length() > 64 ){
				artTypeGenDescription = artTypeGenDescription.substring(0, 64);
			}
			readArtType.setArtTypeDescription(artTypeGenDescription);
			String artTypeGenName = readSubDep.getSubDepName()+"-Generic";
			if(artTypeGenName.length() > 32 ){
				artTypeGenName = artTypeGenName.substring(0, 32);
			}
			readArtType.setArtTypeName(artTypeGenName);
			readArtType.setArtTypeSequence(readSubDep.getSubDepSequence());				
			readArtType.setArtTypeStatus(true);
			readArtType.setArtTypeSubDepId(readSubDep.getSubDepId());
			readArtType.setArtTypeSubDepName(readSubDep.getSubDepName());
			readArtType.setArtTypeTag(artTypeGenTag);
			/* Checking attributes of new object */
			if (readArtType.checkArtTypeAtributes()==false) 
				throw new WrongParametersException("--ArtType Name-->"+readArtType.getArtTypeName()+"--ArtType Long Name-->"+readArtType.getArtTypeName());
			// consoleLog.logMsg(showLogMsg,"->ArtsService->createSubDepGenericArtType","readArtType",readArtType.getArtTypeTag());
			artTypesRepository.save(readArtType);
		}
		consoleLog.logMsg(showLogMsg,"->ArtsService->createSubDepGenericArtType","Exiting","..>>...................");

	}
// ------------------------------------INITIALIZE ARTS DATE--------------------------------------------------------------	
	// -------------------Arts----->setArtsCurrentDate---------------------------------------------------------------	
	@Transactional
	public void setArtsCurrentDate() {
		consoleLog.logMsg(showLogMsg,"->ArtsService->setArtsCurrentDate","Entering","..>>...................");
		Date currentDate = new Date();
		consoleLog.logMsg(showLogMsg,"->ArtsService->setArtsCurrentDate","...currentDate...",currentDate.toString());
		Arts readArt = new Arts();
		List<Arts> artsList = artsRepository.findAll();
		for(int j=0; j<artsList.size(); j++) {
			readArt = artsList.get(j);	
			if (readArt.getArtDate() == null) {
				readArt.setArtDate(currentDate);
				artsRepository.save(readArt);			
			}
		}		
		consoleLog.logMsg(showLogMsg,"->ArtsService->setArtsCurrentDate","Exiting","...................>>..");
		return ;
	}
}
