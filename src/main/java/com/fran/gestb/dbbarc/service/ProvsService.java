package com.fran.gestb.dbbarc.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fran.gestb.aacommon.ConsoleLog;
import com.fran.gestb.aaexceptions.EmptyFieldException;
import com.fran.gestb.aaexceptions.EntityAlreadyExistsException;
import com.fran.gestb.aaexceptions.EntityNotFoundException;
import com.fran.gestb.aaexceptions.UserDoesNotExistException;
import com.fran.gestb.aaexceptions.WrongParametersException;
import com.fran.gestb.dbbarc.model.Arts;
import com.fran.gestb.dbbarc.model.FactModels;
import com.fran.gestb.dbbarc.model.MProvs;
import com.fran.gestb.dbbarc.model.Provs;
import com.fran.gestb.dbbarc.model.SubDeps;
import com.fran.gestb.dbbarc.repo.MProvsRepository;
import com.fran.gestb.dbbarc.repo.FactModelsRepository;
import com.fran.gestb.dbbarc.repo.ProvsRepository;
import com.fran.gestb.dbbarc.repo.SubDepsRepository;
import com.fran.gestb.dbbarxx.model.Facts;
import com.fran.gestb.dbbarxx.repo.FactsRepository;

@Service
public class ProvsService {
	
	@Autowired
	private ProvsRepository provsRepository;
	@Autowired
	private MProvsRepository moreProvsRepository;
	@Autowired
	private FactsRepository factsRepository;
	@Autowired
	FactModelsRepository factModelsRepository;
	@Autowired
	private SubDepsRepository subDepsRepository;
	@Autowired
	private ConsoleLog consoleLog; 
	private boolean showLogMsg = true;
	// -------------------Provs----->getAllProvs-------------------------------------------	
	@Transactional(readOnly = true)
	public List<Provs> getAllProvs() {
		consoleLog.logMsg(showLogMsg,"->ProvsService->getAllProvs","Entering","..>>...................");
		List<Provs> provsList = provsRepository.findAll();
		consoleLog.logMsg(showLogMsg,"->ProvsService->getAllProvs","Exiting","...................>>..");
		return provsList;
	}
	// -------------------Provs----->getAllProvsWithFactType-------------------------------------------	
	@Transactional(readOnly = true)
	public List<Provs> getAllProvsWithFactType(String selFactType) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->ProvsService->getAllProvsWithFactType","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((selFactType==null) | (selFactType=="")) throw new EmptyFieldException("->ProvsService->getAllProvsWithFactType->Null Object or Empty Mandatoy Parameters");		
		// Read SubDep with this subDepName or FactType
		SubDeps readSubDep = subDepsRepository.findBySubDepName(selFactType);
		if (readSubDep == null) throw new EntityNotFoundException("--SubDep Not Found-->"+selFactType);						
		List<Provs> provsList = provsRepository.findAllByProvFactType(readSubDep.getSubDepTag());
		consoleLog.logMsg(showLogMsg,"->ProvsService->getAllProvsWithFactType","Exiting","...................>>..");
		return provsList;
	}
	// -------------------Provs----->getAllProvsWithPayType-------------------------------------------	
	@Transactional(readOnly = true)
	public List<Provs> getAllProvsWithPayType(String selPayType) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->ProvsService->getAllProvsWithPayType","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((selPayType==null) | (selPayType=="")) throw new EmptyFieldException("->ProvsService->getAllProvsWithPayType->Null Object or Empty Mandatoy Parameters");		
		// Read Provs with this PayType				
		List<Provs> provsList = provsRepository.findAllByProvPayType(selPayType);
		consoleLog.logMsg(showLogMsg,"->ProvsService->getAllProvsWithPayType","Exiting","...................>>..");
		return provsList;
	}
	// -------------------Provs----->getAllProvsWithFactOrNote-------------------------------------------	
	@Transactional(readOnly = true)
	public List<Provs> getAllProvsWithFactOrNote(String factOrNote) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->ProvsService->getAllProvsWithFactOrNote","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((factOrNote==null) | (factOrNote=="")) throw new EmptyFieldException("->ProvsService->getAllProvsWithFactOrNote->Null Object or Empty Mandatoy Parameters");		
		// Read Provs with this PayType
		boolean fact = true;
		if ( factOrNote.equalsIgnoreCase("Nota") ) fact = false;
		List<Provs> provsList = provsRepository.findAllByProvFactNote(fact);
		consoleLog.logMsg(showLogMsg,"->ProvsService->getAllProvsWithFactOrNote","Exiting","...................>>..");
		return provsList;
	}
	// -------------------Provs----->saveProv------------------------------------------------
	@Transactional
	public Provs saveProv(Provs newProv) throws EmptyFieldException, EntityAlreadyExistsException {
		consoleLog.logMsg(showLogMsg,"->ProvsService->saveProv","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((newProv==null) | (newProv.getProvName()=="")) throw new EmptyFieldException("->ProvsService->saveProv->Null Object or Empty Mandatoy Parameters");
		/* Checking attributes of new object */
		if (newProv.checkProvAtributes()==false) throw new 
				WrongParametersException("--Prov Name-->"+newProv.getProvName()+"--Prov Long Name-->"+newProv.getProvLongName());
		/* Checking there no other object with same name */
		Provs readProv = provsRepository.findByProvName(newProv.getProvName());
		if (readProv != null) throw new EntityAlreadyExistsException("--Prov Name-->"+readProv.getProvName());
		readProv = provsRepository.save(newProv);
		consoleLog.logMsg(showLogMsg,"->ProvsService->saveProv","Exiting","...................>>..");
		return readProv;
	}
	// -------------------Provs----->getOneProv------------------------------------------------
	@Transactional
	public Provs getOneProv(Provs modProv) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->ProvsService->getOneProv","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((modProv==null) | (modProv.getProvName()=="")) throw new EmptyFieldException("->ProvsService->getOneProv->Null Object or Empty Mandatoy Parameters");	
		Provs readProv = provsRepository.findByProvId(modProv.getProvId());
		if (readProv == null) throw new EntityNotFoundException("--Prov Name-->"+modProv.getProvName());
		consoleLog.logMsg(showLogMsg,"->ProvsService->getOneProv","Exiting","...................>>..");
		return readProv;
	}
	// -------------------Provs----->getOneProvById------------------------------------------------
	@Transactional
	public Provs getOneProvById(long updProvId) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->ProvsService->getOneProvById","Entering","..>>...................");
		Provs readProv = provsRepository.findByProvId(updProvId);
		//if (readProv == null) throw new EntityNotFoundException("--Prov Id not found-->");
		consoleLog.logMsg(showLogMsg,"->ProvsService->getOneProvById","Exiting","...................>>..");
		return readProv;
	}
	// -------------------Provs----->updateOneProv------------------------------------------------
	@Transactional
	public Provs updateOneProv(Provs modProv) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->ProvsService->updateOneProv","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((modProv==null) | (modProv.getProvName()=="")) throw new EmptyFieldException("->ProvsService->updateOneProv->Null Object or Empty Mandatoy Parameters");	
		Provs readProv = provsRepository.findByProvId(modProv.getProvId());
		if (readProv == null) throw new EntityNotFoundException("--Prov Name-->"+modProv.getProvName());		
		readProv = provsRepository.save(modProv);
		consoleLog.logMsg(showLogMsg,"->ProvsService->updateOneProv","Exiting","...................>>..");
		return readProv;
	}
	// -------------------Provs----->updateOneProvSpecial------------------------------------------------
	@Transactional
	public Provs updateOneProvSpecial(Provs modProv) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->ProvsService->updateOneProvSpecial","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((modProv==null) | (modProv.getProvName()=="")) throw new EmptyFieldException("->ProvsService->updateOneProvSpecial->Null Object or Empty Mandatoy Parameters");	
		Provs readProv = provsRepository.findByProvId(modProv.getProvId());
		if (readProv == null) throw new EntityNotFoundException("--Prov Name-->"+modProv.getProvName());
		// Check if ProvName and ProvFactType have been modified, then update Facts, FactModels
		String oldProvNameValue = readProv.getProvName();
		String newProvNameValue = modProv.getProvName();
		String oldProvFactTypeValue = readProv.getProvFactType();
		String newProvFactTypeValue = modProv.getProvFactType();				
		readProv = provsRepository.save(modProv);
		// ProvName and/or ProvFactType changed, update Facts, FactModels
		if ( (oldProvNameValue.equals(newProvNameValue) != true) || (oldProvFactTypeValue.equals(newProvFactTypeValue) != true) ){  
			consoleLog.logMsg(showLogMsg,"->ProvsService->updateOneProvSpecial",".....Updating Facts","...................>>..");
			List<Facts> factsList = factsRepository.findByFactProvId(readProv.getProvId());
			Facts readFact;
			for(int j=0; j<factsList.size(); j++) {
				readFact = factsList.get(j);
				readFact.setFactProvName(newProvNameValue);
				readFact.setFactType(newProvFactTypeValue);
				factsRepository.save(readFact);
			}						
			consoleLog.logMsg(showLogMsg,"->ProvsService->updateOneProvSpecial",".....Updating FactModels","...................>>..");
			List<FactModels> factModelsList = factModelsRepository.findByFactModelsProvId(readProv.getProvId());
			FactModels readFactModel;
			for(int j=0; j<factModelsList.size(); j++) {
				readFactModel = factModelsList.get(j);
				readFactModel.setFactModelsProvName(newProvNameValue);
				readFactModel.setFactModelsFactType(newProvFactTypeValue);
				factModelsRepository.save(readFactModel);
			}
		}		
		consoleLog.logMsg(showLogMsg,"->ProvsService->updateOneProvSpecial","Exiting","...................>>..");
		return readProv;
	}
	// -------------------Provs----->deleteOneProv------------------------------------------------
	@Transactional
	public void deleteOneProv(Provs delProv) {
		consoleLog.logMsg(showLogMsg,"->ProvsService->deleteOneProv","Entering","..>>...................");
		/* Check if the field exist */
		Provs checkProv = provsRepository.findByProvId(delProv.getProvId());
		if (checkProv == null) throw new UserDoesNotExistException("Field does NOT exists-->");
		/* Check if there are facts of this prov */
		List<Facts> fatcsProv = factsRepository.findByFactProvId(delProv.getProvId());
		if (fatcsProv != null) 
			if (fatcsProv.size() != 0) throw 
				new EntityAlreadyExistsException("--NO SE puede borrar el proveedor, ya tiene factura-->"+delProv.getProvName());
		/* Delete all model facts of this prov */
		factModelsRepository.deleteByFactModelsProvId(delProv.getProvId());
		/* delete all more date associated to this prov */
		deleteAllMoreProvs(delProv);
		/* delete prov */
		provsRepository.deleteById(delProv.getProvId());
		consoleLog.logMsg(showLogMsg,"->ProvsService->deleteOneProv","Exiting","...................>>..");
	}
	// -------------------Provs----->deleteAllProvs------------------------------------------------
	@Transactional
	public void deleteAllProvs() {
		consoleLog.logMsg(showLogMsg,"->ProvsService->deleteAllProvs","Entering","..>>...................");
		deleteAll();
		provsRepository.deleteAll();
		consoleLog.logMsg(showLogMsg,"->ProvsService->deleteAllProvs","Exiting","...................>>..");
	}
	// -------------------More Provs----------------------------------------------------------------------------------	

	// -------------------More Provs----->getAllMoreProv-------------------------------------------	
	@Transactional
	public List<MProvs> getAllMoreProv(Provs newProv) {
		consoleLog.logMsg(showLogMsg,"->ProvsService->getAllMoreProv","Entering","..>>...................");
		/* Checking prov with provId exist */
		Provs readProv = provsRepository.findByProvId(newProv.getProvId());
		if (readProv == null) throw new EntityNotFoundException("--Prov Id-->"+newProv.getProvId());						
		List<MProvs> mProvsList = moreProvsRepository.findByMprovProvId(readProv.getProvId());
		consoleLog.logMsg(showLogMsg,"->ProvsService->getAllMoreProv","Exiting","...................>>..");
		return mProvsList;
	}
	// -------------------More Provs----->createMoreProvList------------------------------------------------
	@Transactional
	public List<MProvs> createMoreProvList(List<MProvs> newMoreProvList) throws EmptyFieldException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->ProvsService->createMoreProvList","Entering","..>>...................");		
		/* Checking null new object or empty mandatory attributes  */
		if (newMoreProvList == null) throw new EmptyFieldException("->ProvsService->createMoreProvList->Null Object");
		/* Checking prov with provId exist */
		Provs readProv = provsRepository.findByProvId(newMoreProvList.get(0).getMprovProvId());
		if (readProv == null) throw new EntityNotFoundException("--Prov Id-->"+newMoreProvList.get(0).getMprovProvId());
		List<MProvs> createdMoreProveList = new ArrayList<MProvs>();
		for (int j=0; j<newMoreProvList.size(); j++ ) {
			if (newMoreProvList.get(j).getMprovType()=="") throw new EmptyFieldException("->ProvsService->createMoreProvList->Empty Mandatoy Parameters");		
			/* Checking attributes of new object */
			if (newMoreProvList.get(j).checkMProvAtributes()==false) throw new 
					WrongParametersException("--MProv Type-->"+newMoreProvList.get(j).getMprovType()+"--MProv Data1-->"+newMoreProvList.get(j).getMprovData1());				
			/* Create More Prov Data */
			MProvs createdMoreProv = moreProvsRepository.save(newMoreProvList.get(j));	
			if (createdMoreProv == null) throw new EntityNotFoundException("--Prov Id-->"+newMoreProvList.get(j).getMprovProvId());
			createdMoreProveList.add(createdMoreProv);
		}
		consoleLog.logMsg(showLogMsg,"->ProvsService->createMoreProvList","Exiting","...................>>..");
		return createdMoreProveList;
	}
	// -------------------More Provs----->updateMoreProvList------------------------------------------------
	@Transactional
	public List<MProvs> updateMoreProvList(List<MProvs> updateMoreProvList) throws EmptyFieldException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->ProvsService->updateMoreProvList","Entering","..>>...................");		
		/* Checking null new object or empty mandatory attributes  */
		if (updateMoreProvList == null) throw new EmptyFieldException("->ProvsService->updateMoreProvList->Null Object");
		/* Checking prov with provId exist */
		Provs readProv = provsRepository.findByProvId(updateMoreProvList.get(0).getMprovProvId());
		if (readProv == null) throw new EntityNotFoundException("--Prov Id-->"+updateMoreProvList.get(0).getMprovProvId());
		moreProvsRepository.deleteAllByMprovProvId(readProv.getProvId());
		if (updateMoreProvList.size() == 0) return null;
		List<MProvs> updatedMoreProveList = createMoreProvList(updateMoreProvList);
		consoleLog.logMsg(showLogMsg,"->ProvsService->updateMoreProvList","Exiting","...................>>..");
		return updatedMoreProveList;
	}	
	// -------------------More Provs----->deleteAllMoreProvs------------------------------------------------
	@Transactional
	public void deleteAllMoreProvs(Provs delProv) throws UserDoesNotExistException {
		consoleLog.logMsg(showLogMsg,"->ProvsService->deleteAllMoreProvs","Entering","..>>...................");
		// Check if the prov exist 
		Provs checkProv = provsRepository.findByProvId(delProv.getProvId());
		if (checkProv == null) throw new UserDoesNotExistException("Field does NOT exists-->");
		// Read all more prov data of received prov
		List<MProvs> moreProvsList = moreProvsRepository.findByMprovProvId(delProv.getProvId());
		// Delete all more prov data one by one 
		for(int j = 0;j<moreProvsList.size();j++) {
			moreProvsRepository.deleteById(moreProvsList.get(j).getMprovId());
		}
		consoleLog.logMsg(showLogMsg,"->ProvsService->deleteAllMoreProvs","Exiting","...................>>..");
	}
	// -------------------More Provs----->deleteAllMoreProvs------------------------------------------------
	@Transactional
	public void deleteAll() throws UserDoesNotExistException {
		consoleLog.logMsg(showLogMsg,"->ProvsService->deleteAll","Entering","..>>...................");
		moreProvsRepository.deleteAll();
		consoleLog.logMsg(showLogMsg,"->ProvsService->deleteAll","Exiting","...................>>..");
	}
	// ------------------------------------INITIALIZE PROVS DATE--------------------------------------------------------------	
	// -------------------Provs----->setProvsCurrentDate---------------------------------------------------------------	
	@Transactional
	public void setProvsCurrentDate() {
		consoleLog.logMsg(showLogMsg,"->ProvsService->setProvsCurrentDate","Entering","..>>...................");
		Date currentDate = new Date();
		List<Provs> provsList = provsRepository.findAll();
		for(int j=0; j<provsList.size(); j++) {
			if (provsList.get(j).getProvDate() == null) {
				provsList.get(j).setProvDate(currentDate);
				provsRepository.save(provsList.get(j));
			}
		}		
		consoleLog.logMsg(showLogMsg,"->ProvsService->setProvsCurrentDate","Exiting","...................>>..");
		return ;
	}	
	// -------------------Provs----->setProvsFactType---------------------------------------------------------------	
	@Transactional
	public void setProvsFactType() {
		consoleLog.logMsg(showLogMsg,"->ProvsService->setProvsFactType","Entering","..>>...................");
		List<Provs> provsList = provsRepository.findAll();
		for(int j=0; j<provsList.size(); j++) {
			if (provsList.get(j).getProvFactType() == null) { provsList.get(j).setProvFactType("NO Data"); }
			if (provsList.get(j).getProvPayType() == null) { provsList.get(j).setProvPayType("HojaDia"); }
			provsList.get(j).setProvFactNote(true);
			provsRepository.save(provsList.get(j));
		}		
		consoleLog.logMsg(showLogMsg,"->ProvsService->setProvsFactType","Exiting","...................>>..");
		return ;
	}	
}
