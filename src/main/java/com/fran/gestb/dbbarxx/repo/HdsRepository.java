package com.fran.gestb.dbbarxx.repo;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fran.gestb.dbbarxx.model.Hds;

public interface HdsRepository extends JpaRepository<Hds, Long> {

	List<Hds> findAll();
	Hds findByHdId(Long HdId);	
	Hds findByHdName(String HdName);
	List<Hds> findByHdDate(Date HdDate);
	List<Hds> findByHdDateAndHdType(Date HdDate, String HdType);
	List<Hds> findAllByHdTypeAndHdDateGreaterThanEqualAndHdDateLessThanEqual(String HdType,Date StartHdDate,Date EndHdDate);
	List<Hds> findAllByHdDateGreaterThanEqualAndHdDateLessThanEqual(Date StartHdDate,Date EndHdDate);
	List<Hds> findAllByHdNameAndHdDateGreaterThanEqualAndHdDateLessThanEqual(String HdName,Date StartHdDate,Date EndHdDate);

	long deleteAllByHdDate(Date HdDate);
	long deleteByHdId(long HdId);

}