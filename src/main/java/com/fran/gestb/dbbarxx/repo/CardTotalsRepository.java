package com.fran.gestb.dbbarxx.repo;

import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import com.fran.gestb.dbbarxx.model.CardTotals;

public interface CardTotalsRepository extends JpaRepository<CardTotals, Long> {
	List<CardTotals> findAll();
	CardTotals findByCardTotalId(Long cardTotalId);
	CardTotals findByCardTotalCardTpvName(String cardTotalCardTpvName);
	CardTotals findByCardTotalCardTpvId(Long cardTotalCardTpvId);
	List<CardTotals> findByCardTotalCardTpvIdAndCardTotalDateGreaterThanEqualAndCardTotalDateLessThanEqual(Long cardTotalCardTpvId,Date cardTotalDateStart,Date cardTotalDateEnd);
	List<CardTotals> findByCardTotalDateGreaterThanEqualAndCardTotalDateLessThanEqual(Date cardTotalDateStart,Date cardTotalDateEnd);
	long deleteByCardTotalCardTpvId(Long cardTotalCardTpvId);
	long deleteByCardTotalCardTpvIdAndCardTotalDateStartGreaterThanEqualAndCardTotalDateEndLessThanEqual(Long cardTotalCardTpvId,Date cardTotalDateStart,Date cardTotalDateEnd);
	long deleteByCardTotalDateStartGreaterThanEqualAndCardTotalDateEndLessThanEqual(Date cardTotalDateStart,Date cardTotalDateEnd);

}
