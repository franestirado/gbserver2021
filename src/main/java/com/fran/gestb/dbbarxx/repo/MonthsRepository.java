package com.fran.gestb.dbbarxx.repo;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fran.gestb.dbbarxx.model.Months;

public interface MonthsRepository extends JpaRepository<Months, Long> {
		List<Months> findAll();
		Months findByMonthId(Long MonthId);	
		Months findByMonthName(String MonthName);
		List<Months> findByMonthDate(Date MonthDate);
		List<Months> findByMonthDateAndMonthType(Date MonthDate, String MonthType);
		List<Months> findAllByMonthTypeAndMonthDateGreaterThanEqualAndMonthDateLessThanEqual(String MonthType,Date StartMonthDate,Date EndMonthDate);
		List<Months> findByMonthTypeInAndMonthDateGreaterThanEqualAndMonthDateLessThanEqual(List<String> MonthTypes,Date StartMonthDate,Date EndMonthDate);

		List<Months> findAllByMonthDateGreaterThanEqualAndMonthDateLessThanEqual(Date StartMonthDate,Date EndMonthDate);
		long deleteAllByMonthDateGreaterThanEqualAndMonthDateLessThanEqual(Date StartMonthDate,Date EndMonthDate);		
		long deleteAllByMonthDate(Date MonthDate);
		long deleteByMonthId(long MonthId);
}
