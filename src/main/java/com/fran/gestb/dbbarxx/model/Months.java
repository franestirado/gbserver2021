package com.fran.gestb.dbbarxx.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "months")
public class Months {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "monthId", updatable = false, nullable = false)
	private Long monthId;
	
	@NotNull	
	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date monthDate;	
	
	@NotNull
	@Size(max = 64)
	private String monthName;	
	
	@NotNull
	@Column(nullable=false)
	@Size(max = 64)
	private String monthType;	
	
	@NotNull
	@Column(nullable=false)
	@Size(max = 64)
	private String monthPlace;	
	
	@Column(nullable=false)
	private double monthAmount;
	
	@Column
	@Size(max = 64)
	private String monthNotes;
	
	@Column(nullable=false)
	private boolean monthStatus;
	
	// Checking attributes
	public boolean checkMonthAtributes() {
		if (this.monthName.length() == 0) return false;
		if ( (this.monthName.length() < 3) | (this.monthName.length() >64) )return false;
		if (this.monthType.length() == 0) return false;
		if ( (this.monthType.length() < 3) | (this.monthType.length() >64) )return false;
		if (this.monthPlace.length() == 0) return false;
		if ( (this.monthPlace.length() < 3) | (this.monthPlace.length() >64) )return false;
		return true;
	}
	// constructors
	public Months() {
	}	
	public Months(Long monthId, Date monthDate, String monthName, String monthType, String monthPlace,
			double monthAmount, String monthNotes, boolean monthStatus) {
		super();
		this.monthId 		= monthId;
		this.monthDate 		= monthDate;
		this.monthName 		= monthName;
		this.monthType 		= monthType;
		this.monthPlace 	= monthPlace;
		this.monthAmount 	= monthAmount;
		this.monthNotes 	= monthNotes;
		this.monthStatus 	= monthStatus;
	} 
	//setters and getters
	public Long getMonthId() {
		return monthId;
	}
	public void setMonthId(Long monthId) {
		this.monthId = monthId;
	}
	public Date getMonthDate() {
		return monthDate;
	}
	public void setMonthDate(Date monthDate) {
		this.monthDate = monthDate;
	}
	public String getMonthName() {
		return monthName;
	}
	public void setMonthName(String monthName) {
		this.monthName = monthName;
	}
	public String getMonthType() {
		return monthType;
	}
	public void setMonthType(String monthType) {
		this.monthType = monthType;
	}
	public String getMonthPlace() {
		return monthPlace;
	}
	public void setMonthPlace(String monthPlace) {
		this.monthPlace = monthPlace;
	}
	public double getMonthAmount() {
		return monthAmount;
	}
	public void setMonthAmount(double monthAmount) {
		this.monthAmount = monthAmount;
	}
	public String getMonthNotes() {
		return monthNotes;
	}
	public void setMonthNotes(String monthNotes) {
		this.monthNotes = monthNotes;
	}
	public boolean isMonthStatus() {
		return monthStatus;
	}
	public void setMonthStatus(boolean monthStatus) {
		this.monthStatus = monthStatus;
	}
}

