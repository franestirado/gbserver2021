package com.fran.gestb.dbbarxx.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.Date;

@Entity
@Table(name = "hds")
public class Hds {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "hdId", updatable = false, nullable = false)
	private Long hdId;
	
	@NotNull	
	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date hdDate;	
	
	@NotNull
	@Size(max = 64)
	private String hdName;	
	
	@NotNull
	@Column(nullable=false)
	@Size(max = 64)
	private String hdType;	
	
	@NotNull
	@Column(nullable=false)
	@Size(max = 64)
	private String hdPlace;	
	
	@Column(nullable=false)
	private int hdOrder;
	
	@Column(nullable=false)
	private double hdAmount;
	
	@Column
	@Size(max = 64)
	private String hdNotes;
	
	@Column(nullable=false)
	private boolean hdStatus;
	
	// Checking attributes
	public boolean checkHdAtributes() {
		if (this.hdName.length() == 0) return false;
		if ( (this.hdName.length() < 3) | (this.hdName.length() >25) )return false;
		if (this.hdType.length() == 0) return false;
		if ( (this.hdType.length() < 3) | (this.hdType.length() >64) )return false;
		if (this.hdPlace.length() == 0) return false;
		if ( (this.hdPlace.length() < 3) | (this.hdPlace.length() >64) )return false;
		return true;
	}
	// constructors
	public Hds() {
	}	
	public Hds(Long hdId, Date hdDate, String hdName, String hdType, String hdPlace, int hdOrder,
			double hdAmount, String hdNotes, boolean hdStatus) {
		super();
		this.hdId 		= hdId;
		this.hdDate 	= hdDate;
		this.hdName 	= hdName;
		this.hdType 	= hdType;
		this.hdPlace 	= hdPlace;
		this.hdOrder 	= hdOrder;
		this.hdAmount 	= hdAmount;
		this.hdNotes 	= hdNotes;
		this.hdStatus 	= hdStatus;
	} 
	//setters and getters
	public Long getHdId() {
		return hdId;
	}
	public void setHdId(Long hdId) {
		this.hdId = hdId;
	}
	public Date getHdDate() {
		return hdDate;
	}
	public void setHdDate(Date hdDate) {
		this.hdDate = hdDate;
	}
	public String getHdName() {
		return hdName;
	}
	public void setHdName(String hdName) {
		this.hdName = hdName;
	}
	public String getHdType() {
		return hdType;
	}
	public void setHdType(String hdType) {
		this.hdType = hdType;
	}
	public String getHdPlace() {
		return hdPlace;
	}
	public void setHdPlace(String hdPlace) {
		this.hdPlace = hdPlace;
	}
	public int getHdOrder() {
		return hdOrder;
	}
	public void setHdOrder(int hdOrder) {
		this.hdOrder = hdOrder;
	}
	public double getHdAmount() {
		return hdAmount;
	}
	public void setHdAmount(double hdAmount) {
		this.hdAmount = hdAmount;
	}
	public String getHdNotes() {
		return hdNotes;
	}
	public void setHdNotes(String hdNotes) {
		this.hdNotes = hdNotes;
	}
	public boolean isHdStatus() {
		return hdStatus;
	}
	public void setHdStatus(boolean hdStatus) {
		this.hdStatus = hdStatus;
	}
}
