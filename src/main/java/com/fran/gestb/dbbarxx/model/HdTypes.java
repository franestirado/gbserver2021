package com.fran.gestb.dbbarxx.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "HDTYPES")
public class HdTypes {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "hdTypeId", updatable = false, nullable = false)
	private Long hdTypeId;	
	@NotNull	
	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date hdTypeDate;
	@NotNull
	@Column(unique = true, nullable=false)
	@Size(max = 64)
	private String hdTypeName;	
	@NotNull
	@Column(nullable=false)
	@Size(max = 64)
	private String hdTypeType;	
	@NotNull
	@Column(nullable=false)
	@Size(max = 64)
	private String hdTypePlace;
	@Column(nullable=false)
	private int hdTypeOrder;
	@Column(nullable=false)
	private boolean hdTypeStatus;
	// Checking attributes
	public boolean checkHdTypeAtributes() {
		if (this.hdTypeDate == null) return false;
		if (this.hdTypeName.length() == 0) return false;
		if ( (this.hdTypeName.length() < 1) | (this.hdTypeName.length() >20) )return false;
		if (this.hdTypeType.length() == 0) return false;
		if ( (this.hdTypeType.length() < 1) | (this.hdTypeType.length() >64) )return false;
		if (this.hdTypePlace.length() == 0) return false;
		if ( (this.hdTypePlace.length() < 1) | (this.hdTypePlace.length() >64) )return false;
		return true;
	}
	// constructors
	public HdTypes() {
	}
	public HdTypes(String hdTypeName, Date hdTypeDate,String hdTypeType, String hdTypePlace, int hdTypeOrder, boolean hdTypeStatus) {
		this.hdTypeName = hdTypeName;
		this.hdTypeDate = hdTypeDate;
		this.hdTypeType = hdTypeType;
		this.hdTypePlace = hdTypePlace;
		this.hdTypeOrder = hdTypeOrder;
		this.hdTypeStatus = hdTypeStatus;
	}
	public HdTypes(Long hdTypeId,Date hdTypeDate,String hdTypeName, String hdTypeType, String hdTypePlace, int hdTypeOrder, boolean hdTypeStatus) {
		this.hdTypeId = hdTypeId;
		this.hdTypeDate = hdTypeDate;
		this.hdTypeName = hdTypeName;
		this.hdTypeType = hdTypeType;
		this.hdTypePlace = hdTypePlace;
		this.hdTypeOrder = hdTypeOrder;
		this.hdTypeStatus = hdTypeStatus;
	}
	//setters and getters
	public Long getHdTypeId() {
		return hdTypeId;
	}
	public void setHdTypeId(Long hdTypeId) {
		this.hdTypeId = hdTypeId;
	}
	public Date getHdTypeDate() {
		return hdTypeDate;
	}
	public void setHdTypeDate(Date hdTypeDate) {
		this.hdTypeDate = hdTypeDate;
	}
	public String getHdTypeName() {
		return hdTypeName;
	}
	public void setHdTypeName(String hdTypeName) {
		this.hdTypeName = hdTypeName;
	}
	public String getHdTypeType() {
		return hdTypeType;
	}
	public void setHdTypeType(String hdTypeType) {
		this.hdTypeType = hdTypeType;
	}
	public String getHdTypePlace() {
		return hdTypePlace;
	}
	public void setHdTypePlace(String hdTypePlace) {
		this.hdTypePlace = hdTypePlace;
	}
	public int gethdTypeOrder() {
		return hdTypeOrder;
	}
	public void sethdTypeOrder(int hdTypeOrder) {
		this.hdTypeOrder = hdTypeOrder;
	}
	public boolean isHdTypeStatus() {
		return hdTypeStatus;
	}
	public void setHdTypeStatus(boolean hdTypeStatus) {
		this.hdTypeStatus = hdTypeStatus;
	}

}
