package com.fran.gestb.dbbarxx.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "cardTpvs")
public class CardTpvs {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "cardTpvId", updatable = false, nullable = false)
	private Long cardTpvId;	
	@NotNull	
	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date cardTpvDate;
	@NotNull
	@Size(max = 32)
	@Column(unique = true, nullable=false)
	private String cardTpvName;	
	@NotNull
	@Size(max = 32)
	@Column(nullable=false)
	private String cardTpvShopNr;	
	@NotNull
	@Size(max = 32)
	@Column(nullable=false)
	private String cardTpvTpvNr;	
	@NotNull
	@Column(nullable=false)
	@Size(max = 32)
	private String cardTpvSerialNr;	
	@NotNull
	@Column(nullable=true)
	@Size(max = 32)
	private String cardTpvBank;
	@NotNull
	@Column(nullable=true)
	@Size(max = 32)
	private String cardTpvType;
	@NotNull
	@Column(nullable=true)
	@Size(max = 32)
	private String cardTpvContact;
	@Column(nullable=false)
	private boolean cardTpvMulti;	
	@Column(nullable=false)
	private double cardTpvComis1;
	@Column(nullable=false)
	private double cardTpvComis2;
	@Column(nullable=false)
	private double cardTpvComis3;
	@Column(nullable=false)
	private double cardTpvComis4;	
	@Column(nullable=false)
	private double cardTpvComisTax;
	@Column(nullable=false)
	private long cardTpvProvId;	
	@Column(nullable=false)
	private String cardTpvProvName;
	@Column(nullable=false)
	private long cardTpvArtId;	
	@Column(nullable=false)
	private String cardTpvArtName;	
	@Column(nullable=false)
	private boolean cardTpvStatus;	
	// Checking attributes
	public boolean checkCardTpvAtributes() {	
		if (this.cardTpvDate == null) return false;
		if ( (this.cardTpvName.length() < 1) 		| (this.cardTpvName.length() 	> 32) )return false;		
		if ( (this.cardTpvShopNr.length() < 1) 	| (this.cardTpvShopNr.length() > 32) )return false;
		if ( (this.cardTpvTpvNr.length() < 1) 		| (this.cardTpvTpvNr.length() 	> 32) )return false;
		if ( (this.cardTpvSerialNr.length() < 1) 	| (this.cardTpvSerialNr.length() > 32) )return false;
		if ( (this.cardTpvBank.length() < 1) 		| (this.cardTpvBank.length() 	> 32) )return false;
		if ( (this.cardTpvType.length() < 1) 		| (this.cardTpvType.length() 	> 32) )return false;	
		if ( (this.cardTpvContact.length() < 1) 	| (this.cardTpvContact.length() >32) )return false;
		return true;
	}
	// constructors
	public CardTpvs() { }
	public CardTpvs(	Long cardTpvId, Date cardTpvDate, String cardTpvName, String cardTpvShopNr, String cardTpvTpvNr,
						String cardTpvSerialNr, String cardTpvBank, String cardTpvType, String cardTpvContact,
						boolean cardTpvMulti, double cardTpvComis1,double cardTpvComis2,double cardTpvComis3,double cardTpvComis4, 
						double cardTpvComisTax, long cardTpvProvId, String cardTpvProvName, long cardTpvArtId, String cardTpvArtName,
						boolean cardTpvStatus) {
		super();
		this.cardTpvId = cardTpvId;
		this.cardTpvDate = cardTpvDate;
		this.cardTpvName = cardTpvName;
		this.cardTpvShopNr = cardTpvShopNr;
		this.cardTpvTpvNr = cardTpvTpvNr;
		this.cardTpvSerialNr = cardTpvSerialNr;
		this.cardTpvBank = cardTpvBank;
		this.cardTpvType = cardTpvType;
		this.cardTpvContact = cardTpvContact;
		this.cardTpvMulti = cardTpvMulti;
		this.cardTpvComis1 = cardTpvComis1;
		this.cardTpvComis2 = cardTpvComis2;
		this.cardTpvComis3 = cardTpvComis3;
		this.cardTpvComis4 = cardTpvComis4;
		this.cardTpvComisTax = cardTpvComisTax;
		this.cardTpvStatus = cardTpvStatus;
		this.cardTpvProvId = cardTpvProvId;
		this.cardTpvProvName = cardTpvProvName;
		this.cardTpvArtId = cardTpvArtId;
		this.cardTpvArtName = cardTpvArtName;
	}	
	//setters and getters
	public Long getCardTpvId() {
		return cardTpvId;
	}
	public void setCardTpvId(Long cardTpvId) {
		this.cardTpvId = cardTpvId;
	}
	public Date getCardTpvDate() {
		return cardTpvDate;
	}
	public void setCardTpvDate(Date cardTpvDate) {
		this.cardTpvDate = cardTpvDate;
	}
	public String getCardTpvName() {
		return cardTpvName;
	}
	public void setCardTpvName(String cardTpvName) {
		this.cardTpvName = cardTpvName;
	}
	public String getCardTpvShopNr() {
		return cardTpvShopNr;
	}
	public void setCardTpvShopNr(String cardTpvShopNr) {
		this.cardTpvShopNr = cardTpvShopNr;
	}
	public String getCardTpvTpvNr() {
		return cardTpvTpvNr;
	}
	public void setCardTpvTpvNr(String cardTpvTpvNr) {
		this.cardTpvTpvNr = cardTpvTpvNr;
	}
	public String getCardTpvSerialNr() {
		return cardTpvSerialNr;
	}
	public void setCardTpvSerialNr(String cardTpvSerialNr) {
		this.cardTpvSerialNr = cardTpvSerialNr;
	}
	public String getCardTpvBank() {
		return cardTpvBank;
	}
	public void setCardTpvBank(String cardTpvBank) {
		this.cardTpvBank = cardTpvBank;
	}
	public String getCardTpvType() {
		return cardTpvType;
	}
	public void setCardTpvType(String cardTpvType) {
		this.cardTpvType = cardTpvType;
	}
	public String getCardTpvContact() {
		return cardTpvContact;
	}
	public void setCardTpvContact(String cardTpvContact) {
		this.cardTpvContact = cardTpvContact;
	}
	public boolean iscardTpvMulti() {
		return cardTpvMulti;
	}
	public void setcardTpvMulti(boolean cardTpvMulti) {
		this.cardTpvMulti = cardTpvMulti;
	}
	public double getCardTpvComis1() {
		return cardTpvComis1;
	}
	public void setCardTpvComis1(double cardTpvComis1) {
		this.cardTpvComis1 = cardTpvComis1;
	}
	public double getCardTpvComis2() {
		return cardTpvComis2;
	}
	public void setCardTpvComis2(double cardTpvComis2) {
		this.cardTpvComis2 = cardTpvComis2;
	}
	public double getCardTpvComis3() {
		return cardTpvComis3;
	}
	public void setCardTpvComis3(double cardTpvComis3) {
		this.cardTpvComis3 = cardTpvComis3;
	}
	public double getCardTpvComis4() {
		return cardTpvComis4;
	}
	public void setCardTpvComis4(double cardTpvComis4) {
		this.cardTpvComis4 = cardTpvComis4;
	}	
	public double getCardTpvComisTax() {
		return cardTpvComisTax;
	}
	public void setCardTpvComisTax(double cardTpvComisTax) {
		this.cardTpvComisTax = cardTpvComisTax;
	}
	public long getCardTpvProvId() {
		return cardTpvProvId;
	}
	public void setCardTpvProvId(long cardTpvProvId) {
		this.cardTpvProvId = cardTpvProvId;
	}
	public String getCardTpvProvName() {
		return cardTpvProvName;
	}
	public void setCardTpvProvName(String cardTpvProvName) {
		this.cardTpvProvName = cardTpvProvName;
	}
	public String getCardTpvArtName() {
		return cardTpvArtName;
	}
	public void setCardTpvArtName(String cardTpvArtName) {
		this.cardTpvArtName = cardTpvArtName;
	}
	public long getCardTpvArtId() {
		return cardTpvArtId;
	}
	public void setCardTpvArtId(long cardTpvArtId) {
		this.cardTpvArtId = cardTpvArtId;
	}	
	public boolean isCardTpvStatus() {
		return cardTpvStatus;
	}
	public void setCardTpvStatus(boolean cardTpvStatus) {
		this.cardTpvStatus = cardTpvStatus;
	}	
}
