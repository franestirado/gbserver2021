package com.fran.gestb.dbbarxx.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "facts")
public class Facts {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "factId", updatable = false, nullable = false)
	private Long factId;
	@NotNull
	@Column(nullable=false)
	private Long factProvId;	
	@NotNull
	@Size(max = 64)
	private String factProvName;	
	@NotNull
	@Size(max = 32)
	private String factNumber;	
	@NotNull	
	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date factDate;	
	@NotNull
	@Size(max = 16)
	private String factType;		
	@Column(nullable=false)
	private double factNetCost;	
	@Column(nullable=false)
	private double factTaxes;	
	@Column(nullable=false)
	private double factTotalCost;	
	@NotNull
	@Size(max = 16)
	private String factPayType;	
	@Column(nullable=false)	
	private boolean factDetailed;
	@Column	(nullable=false)	
	private boolean factNote;	
	@Column(nullable=false)	
	private boolean factStatus;	
	// Checking attributes
	public boolean checkFactAtributes() {
		if (this.factDate == null) return false;
		if (this.factProvName.length() == 0) return false;
		if ( (this.factProvName.length() < 3) | (this.factProvName.length() >64) )return false;
		if (this.factNumber.length() == 0) return false;
		if ( (this.factNumber.length() < 1) | (this.factNumber.length() >32) )return false;
		if (this.factType.length() == 0) return false;
		if ( (this.factType.length() < 1) | (this.factType.length() >16) )return false;
		if (this.factPayType.length() == 0) return false;
		if ( (this.factPayType.length() < 1) | (this.factPayType.length() >16) )return false;
		return true;
	}
	// constructors
	public Facts() {					
	}
	public Facts(Long factId, Long factProvId, String factProvName, String factNumber, Date factDate,  
			String factType, double factNetCost, double factTaxes, double factTotalCost, String factPayType,
			boolean factDetailed, boolean factNote, boolean factStatus) {
		super();
		this.factId = factId;
		this.factProvId = factProvId;
		this.factProvName = factProvName;
		this.factNumber = factNumber;
		this.factDate = factDate;
		this.factType = factType;
		this.factNetCost = factNetCost;
		this.factTaxes = factTaxes;
		this.factTotalCost = factTotalCost;
		this.factPayType = factPayType;
		this.factDetailed = factDetailed;
		this.factNote = factNote;
		this.factStatus = factStatus;
	}		
	//setters and getters
	public Long getFactId() {
		return factId;
	}
	public void setFactId(Long factId) {
		this.factId = factId;
	}
	public Long getFactProvId() {
		return factProvId;
	}
	public void setFactProvId(Long factProvId) {
		this.factProvId = factProvId;
	}
	public String getFactProvName() {
		return factProvName;
	}
	public void setFactProvName(String factProvName) {
		this.factProvName = factProvName;
	}
	public String getFactNumber() {
		return factNumber;
	}
	public void setFactNumber(String factNumber) {
		this.factNumber = factNumber;
	}
	public Date getFactDate() {
		return factDate;
	}
	public void setFactDate(Date factDate) {
		this.factDate = factDate;
	}
	public String getFactType() {
		return factType;
	}
	public void setFactType(String factType) {
		this.factType = factType;
	}
	public double getFactNetCost() {
		return factNetCost;
	}
	public void setFactNetCost(double factNetCost) {
		this.factNetCost = factNetCost;
	}
	public double getFactTaxes() {
		return factTaxes;
	}
	public void setFactTaxes(double factTaxes) {
		this.factTaxes = factTaxes;
	}
	public double getFactTotalCost() {
		return factTotalCost;
	}
	public void setFactTotalCost(double factTotalCost) {
		this.factTotalCost = factTotalCost;
	}
	public String getFactPayType() {
		return factPayType;
	}
	public void setFactPayType(String factPayType) {
		this.factPayType = factPayType;
	}
	public boolean isFactDetailed() {
		return factDetailed;
	}
	public void setFactDetailed(boolean factDetailed) {
		this.factDetailed = factDetailed;
	}
	public boolean isFactNote() {
		return factNote;
	}
	public void setFactNote(boolean factNote) {
		this.factNote = factNote;
	}
	public boolean isFactStatus() {
		return factStatus;
	}
	public void setFactStatus(boolean factStatus) {
		this.factStatus = factStatus;
	}
	
}
