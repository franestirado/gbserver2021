package com.fran.gestb.dbbarxx.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "cardPayouts")
public class CardPayouts {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "cardPayoutId", updatable = false, nullable = false)
	private Long cardPayoutId;	
	@NotNull	
	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date cardPayoutDate;		
	@NotNull
	@Column(nullable=false)
	private Long cardPayoutOperationNr;
	@NotNull
	@Column(nullable=false)
	private Long cardPayoutMulti;	
	@Column(nullable=false)
	private double cardPayoutTotal;	
	@NotNull
	@Column(nullable=false)
	private Long cardPayoutTpvId;	
	@Column(nullable=true)
	private Long cardPayoutTotalId;
	@Temporal(TemporalType.DATE)
	@Column(nullable=true)
	private Date cardPayoutTotalDate;
	// Checking attributes
	public boolean checkCardPayoutAtributes() {	
		if   (this.cardPayoutDate == null) return false;
		if   (this.cardPayoutOperationNr == null)return false;
		if   (this.cardPayoutMulti == 0) return false;
		if   (this.cardPayoutTotal == 0) return false;
		if   (this.cardPayoutTpvId == null) return false;
		return true;
	}
	// constructors
	public CardPayouts() {}
	public CardPayouts(Long cardPayoutId, Date cardPayoutDate, Long cardPayoutOperationNr,Long cardPayoutMulti,
			double cardPayoutTotal, Long cardPayoutTpvId, Long cardPayoutTotalId, Date cardPayoutTotalDate) {
		super();
		this.cardPayoutId = cardPayoutId;
		this.cardPayoutDate = cardPayoutDate;
		this.cardPayoutOperationNr = cardPayoutOperationNr;
		this.cardPayoutMulti = cardPayoutMulti;
		this.cardPayoutTotal = cardPayoutTotal;
		this.cardPayoutTpvId = cardPayoutTpvId;
		this.cardPayoutTotalId = cardPayoutTotalId;
		this.cardPayoutTotalDate = cardPayoutTotalDate;
	}
	//setters and getters
	public Long getCardPayoutId() {
		return cardPayoutId;
	}
	public void setCardPayoutId(Long cardPayoutId) {
		this.cardPayoutId = cardPayoutId;
	}
	public Date getCardPayoutDate() {
		return cardPayoutDate;
	}
	public void setCardPayoutDate(Date cardPayoutDate) {
		this.cardPayoutDate = cardPayoutDate;
	}
	public Long getcardPayoutMulti() {
		return cardPayoutMulti;
	}
	public void setcardPayoutMulti(Long cardPayoutMulti) {
		this.cardPayoutMulti = cardPayoutMulti;
	}
	public Long getCardPayoutOperationNr() {
		return cardPayoutOperationNr;
	}
	public void setCardPayoutOperationNr(Long cardPayoutOperationNr) {
		this.cardPayoutOperationNr = cardPayoutOperationNr;
	}
	public double getCardPayoutTotal() {
		return cardPayoutTotal;
	}
	public void setCardPayoutTotal(double cardPayoutTotal) {
		this.cardPayoutTotal = cardPayoutTotal;
	}
	public Long getCardPayoutTpvId() {
		return cardPayoutTpvId;
	}
	public void setCardPayoutTpvId(Long cardPayoutTpvId) {
		this.cardPayoutTpvId = cardPayoutTpvId;
	}
	public Long getCardPayoutTotalId() {
		return cardPayoutTotalId;
	}
	public void setCardPayoutTotalId(Long cardPayoutTotalId) {
		this.cardPayoutTotalId = cardPayoutTotalId;
	}
	public Date getCardPayoutTotalDate() {
		return cardPayoutTotalDate;
	}
	public void setCardPayoutTotalDate(Date cardPayoutTotalDate) {
		this.cardPayoutTotalDate = cardPayoutTotalDate;
	}
	
}
