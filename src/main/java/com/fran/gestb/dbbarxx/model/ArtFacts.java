package com.fran.gestb.dbbarxx.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "artfacts")
public class ArtFacts {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "modelFactId", updatable = false, nullable = false)
	private Long artFactId;
	
	@NotNull
	@Column(nullable=false)
	private Long artFactFactId;
	
	@NotNull	
	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date artFactFactDate;
	
	@NotNull
	@Column(nullable=false)
	private Long artFactArtId;
	
	@NotNull
	@Column(nullable=false)
	@Size(max = 32)
	@NotNull
	private String artFactArtName;
	
	@NotNull
	@Column(nullable=true)
	@Size(max = 16)
	@NotNull
	private String artFactArtType;
	
	@NotNull
	@Column(nullable=false)
	private double artFactArtSize;
	
	@NotNull
	@Column(nullable=true)
	@Size(max = 16)
	@NotNull
	private String artFactArtMeasurement;
	
	@NotNull
	@Column(nullable=false)
	private double artFactArtUnitPrice;
	
	@NotNull
	@Column(nullable=false)
	private double artFactArtPackageUnits;
	
	@NotNull
	@Column(nullable=false)
	private double artFactArtPackagePrice;
	
	@NotNull
	@Column(nullable=false)
	private double artFactArtTax;
	
	@NotNull
	@Column(nullable=false)
	private double artFactQuantity;
	
	@NotNull
	@Column(nullable=false)
	private double artFactDiscount;
	
	@NotNull
	@Column(nullable=false)
	private double artFactNetCost;   
	
	@NotNull
	@Column(nullable=false)
	private double artFactTax;
	
	@NotNull
	@Column(nullable=false)
	private double artFactTotalCost;
	
	// Checking attributes
	public boolean checkArtFactAtributes() {
		if (this.artFactFactDate == null) return false;
		if (this.artFactArtName.length() == 0) return false;
		if ( (this.artFactArtName.length() < 1) | (this.artFactArtName.length() >32) )return false;
		if (this.artFactArtType.length() == 0) return false;
		return true;
	}
	// constructors
	public ArtFacts() {}
	public ArtFacts(Long artFactId, Long artFactFactId, Date artFactFactDate,
			Long artFactArtId,  String artFactArtName,
			 String artFactArtType, double artFactArtSize,
			 String artFactArtMeasurement, double artFactArtUnitPrice,
			 double artFactArtPackageUnits, double artFactArtPackagePrice,
			 double artFactArtTax, double artFactQuantity, double artFactDiscount,
			 double artFactNetCost, double artFactTax, double artFactTotalCost) {
		super();
		this.artFactId = artFactId;
		this.artFactFactId = artFactFactId;
		this.artFactFactDate = artFactFactDate;
		this.artFactArtId = artFactArtId;
		this.artFactArtName = artFactArtName;
		this.artFactArtType = artFactArtType;
		this.artFactArtSize = artFactArtSize;
		this.artFactArtMeasurement = artFactArtMeasurement;
		this.artFactArtUnitPrice = artFactArtUnitPrice;
		this.artFactArtPackageUnits = artFactArtPackageUnits;
		this.artFactArtPackagePrice = artFactArtPackagePrice;
		this.artFactArtTax = artFactArtTax;
		this.artFactQuantity = artFactQuantity;
		this.artFactDiscount = artFactDiscount;
		this.artFactNetCost = artFactNetCost;
		this.artFactTax = artFactTax;
		this.artFactTotalCost = artFactTotalCost;
	}
	//setters and getters
	public Long getArtFactId() {
		return artFactId;
	}
	public void setArtFactId(Long artFactId) {
		this.artFactId = artFactId;
	}
	public Long getArtFactFactId() {
		return artFactFactId;
	}
	public void setArtFactFactId(Long artFactFactId) {
		this.artFactFactId = artFactFactId;
	}
	public Date getArtFactFactDate() {
		return artFactFactDate;
	}
	public void setArtFactFactDate(Date artFactFactDate) {
		this.artFactFactDate = artFactFactDate;
	}
	public Long getArtFactArtId() {
		return artFactArtId;
	}
	public void setArtFactArtId(Long artFactArtId) {
		this.artFactArtId = artFactArtId;
	}
	public String getArtFactArtName() {
		return artFactArtName;
	}
	public void setArtFactArtName(String artFactArtName) {
		this.artFactArtName = artFactArtName;
	}
	public String getArtFactArtType() {
		return artFactArtType;
	}
	public void setArtFactArtType(String artFactArtType) {
		this.artFactArtType = artFactArtType;
	}
	public double getArtFactArtSize() {
		return artFactArtSize;
	}
	public void setArtFactArtSize(double artFactArtSize) {
		this.artFactArtSize = artFactArtSize;
	}
	public String getArtFactArtMeasurement() {
		return artFactArtMeasurement;
	}
	public void setArtFactArtMeasurement(String artFactArtMeasurement) {
		this.artFactArtMeasurement = artFactArtMeasurement;
	}
	public double getArtFactArtUnitPrice() {
		return artFactArtUnitPrice;
	}
	public void setArtFactArtUnitPrice(double artFactArtUnitPrice) {
		this.artFactArtUnitPrice = artFactArtUnitPrice;
	}
	public double getArtFactArtPackageUnits() {
		return artFactArtPackageUnits;
	}
	public void setArtFactArtPackageUnits(double artFactArtPackageUnits) {
		this.artFactArtPackageUnits = artFactArtPackageUnits;
	}
	public double getArtFactArtPackagePrice() {
		return artFactArtPackagePrice;
	}
	public void setArtFactArtPackagePrice(double artFactArtPackagePrice) {
		this.artFactArtPackagePrice = artFactArtPackagePrice;
	}
	public double getArtFactArtTax() {
		return artFactArtTax;
	}
	public void setArtFactArtTax(double artFactArtTax) {
		this.artFactArtTax = artFactArtTax;
	}
	public double getArtFactQuantity() {
		return artFactQuantity;
	}
	public void setArtFactQuantity(double artFactQuantity) {
		this.artFactQuantity = artFactQuantity;
	}
	public double getArtFactDiscount() {
		return artFactDiscount;
	}
	public void setArtFactDiscount(double artFactDiscount) {
		this.artFactDiscount = artFactDiscount;
	}
	public double getArtFactNetCost() {
		return artFactNetCost;
	}
	public void setArtFactNetCost(double artFactNetCost) {
		this.artFactNetCost = artFactNetCost;
	}
	public double getArtFactTax() {
		return artFactTax;
	}
	public void setArtFactTax(double artFactTax) {
		this.artFactTax = artFactTax;
	}
	public double getArtFactTotalCost() {
		return artFactTotalCost;
	}
	public void setArtFactTotalCost(double artFactTotalCost) {
		this.artFactTotalCost = artFactTotalCost;
	}		
}
