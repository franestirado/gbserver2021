package com.fran.gestb.dbbarxx.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "maqs")
public class Maqs {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "maqId", updatable = false, nullable = false)
	private Long maqId;
	@NotNull
	@Size(max = 64)
	@Column(nullable=false)
	private String maqMaqBName;	
	@NotNull	
	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date maqStartDate;
	@NotNull	
	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date maqEndDate;
	@NotNull
	@Size(max = 32)
	@Column(nullable=false)
	private String maqSumType;							//(MAQB, MAQA, TABACO, etc.)
	@NotNull
	@Size(max = 32)
	@Column(nullable=false)
	private String maqSumDataType;						//(MECH CNT, ELECTR CNT, PART CNT, COIN START, COIN END, SUMMARY)
	@Column(nullable=false)
	private double maqCoin1StartInputDinAdded;			//(Coin1 - StartInput  - DinAdded   )
	@Column(nullable=false)
	private double maqCoin2StartOutputDinDifDays;		//(Coin2 - StartOutput - DinDifDays )
	@Column(nullable=false)
	private double maqCoin3StartDrawerDinDifCoins;		//(Coin3 - StartDrawer - DinDifCoins)
	@Column(nullable=false)
	private double maqCoin4EndInputDinEnd;				//(Coin4 - EndInput    - DinEnd     )
	@Column(nullable=false)
	private double maqCoin5EndOutputDinMechCntr;		//(Coin5 - EndOutput   - DinMechCntr)
	@Column(nullable=false)
	private double maqCoin6EndDrawerDinPartCntr;		//(Coin6 - EndDrawer   - DinPartCntr)
	@Column(nullable=false)	
	private boolean maqStatus;
	// Checking attributes
	public boolean checkMaqAtributes() {
		if ( (this.maqMaqBName 		== null) || (this.maqMaqBName.length() 	== 0) ) return false;
		if ( (this.maqStartDate 	== null) ) return false;
		if ( (this.maqEndDate	 	== null) ) return false;
		if ( (this.maqSumType 		== null) || (this.maqSumType.length() 	== 0) ) return false;
		if ( (this.maqSumDataType 	== null) || (this.maqSumDataType.length() == 0) ) return false;
		return true;
	}
	// constructors
	public Maqs() {}
	public Maqs(Long maqId, @NotNull @Size(max = 64) String maqMaqBName, @NotNull Date maqStartDate,
			@NotNull Date maqEndDate, @NotNull @Size(max = 32) String maqSumType,
			@NotNull @Size(max = 32) String maqSumDataType, double maqCoin1StartInputDinAdded,
			double maqCoin2StartOutputDinDifDays, double maqCoin3StartDrawerDinDifCoins, double maqCoin4EndInputDinEnd,
			double maqCoin5EndOutputDinMechCntr, double maqCoin6EndDrawerDinPartCntr, boolean maqStatus) {
		super();
		this.maqId = maqId;
		this.maqMaqBName = maqMaqBName;
		this.maqStartDate = maqStartDate;
		this.maqEndDate = maqEndDate;
		this.maqSumType = maqSumType;
		this.maqSumDataType = maqSumDataType;
		this.maqCoin1StartInputDinAdded = maqCoin1StartInputDinAdded;
		this.maqCoin2StartOutputDinDifDays = maqCoin2StartOutputDinDifDays;
		this.maqCoin3StartDrawerDinDifCoins = maqCoin3StartDrawerDinDifCoins;
		this.maqCoin4EndInputDinEnd = maqCoin4EndInputDinEnd;
		this.maqCoin5EndOutputDinMechCntr = maqCoin5EndOutputDinMechCntr;
		this.maqCoin6EndDrawerDinPartCntr = maqCoin6EndDrawerDinPartCntr;
		this.maqStatus = maqStatus;
	}
	//setters and getters
	public Long getMaqId() {
		return maqId;
	}
	public void setMaqId(Long maqId) {
		this.maqId = maqId;
	}
	public String getMaqMaqBName() {
		return maqMaqBName;
	}
	public void setMaqMaqBName(String maqMaqBName) {
		this.maqMaqBName = maqMaqBName;
	}
	public Date getMaqStartDate() {
		return maqStartDate;
	}
	public void setMaqStartDate(Date maqStartDate) {
		this.maqStartDate = maqStartDate;
	}
	public Date getMaqEndDate() {
		return maqEndDate;
	}
	public void setMaqEndDate(Date maqEndDate) {
		this.maqEndDate = maqEndDate;
	}
	public String getMaqSumType() {
		return maqSumType;
	}
	public void setMaqSumType(String maqSumType) {
		this.maqSumType = maqSumType;
	}
	public String getMaqSumDataType() {
		return maqSumDataType;
	}
	public void setMaqSumDataType(String maqSumDataType) {
		this.maqSumDataType = maqSumDataType;
	}
	public double getMaqCoin1StartInputDinAdded() {
		return maqCoin1StartInputDinAdded;
	}
	public void setMaqCoin1StartInputDinAdded(double maqCoin1StartInputDinAdded) {
		this.maqCoin1StartInputDinAdded = maqCoin1StartInputDinAdded;
	}
	public double getMaqCoin2StartOutputDinDifDays() {
		return maqCoin2StartOutputDinDifDays;
	}
	public void setMaqCoin2StartOutputDinDifDays(double maqCoin2StartOutputDinDifDays) {
		this.maqCoin2StartOutputDinDifDays = maqCoin2StartOutputDinDifDays;
	}
	public double getMaqCoin3StartDrawerDinDifCoins() {
		return maqCoin3StartDrawerDinDifCoins;
	}
	public void setMaqCoin3StartDrawerDinDifCoins(double maqCoin3StartDrawerDinDifCoins) {
		this.maqCoin3StartDrawerDinDifCoins = maqCoin3StartDrawerDinDifCoins;
	}
	public double getMaqCoin4EndInputDinEnd() {
		return maqCoin4EndInputDinEnd;
	}
	public void setMaqCoin4EndInputDinEnd(double maqCoin4EndInputDinEnd) {
		this.maqCoin4EndInputDinEnd = maqCoin4EndInputDinEnd;
	}
	public double getMaqCoin5EndOutputDinMechCntr() {
		return maqCoin5EndOutputDinMechCntr;
	}
	public void setMaqCoin5EndOutputDinMechCntr(double maqCoin5EndOutputDinMechCntr) {
		this.maqCoin5EndOutputDinMechCntr = maqCoin5EndOutputDinMechCntr;
	}
	public double getMaqCoin6EndDrawerDinPartCntr() {
		return maqCoin6EndDrawerDinPartCntr;
	}
	public void setMaqCoin6EndDrawerDinPartCntr(double maqCoin6EndDrawerDinPartCntr) {
		this.maqCoin6EndDrawerDinPartCntr = maqCoin6EndDrawerDinPartCntr;
	}
	public boolean isMaqStatus() {
		return maqStatus;
	}
	public void setMaqStatus(boolean maqStatus) {
		this.maqStatus = maqStatus;
	}	
}
