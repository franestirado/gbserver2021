package com.fran.gestb.dbbarxx.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fran.gestb.aacommon.ConsoleLog;
import com.fran.gestb.aacommon.ValueRecord;
import com.fran.gestb.aaexceptions.EmptyFieldException;
import com.fran.gestb.aaexceptions.EntityAlreadyExistsException;
import com.fran.gestb.aaexceptions.EntityNotFoundException;
import com.fran.gestb.aaexceptions.WrongParametersException;
import com.fran.gestb.dbbarxx.model.CardPayouts;
import com.fran.gestb.dbbarxx.model.CardTotals;
import com.fran.gestb.dbbarxx.model.CardTpvs;
import com.fran.gestb.dbbarxx.repo.CardPayoutsRepository;
import com.fran.gestb.dbbarxx.repo.CardTotalsRepository;
import com.fran.gestb.dbbarxx.repo.CardTpvsRepository;

@Service
public class CardsService {
	@Autowired
	private CardPayoutsRepository cardsPayoutsRepository;
	@Autowired
	private CardTpvsRepository cardTpvsRepository;
	@Autowired
	private CardTotalsRepository cardTotalsRepository;
	@Autowired
	private ConsoleLog consoleLog; 
	private boolean showLogMsg = true;

	// -------------------Cards Payouts------------------------------------------------------------------------------------
	// -------------------Cards Payouts----->getAllCardPayouts----------------------------------------------------------	
	@Transactional
	public List<CardPayouts> getAllCardPayouts( List<ValueRecord> cardPayoutVRList) throws EmptyFieldException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->CardsService->getAllCardPayouts","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ( (cardPayoutVRList == null) | (cardPayoutVRList.size() == 0) ) 
			throw new EmptyFieldException("->CardsService->getAllCardPayouts.1.->Null Object or Empty Mandatoy Parameters");
		for (int j=0; j<cardPayoutVRList.size() ; j++) {
			if ( (cardPayoutVRList.get(j) == null) | (cardPayoutVRList.get(j).getValue().length() == 0) )
				throw new EmptyFieldException("->CardsService->getAllCardPayouts.2.->Null Object or Empty Mandatoy Parameters");
			// consoleLog.logMsg(showLogMsg,"->cardsController->getAllCardPayouts","....",cardPayoutVRList.get(j).getValue());
		}		
		// check if received card TPV exists 
		CardTpvs readCardTpv = null;
		if ( cardPayoutVRList.get(0).getValue().equals("ALL") == false ) {
			readCardTpv = cardTpvsRepository.findByCardTpvName(cardPayoutVRList.get(0).getValue());
			if (readCardTpv == null)
				throw new EntityNotFoundException("->CardsService->getAllCardPayouts.1.->Not Found"+cardPayoutVRList.get(0).getValue());
		}
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		//formatter.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));	 
		//String dateInString = "22-01-2015 10:15:55 AM"; 
		Date startDate,endDate;
		try {
			endDate = formatter.parse(cardPayoutVRList.get(2).getValue());
			// consoleLog.logMsg(showLogMsg,"->CardsService->getAllCardPayouts","..endDate........",endDate.toString());
			startDate = formatter.parse(cardPayoutVRList.get(1).getValue());
			// consoleLog.logMsg(showLogMsg,"->CardsService->getAllCardPayouts","..startDate..",startDate.toString());

		} catch (ParseException e) {
			throw new EmptyFieldException("->CardsService->getAllCardPayouts.1.->Wrong DATE Object ");
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		//String formattedDateString = formatter.format(date);
		List<CardPayouts> readCardsList = null;
		if ( cardPayoutVRList.get(0).getValue().equals("ALL") == true )  {
			readCardsList = cardsPayoutsRepository.findByCardPayoutDateGreaterThanEqualAndCardPayoutDateLessThanEqual(startDate,endDate);				
		} else {
			readCardsList = cardsPayoutsRepository.findByCardPayoutTpvIdAndCardPayoutDateGreaterThanEqualAndCardPayoutDateLessThanEqual(readCardTpv.getCardTpvId(),startDate,endDate);				
		}
		//consoleLog.logMsg(showLogMsg,"->CardsService->getAllCardPayouts","...tpv...",cardPayoutVRList.get(0).getValue());
		consoleLog.logMsg(showLogMsg,"->CardsService->getAllCardPayouts","Exiting","..>>...................");

		return readCardsList;
	}		
	// -------------------Cards Payouts----->createCardPayoutsList----------------------------------------------------------	
	@Transactional
	public List<CardPayouts> createCardPayoutsList(List<CardPayouts> newCardPayoutsList) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->CardsService->createCardPayoutsList","Entering","..>>...................");		
		// Checking null new object or empty mandatory attributes		
		if ((newCardPayoutsList==null)) 
			throw new EmptyFieldException("->CardsService->createCardPayoutsList-> Null Object or Empty Mandatoy Parameters");	
		if ((newCardPayoutsList.size() < 1)) 
			throw new EmptyFieldException("->CardsService->createCardPayoutsList-> Empty Mandatoy Parameters");	
		// Check if cardTpvId exist, first element of arts list 
		CardTpvs readCardTpv = cardTpvsRepository.findByCardTpvId(newCardPayoutsList.get(0).getCardPayoutTpvId());
		if ((readCardTpv==null)) 
			throw new EntityNotFoundException("->CardsService->createCardPayoutsList-> Card Tpv NOT Found"+newCardPayoutsList.get(0).getCardPayoutTpvId().toString());

		List<CardPayouts> createCardPayoutsList = new ArrayList<CardPayouts>();
		CardPayouts createdCardPayout;
		for(int i = 0; i<newCardPayoutsList.size(); i++)	{
//consoleLog.logMsg(showLogMsg,"->createCardPayoutsList",".."+i+"..Date..",newCardPayoutsList.get(i).getCardPayoutDate().toString());
//consoleLog.logMsg(showLogMsg,"->createCardPayoutsList",".."+i+"..OperationNr..",newCardPayoutsList.get(i).getCardPayoutOperationNr().toString());
//consoleLog.logMsg(showLogMsg,"->createCardPayoutsList",".."+i+"..Total..",".."+newCardPayoutsList.get(i).getCardPayoutTotal() );
//consoleLog.logMsg(showLogMsg,"->createCardPayoutsList",".."+i+"..TpvId..",newCardPayoutsList.get(i).getCardPayoutTpvId().toString());

			// Check Card Payout attributes
			if (newCardPayoutsList.get(i).checkCardPayoutAtributes() == false) 
				throw new EmptyFieldException("->CardsService->createCardPayoutsList-> Card Payout wrong Attributes.."+newCardPayoutsList.get(i).getCardPayoutTpvId().toString());	
			createdCardPayout = cardsPayoutsRepository.save(newCardPayoutsList.get(i));
			if (createdCardPayout == null) 
				throw new EntityAlreadyExistsException("->CardsService->createCardPayoutsList-> Card Payout Create Error.."+newCardPayoutsList.get(i).getCardPayoutTpvId().toString());	
			createCardPayoutsList.add(createdCardPayout);			
		}
		consoleLog.logMsg(showLogMsg,"->CardsService->createCardPayoutsList","Exiting","...................>>..");
		return createCardPayoutsList;
	}	
	// -------------------Cards Payouts----->updateBeforeAfterCardPayoutsListTotalId------------------------------------------------
	@Transactional
	public List<CardPayouts> updateBeforeAfterCardPayoutsListTotalId(List<CardPayouts> updateCardPayoutsList) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException  {
		consoleLog.logMsg(showLogMsg,"->CardsService->updateBeforeAfterCardPayoutsListTotalId","Entering","..>>...................");
		// Checking null new object or empty mandatory attributes  
		if ( (updateCardPayoutsList == null) | (updateCardPayoutsList.size() < 1) ) 
			throw new EmptyFieldException("->CardsService->updateBeforeAfterCardPayoutsListTotalId.1.->Null Object or Empty Mandatoy Parameters");
		// Check if card total id exist
		long selectedCardTotalId = updateCardPayoutsList.get(0).getCardPayoutTotalId();
		CardTotals readCardTotal = cardTotalsRepository.findByCardTotalId(selectedCardTotalId);
		if (readCardTotal == null)
			throw new EntityNotFoundException("->CardsService->updateBeforeAfterCardPayoutsListTotalId.1.->Not Found");
		// Set Card Total Id to null in the card payouts that had this card total id before updating
		List<CardPayouts> readCardPayoutsList = cardsPayoutsRepository.findByCardPayoutTotalId(selectedCardTotalId);				
		for (int j=0; j<readCardPayoutsList.size() ; j++) {
			CardPayouts readCardPayout = readCardPayoutsList.get(j);
			readCardPayout.setCardPayoutTotalId(null);
			cardsPayoutsRepository.save(readCardPayout);
		}
		// Update Card Payouts with received of payouts, having already set the card total id
		List<CardPayouts> updatedCardPayoutsList = new ArrayList<CardPayouts>();
		for (int j=0; j<updateCardPayoutsList.size() ; j++) {
			CardPayouts readCardPayout = updateCardPayoutsList.get(j);
			cardsPayoutsRepository.save(readCardPayout);
			updatedCardPayoutsList.add(readCardPayout);
		}
		consoleLog.logMsg(showLogMsg,"->CardsService->updateBeforeAfterCardPayoutsListTotalId","Exiting","...................>>..");
		return updatedCardPayoutsList;
	}
	// -------------------Cards Payouts----->delOneCardPayout------------------------------------------------
	@Transactional
	public void delOneCardPayout(CardPayouts delCardPayout) {
		consoleLog.logMsg(showLogMsg,"->CardsService->delOneCardPayout","Entering","..>>...................");
		/* Check if the field exist */
		cardsPayoutsRepository.deleteById(delCardPayout.getCardPayoutId());
		consoleLog.logMsg(showLogMsg,"->CardsService->delOneCardPayout","Exiting","...................>>..");
		return;
	}
	// -------------------Cards Payouts----->delAllPayoutLists------------------------------------------------
	@Transactional
	public void delAllPayoutLists(List<CardPayouts>  delCardPayoutslist) {
		consoleLog.logMsg(showLogMsg,"->CardsService->delAllPayoutLists","Entering","..>>...................");
		/* Check if the field exist */
		for(int i = 0; i<delCardPayoutslist.size(); i++)	{
			cardsPayoutsRepository.deleteById(delCardPayoutslist.get(i).getCardPayoutId());
		}
		consoleLog.logMsg(showLogMsg,"->CardsService->delAllPayoutLists","Exiting","...................>>..");
		return;
	}
	// -------------------Cards Payouts----->getAllCardPayoutsNotTotal----------------------------------------------------------	
	@Transactional
	public List<CardPayouts> getAllCardPayoutsNotTotal( List<ValueRecord> cardPayoutVRList) throws EmptyFieldException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->CardsService->getAllCardPayoutsNotTotal","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ( (cardPayoutVRList == null) | (cardPayoutVRList.size() == 0) ) 
			throw new EmptyFieldException("->CardsService->getAllCardPayoutsNotTotal.1.->Null Object or Empty Mandatoy Parameters");
		for (int j=0; j<cardPayoutVRList.size() ; j++) {
			if ( (cardPayoutVRList.get(j) == null) | (cardPayoutVRList.get(j).getValue().length() == 0) )
				throw new EmptyFieldException("->CardsService->getAllCardPayoutsNotTotal.2.->Null Object or Empty Mandatoy Parameters");
		}		
		// check if received card TPV exists 
		CardTpvs readCardTpv = null;
		if ( cardPayoutVRList.get(0).getValue().equals("ALL") == false ) {
			readCardTpv = cardTpvsRepository.findByCardTpvName(cardPayoutVRList.get(0).getValue());
			if (readCardTpv == null)
				throw new EntityNotFoundException("->CardsService->getAllCardPayoutsNotTotal.1.->Not Found"+cardPayoutVRList.get(0).getValue());
		}				
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		Date startDate,endDate;
		try {
			endDate = formatter.parse(cardPayoutVRList.get(2).getValue());
			// consoleLog.logMsg(showLogMsg,"->CardsService->getAllCardPayouts","..endDate........",endDate.toString());
			startDate = formatter.parse(cardPayoutVRList.get(1).getValue());
			// consoleLog.logMsg(showLogMsg,"->CardsService->getAllCardPayouts","..startDate..",startDate.toString());
		} catch (ParseException e) {
			throw new EmptyFieldException("->CardsService->getAllCardPayoutsNotTotal.1.->Wrong DATE Object ");
		}
		List<CardPayouts> readCardsList = null;
		if ( cardPayoutVRList.get(0).getValue().equals("ALL") == true )  {
			readCardsList = cardsPayoutsRepository.findByCardPayoutTotalIdAndCardPayoutDateGreaterThanEqualAndCardPayoutDateLessThanEqual(null,startDate,endDate);				
		} else {
			readCardsList = cardsPayoutsRepository.findByCardPayoutTotalIdAndCardPayoutTpvIdAndCardPayoutDateGreaterThanEqualAndCardPayoutDateLessThanEqual(null,readCardTpv.getCardTpvId(),startDate,endDate);				
		}
		consoleLog.logMsg(showLogMsg,"->CardsService->getAllCardPayoutsNotTotal","...tpv...",cardPayoutVRList.get(0).getValue());
		return readCardsList;
	}	
	// -------------------Cards Payouts----->getAllCardPayoutsWithCardTotalId----------------------------------------------------------	
	@Transactional
	public List<CardPayouts> getAllCardPayoutsWithCardTotalId( List<ValueRecord> cardPayoutVRList) throws EmptyFieldException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->CardsService->getAllCardPayoutsWithCardTotalId","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ( (cardPayoutVRList == null) | (cardPayoutVRList.size() == 0) ) 
			throw new EmptyFieldException("->CardsService->getAllCardPayoutsWithCardTotalId.1.->Null Object or Empty Mandatoy Parameters");
		for (int j=0; j<cardPayoutVRList.size() ; j++) {
			if ( (cardPayoutVRList.get(j) == null) | (cardPayoutVRList.get(j).getValue().length() == 0) )
				throw new EmptyFieldException("->CardsService->getAllCardPayoutsWithCardTotalId.2.->Null Object or Empty Mandatoy Parameters");
		}		
		// check if received card Total with received total id exists 
		CardTotals readCardTotal = null;
		long selCardTotalId = Long.parseLong(cardPayoutVRList.get(0).getValue());
		readCardTotal = cardTotalsRepository.findByCardTotalId(selCardTotalId);
		if (readCardTotal == null)
			throw new EntityNotFoundException("->CardsService->getAllCardPayoutsWithCardTotalId.1.->Not Found"+cardPayoutVRList.get(0).getValue());
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		Date startDate,endDate;
		try {
			endDate = formatter.parse(cardPayoutVRList.get(2).getValue());
			startDate = formatter.parse(cardPayoutVRList.get(1).getValue());

		} catch (ParseException e) {
			throw new EmptyFieldException("->CardsService->getAllCardPayoutsWithCardTotalId.1.->Wrong DATE Object ");
		}
		List<CardPayouts> readCardsList = null;
		readCardsList = cardsPayoutsRepository.findByCardPayoutTotalIdAndCardPayoutDateGreaterThanEqualAndCardPayoutDateLessThanEqual(selCardTotalId,startDate,endDate);				

		consoleLog.logMsg(showLogMsg,"->CardsService->getAllCardPayoutsWithCardTotalId","...card.total.id...",cardPayoutVRList.get(0).getValue());
		return readCardsList;
	}		
	// -------------------Cards Payouts----->getCardPayoutsNoTotal----------------------------------------------------------	
	@Transactional
	public List<CardPayouts> getCardPayoutsNoTotal( List<ValueRecord> cardPayoutVRList) throws EmptyFieldException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->CardsService->getCardPayoutsNoTotal","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ( (cardPayoutVRList == null) | (cardPayoutVRList.size() == 0) ) 
			throw new EmptyFieldException("->CardsService->getCardPayoutsNoTotal.1.->Null Object or Empty Mandatoy Parameters");
		for (int j=0; j<cardPayoutVRList.size() ; j++) {
			if ( (cardPayoutVRList.get(j) == null) | (cardPayoutVRList.get(j).getValue().length() == 0) )
				throw new EmptyFieldException("->CardsService->getCardPayoutsNoTotal.2.->Null Object or Empty Mandatoy Parameters");
		}		
		// check if received card TPV exists 
		CardTpvs readCardTpv = null;
		if ( cardPayoutVRList.get(0).getValue().equals("ALL") == false ) {
			readCardTpv = cardTpvsRepository.findByCardTpvName(cardPayoutVRList.get(0).getValue());
			if (readCardTpv == null)
				throw new EntityNotFoundException("->CardsService->getAllCardPayoutsNotTotal.1.->Not Found"+cardPayoutVRList.get(0).getValue());
		}	
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		Date startDate,endDate;
		try {
			endDate = formatter.parse(cardPayoutVRList.get(2).getValue());
			startDate = formatter.parse(cardPayoutVRList.get(1).getValue());

		} catch (ParseException e) {
			throw new EmptyFieldException("->CardsService->getCardPayoutsNoTotal.1.->Wrong DATE Object ");
		}
		// Reading all card payouts in the period having null total id
		List<CardPayouts> readCardsNoTotList = null;
		if ( cardPayoutVRList.get(0).getValue().equals("ALL") == true )  {
			readCardsNoTotList = cardsPayoutsRepository.findByCardPayoutTotalIdAndCardPayoutDateGreaterThanEqualAndCardPayoutDateLessThanEqual(null,startDate,endDate);				
		} else {
			readCardsNoTotList = cardsPayoutsRepository.findByCardPayoutTotalIdAndCardPayoutTpvIdAndCardPayoutDateGreaterThanEqualAndCardPayoutDateLessThanEqual(null,readCardTpv.getCardTpvId(),startDate,endDate);				
		}
		// Reading all card payouts in the period having total date greater than end of period
		List<CardPayouts> readCardsTotLaterList = null;
		if ( cardPayoutVRList.get(0).getValue().equals("ALL") == true )  {
			readCardsTotLaterList = cardsPayoutsRepository.findByCardPayoutDateGreaterThanEqualAndCardPayoutDateLessThanEqualAndCardPayoutTotalDateGreaterThan(startDate,endDate,endDate);				
		} else {
			readCardsTotLaterList = cardsPayoutsRepository.findByCardPayoutTpvIdAndCardPayoutDateGreaterThanEqualAndCardPayoutDateLessThanEqualAndCardPayoutTotalDateGreaterThan(readCardTpv.getCardTpvId(),startDate,endDate,endDate);				
		}
		readCardsNoTotList.addAll(readCardsTotLaterList);
		consoleLog.logMsg(showLogMsg,"->CardsService->getCardPayoutsNoTotal","Exiting","..>>...................");
		return readCardsNoTotList;
	}		
	// -------------------Cards Tpvs------------------------------------------------------------------------------------
	// -------------------Cards Tpvs----->getAllCardTpvs-------------------------------------------	
	@Transactional(readOnly = true)
	public List<CardTpvs> getAllCardTpvs() {
		consoleLog.logMsg(showLogMsg,"->CardsService->getAllCardTpvs","Entering","..>>...................");
		List<CardTpvs> CardTpvsList = cardTpvsRepository.findAll();
		consoleLog.logMsg(showLogMsg,"->CardsService->getAllCardTpvs","Exiting","...................>>..");
		return CardTpvsList;
	}
	// -------------------Cards Tpvs----->createOneCardTpv------------------------------------------------
	@Transactional
	public CardTpvs createOneCardTpv(CardTpvs newCardTpv) throws EmptyFieldException, EntityAlreadyExistsException {
		consoleLog.logMsg(showLogMsg,"->CardsService->createOneCardTpv","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((newCardTpv==null) | (newCardTpv.getCardTpvName()=="")) 
			throw new EmptyFieldException("->CardsService->createOneCardTpv->Null Object or Empty Mandatoy Parameters");
		/* Checking attributes of new object */
		if (newCardTpv.checkCardTpvAtributes()==false) 
			throw new WrongParametersException("--Card Name-->"+newCardTpv.getCardTpvName()+"--Card Type-->"+newCardTpv.getCardTpvType());
		/* Checking there no other object with same name */
		CardTpvs readCardTpv = cardTpvsRepository.findByCardTpvName(newCardTpv.getCardTpvName());
		if (readCardTpv != null) throw new EntityAlreadyExistsException("--Card Type Name-->"+readCardTpv.getCardTpvName());
		readCardTpv = cardTpvsRepository.save(newCardTpv);
		consoleLog.logMsg(showLogMsg,"->CardsService->createOneCardTpv","Exiting","...................>>..");
		return readCardTpv;
	}
	// -------------------Cards Tpvs----->getOneCardTpv------------------------------------------------
	@Transactional
	public CardTpvs getOneCardTpv(CardTpvs modCardTpv) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->CardsService->getOneCardTpv","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((modCardTpv==null) | (modCardTpv.getCardTpvName()=="")) throw new EmptyFieldException("->CardsService->getOneCardTpv->Null Object or Empty Mandatoy Parameters");	
		CardTpvs readCardTpv = cardTpvsRepository.findByCardTpvId(modCardTpv.getCardTpvId());
		if (readCardTpv == null) throw new EntityNotFoundException("--Prov Name-->"+modCardTpv.getCardTpvName());		
		consoleLog.logMsg(showLogMsg,"->CardsService->getOneCardTpv","Exiting","...................>>..");
		return readCardTpv;
	}
	// -------------------Cards Tpvs----->updateOneCardTpv------------------------------------------------
	@Transactional
	public CardTpvs updateOneCardTpv(CardTpvs modCardTpv) throws EmptyFieldException, EntityAlreadyExistsException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->CardsService->updateOneCardTpv","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((modCardTpv==null) | (modCardTpv.getCardTpvName()=="")) throw new EmptyFieldException("->CardsService->updateOneCardTpv->Null Object or Empty Mandatoy Parameters");	
		CardTpvs readCardTpv = cardTpvsRepository.findByCardTpvId(modCardTpv.getCardTpvId());
		if (readCardTpv == null) throw new EntityNotFoundException("--Prov Name-->"+modCardTpv.getCardTpvName());
		readCardTpv = cardTpvsRepository.save(modCardTpv);
		consoleLog.logMsg(showLogMsg,"->CardsService->updateOneCardTpv","Exiting","...................>>..");
		return readCardTpv;
	}
	// -------------------Cards Tpvs----->DeleteOneCardTpv------------------------------------------------
	@Transactional
	public void DeleteOneCardTpv(CardTpvs delCardTpv) {
		consoleLog.logMsg(showLogMsg,"->CardsService->DeleteOneCardTpv","Entering","..>>...................");
		/* Check if the field exist */
		cardTpvsRepository.deleteById(delCardTpv.getCardTpvId());
		consoleLog.logMsg(showLogMsg,"->CardsService->DeleteOneCardTpv","Exiting","...................>>..");
	}
	// -------------------Cards Tpvs----->DeleteAllCardTpvs------------------------------------------------
	@Transactional
	public void DeleteAllCardTpvs(CardTpvs delCardTpv) {
		consoleLog.logMsg(showLogMsg,"->CardsService->DeleteAllCardTpvs","Entering","..>>...................");
		cardTpvsRepository.deleteAll();
		consoleLog.logMsg(showLogMsg,"->CardsService->DeleteAllCardTpvs","Exiting","...................>>..");
	}
	// ------------------------------------INITIALIZE CARD TPVS DATE--------------------------------------------------------------	
	// -------------------Card Tpvs----->setCardTpvsCurrentDate---------------------------------------------------------------	
	@Transactional
	public void setCardTpvsCurrentDate() {
		consoleLog.logMsg(showLogMsg,"->CardsService->setCardTpvsCurrentDate","Entering","..>>...................");
		Date currentDate = new Date();
		double initialValue = 0;
		List<CardTpvs> cardTpvsList = cardTpvsRepository.findAll();
		for(int j=0; j<cardTpvsList.size(); j++) {
			cardTpvsList.get(j).setCardTpvComis1(initialValue);
			if (cardTpvsList.get(j).getCardTpvDate() == null) {
				cardTpvsList.get(j).setCardTpvDate(currentDate);								
			}
			cardTpvsRepository.save(cardTpvsList.get(j));
		}		
		consoleLog.logMsg(showLogMsg,"->CardsService->setCardTpvsCurrentDate","Exiting","...................>>..");
		return ;
	}
	// -------------------Cards Totals------------------------------------------------------------------------------------
	// -------------------Cards Totals----->getAllCardsTotals----------------------------------------------------------	
	@Transactional
	public List<CardTotals> getAllCardsTotals( List<ValueRecord> cardTotalVRList) throws EmptyFieldException, EntityNotFoundException {
		consoleLog.logMsg(showLogMsg,"->CardsService->getAllCardsTotals","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ( (cardTotalVRList == null) | (cardTotalVRList.size() == 0) ) 
			throw new EmptyFieldException("->CardsService->getAllCardsTotals.1.->Null Object or Empty Mandatoy Parameters");
		for (int j=0; j<cardTotalVRList.size() ; j++) {
			if ( (cardTotalVRList.get(j) == null) || (cardTotalVRList.get(j).getValue().length() == 0) )
				throw new EmptyFieldException("->CardsService->getAllCardsTotals.2.->Null Object or Empty Mandatoy Parameters");
			// consoleLog.logMsg(showLogMsg,"->cardsController->getAllCardPayouts","....",cardPayoutVRList.get(j).getValue());
		}		
		// check if received card TPV exists 
		CardTpvs readCardTpv = null;
		if ( cardTotalVRList.get(0).getValue().equals("ALL") == false ) {
			readCardTpv = cardTpvsRepository.findByCardTpvName(cardTotalVRList.get(0).getValue());
			if (readCardTpv == null)
				throw new EntityNotFoundException("->CardsService->getAllCardsTotals.1.->Not Found"+cardTotalVRList.get(0).getValue());
		}
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		Date startDate,endDate;
		try {
			endDate = formatter.parse(cardTotalVRList.get(2).getValue());
			startDate = formatter.parse(cardTotalVRList.get(1).getValue());
		} catch (ParseException e) {
			throw new EmptyFieldException("->CardsService->getAllCardsTotals.1.->Wrong DATE Object ");
		}
		List<CardTotals> readCardsTotalList = null;
		if ( cardTotalVRList.get(0).getValue().equals("ALL") == true )  {
			readCardsTotalList = cardTotalsRepository.findByCardTotalDateGreaterThanEqualAndCardTotalDateLessThanEqual(startDate,endDate);				
		} else {
			readCardsTotalList = cardTotalsRepository.findByCardTotalCardTpvIdAndCardTotalDateGreaterThanEqualAndCardTotalDateLessThanEqual(readCardTpv.getCardTpvId(),startDate,endDate);				
		}		
		consoleLog.logMsg(showLogMsg,"->CardsService->getAllCardsTotals","Exiting","...................>>..");
		return readCardsTotalList;
	}	
	// -------------------Cards Totals----->createOneCardTotal------------------------------------------------
	@Transactional
	public CardTotals createOneCardTotal(CardTotals newCardTotal) throws EmptyFieldException, EntityAlreadyExistsException {
		consoleLog.logMsg(showLogMsg,"->CardsService->createOneCardTotal","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((newCardTotal==null) | (newCardTotal.getCardTotalCardTpvName()=="")) 
			throw new EmptyFieldException("->CardsService->createOneCardTotal->Null Object or Empty Mandatoy Parameters");
		/* Checking attributes of new object */
		if (newCardTotal.checkCardTotalAtributes()==false) 
			throw new WrongParametersException("--Card Name-->"+newCardTotal.getCardTotalCardTpvName()+"--Card Bank-->"+newCardTotal.getCardTotalCardTpvBank());
		CardTotals readCardTotal;
		/* Checking there no other object with same name */		
		//readCardTotal = cardTpvsRepository.findByCardTpvName(newCardTotal.getCardTotalCardTpvName());
		//if (readCardTotal != null) throw new EntityAlreadyExistsException("--Card Type Name-->"+readCardTotal.getCardTotalCardTpvName());
		readCardTotal = cardTotalsRepository.save(newCardTotal);
		consoleLog.logMsg(showLogMsg,"->CardsService->createOneCardTotal","Exiting","...................>>..");
		return readCardTotal;
	}
	// -------------------Cards Totals----->updateOneCardTotal------------------------------------------------
	@Transactional
	public CardTotals updateOneCardTotal(CardTotals updCardTotal) throws EmptyFieldException, EntityAlreadyExistsException {
		consoleLog.logMsg(showLogMsg,"->CardsService->updateOneCardTotal","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((updCardTotal==null) | (updCardTotal.getCardTotalCardTpvName()=="")) 
			throw new EmptyFieldException("->CardsService->updateOneCardTotal->Null Object or Empty Mandatoy Parameters");
		/* Checking attributes of new object */
		if (updCardTotal.checkCardTotalAtributes()==false) 
			throw new WrongParametersException("--Card Name-->"+updCardTotal.getCardTotalCardTpvName()+"--Card Type-->"+updCardTotal.getCardTotalCardTpvBank());
		CardTotals readCardTotal;
		// Check that exists the card total to be updated
		readCardTotal = cardTotalsRepository.findByCardTotalId(updCardTotal.getCardTotalId());
		if (readCardTotal == null)
			throw new EntityNotFoundException("->CardsService->updateOneCardTotal.1.->Not Found"+updCardTotal.getCardTotalCardTpvName());
		readCardTotal = cardTotalsRepository.save(updCardTotal);
		consoleLog.logMsg(showLogMsg,"->CardsService->updateOneCardTotal","Exiting","...................>>..");
		return readCardTotal;
	}
	// -------------------Cards Totals----->getOneCardTotal------------------------------------------------
	@Transactional
	public CardTotals getOneCardTotal(CardTotals updCardTotal) throws EmptyFieldException, EntityAlreadyExistsException {
		consoleLog.logMsg(showLogMsg,"->CardsService->getOneCardTotal","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ((updCardTotal==null)) 
			throw new EmptyFieldException("->CardsService->getOneCardTotal->Null Object or Empty Mandatoy Parameters");
		CardTotals readCardTotal;
		// Check that exists the card total to be updated
		readCardTotal = cardTotalsRepository.findByCardTotalId(updCardTotal.getCardTotalId());
		if (readCardTotal == null)
			throw new EntityNotFoundException("->CardsService->getOneCardTotal.1.->Not Found"+updCardTotal.getCardTotalCardTpvName());
		consoleLog.logMsg(showLogMsg,"->CardsService->getOneCardTotal","Exiting","...................>>..");
		return readCardTotal;
	}
	// -------------------Cards Totals----->deleteOneCardTotal------------------------------------------------
	@Transactional
	public void deleteOneCardTotal(CardTotals delCardTotal) {
		consoleLog.logMsg(showLogMsg,"->CardsService->deleteOneCardTotal","Entering","..>>...................");
		/* Check if the field exist */
		// delete cardTotalId in all card payouts of this totalization, setting it to null
		List<CardPayouts> readCardPayoutsList = cardsPayoutsRepository.findByCardPayoutTotalId(delCardTotal.getCardTotalId());
		if (readCardPayoutsList != null) {
			for (int j=0; j<readCardPayoutsList.size() ; j++) {
				CardPayouts readCardPayout = readCardPayoutsList.get(j);
				readCardPayout.setCardPayoutTotalId(null);
				readCardPayout.setCardPayoutTotalDate(null);
				cardsPayoutsRepository.save(readCardPayout);
			}
		}		
		cardTotalsRepository.deleteById(delCardTotal.getCardTotalId());
		consoleLog.logMsg(showLogMsg,"->CardsService->deleteOneCardTotal","Exiting","...................>>..");
	}
	// -------------------Cards Totals----->deleteAllCardsTotals------------------------------------------------
	@Transactional
	public long deleteAllCardsTotals( List<ValueRecord> delCardTpvVRList) throws EmptyFieldException, EntityNotFoundException{
		consoleLog.logMsg(showLogMsg,"->CardsService->deleteAllCardsTotals","Entering","..>>...................");
		/* Checking null new object or empty mandatory attributes  */
		if ( (delCardTpvVRList == null) | (delCardTpvVRList.size() == 0) ) 
			throw new EmptyFieldException("->CardsService->deleteAllCardsTotals.1.->Null Object or Empty Mandatoy Parameters");
		for (int j=0; j<delCardTpvVRList.size() ; j++) {
			if ( (delCardTpvVRList.get(j) == null) || (delCardTpvVRList.get(j).getValue().length() == 0) )
				throw new EmptyFieldException("->CardsService->deleteAllCardsTotals.2.->Null Object or Empty Mandatoy Parameters");
		}		
		// check if received card TPV exists 
		CardTpvs readCardTpv = null;
		if ( delCardTpvVRList.get(0).getValue().equals("ALL") == false ) {
			readCardTpv = cardTpvsRepository.findByCardTpvName(delCardTpvVRList.get(0).getValue());
			if (readCardTpv == null)
				throw new EntityNotFoundException("->CardsService->deleteAllCardsTotals.1.->Not Found"+delCardTpvVRList.get(0).getValue());
		}
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		Date startDate,endDate;
		try {
			endDate = formatter.parse(delCardTpvVRList.get(2).getValue());
			startDate = formatter.parse(delCardTpvVRList.get(1).getValue());
		} catch (ParseException e) {
			throw new EmptyFieldException("->CardsService->deleteAllCardsTotals.1.->Wrong DATE Object ");
		}
		long delCardsTotals;
		delCardsTotals = 1;
		// Before all card payouts in this period and of this tpv have to have total id set to null
		/*
		if ( delCardTpvVRList.get(0).getValue().equals("ALL") == true )  {
			delCardsTotals = cardTotalsRepository.deleteByCardTotalDateStartGreaterThanEqualAndCardTotalDateEndLessThanEqual(startDate,endDate);				
		} else {
			delCardsTotals = cardTotalsRepository.deleteByCardTotalCardTpvIdAndCardTotalDateStartGreaterThanEqualAndCardTotalDateEndLessThanEqual(readCardTpv.getCardTpvId(),startDate,endDate);				
		}*/		
		consoleLog.logMsg(showLogMsg,"->CardsService->deleteAllCardsTotals","Exiting","...................>>..");
		return delCardsTotals;
	}
	// -------------------Cards Totals----->setCardPayoutsCardTotalIdToNull------------------------------------------------
	@Transactional
	public void setCardPayoutsCardTotalIdToNull() {
		consoleLog.logMsg(showLogMsg,"->CardsService->setCardPayoutsCardTotalIdToNull","Entering","..>>...................");

		List<CardPayouts> readCardPayoutsList = cardsPayoutsRepository.findAll();
		if (readCardPayoutsList != null) {
			for (int j=0; j<readCardPayoutsList.size() ; j++) {
				CardPayouts readCardPayout = readCardPayoutsList.get(j);
				readCardPayout.setCardPayoutTotalId(null);
				cardsPayoutsRepository.save(readCardPayout);
			}
		}		
		consoleLog.logMsg(showLogMsg,"->CardsService->setCardPayoutsCardTotalIdToNull","Exiting","...................>>..");
	}
}
