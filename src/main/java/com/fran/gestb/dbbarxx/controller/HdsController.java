package com.fran.gestb.dbbarxx.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fran.gestb.aacommon.ConsoleLog;
import com.fran.gestb.aaexceptions.EmptyFieldException;
import com.fran.gestb.aaexceptions.UserAlreadyExistsException;
import com.fran.gestb.dbbarxx.model.HdTpvs;
import com.fran.gestb.dbbarxx.model.HdTypes;
import com.fran.gestb.dbbarxx.model.Hds;
import com.fran.gestb.dbbarxx.service.HdsService;

@RestController
@RequestMapping("/hds")
public class HdsController {

	@Autowired
	private HdsService hdsService;
	@Autowired
	private ConsoleLog consoleLog; 
	private boolean showLogMsg = true;
	
	// -------------------HDS---------------------------------------------------------------
	// -------------------Hds----->GetListHds------------------------------------------------
	@RequestMapping(value="/getListHds", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public List<Hds> getListHd(@RequestBody List<Date> firstAndLastDayOfMonth) {
		consoleLog.logMsg(showLogMsg,"->HdsController->getListHd","Entering","..>>...................");
		List<Hds> hdsList = hdsService.getListHds(firstAndLastDayOfMonth);
		consoleLog.logMsg(showLogMsg,"->HdsController->getListHd","Exiting","...................>>..");
		return hdsList;
	}
	// -------------------Hds----->getHdsMonth------------------------------------------------
	@RequestMapping(value="/getHdsMonth", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public List<Hds> getHdsMonth(@RequestBody List<Date> firstAndLastDayOfMonth) {
		consoleLog.logMsg(showLogMsg,"->HdsController->getHdsMonth","Entering","..>>...................");
		List<Hds> hdsList = hdsService.getHdsMonth(firstAndLastDayOfMonth);
		consoleLog.logMsg(showLogMsg,"->HdsController->getHdsMonth","Exiting","...................>>..");
		return hdsList;
	}
	// -------------------Hds----->GetDateHds------------------------------------------------
	@RequestMapping(value="/getDateHds", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public List<Hds> getDateHd(@RequestBody Date workingDate) {
		consoleLog.logMsg(showLogMsg,"->HdsController->getDateHd","Entering","..>>...................");
		List<Hds> hdsList = hdsService.getDateHds(workingDate);
		consoleLog.logMsg(showLogMsg,"->HdsController->getDateHd","Exiting","...................>>..");
		return hdsList;
	}
	// -------------------Hds----->createSumHd------------------------------------------------
	@RequestMapping(value = "/createSumHd", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public Hds createSumHd(@RequestBody Hds sumHd) {
		consoleLog.logMsg(showLogMsg,"->HdsController->createSumHd","Entering","..>>...................");
		Hds checkHd = hdsService.createSumHd(sumHd);
		consoleLog.logMsg(showLogMsg,"->HdsController->createSumHd","Exiting","...................>>..");
		return checkHd;	
	}
	// -------------------Hds----->createDateHds------------------------------------------------
	@RequestMapping(value = "/createDateHds", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public List<Hds> createDateHds(@RequestBody List<Hds> dateHds) {
		consoleLog.logMsg(showLogMsg,"->HdsController->createDateHds","Entering","..>>...................");
		List<Hds> savedHds = hdsService.createDateHds(dateHds);
		consoleLog.logMsg(showLogMsg,"->HdsController->createDateHds","Exiting","...................>>..");
		return savedHds;	
	}
	// -------------------Hds----->updateDateHds------------------------------------------------
	@RequestMapping(value = "/updateDateHds", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public List<Hds>  updateDateHds(@RequestBody List<Hds> dateHds) {
		consoleLog.logMsg(showLogMsg,"->HdsController->updateDateHds","Entering","..>>...................");
		List<Hds> savedHds = hdsService.updateDateHds(dateHds);
		consoleLog.logMsg(showLogMsg,"->HdsController->updateDateHds","Exiting","...................>>..");
		return savedHds;	
	}
	// -------------------Hds----->deleteOneSumHd------------------------------------------------
	@RequestMapping(value="/deleteOneSumHd", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public long deleteOneSumHd(@RequestBody Hds delSumHd) {
		consoleLog.logMsg(showLogMsg,"->HdsController->deleteOneSumHd","Entering","..>>...................");
		long deleteResult = hdsService.deleteOneSumHd(delSumHd);
		consoleLog.logMsg(showLogMsg,"->HdsController->deleteOneSumHd","Exiting","...................>>..");
		return deleteResult;
	}
	// -------------------Hds----->deleteAllSumHd------------------------------------------------
	@RequestMapping(value = "/deleteAllSumHd", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public long  deleteAllSumHds(@RequestBody List<Hds> delSumHds) {
		consoleLog.logMsg(showLogMsg,"->HdsController->deleteAllSumHds","Entering","..>>...................");
		long deletedSumHds = hdsService.deleteAllSumHds(delSumHds);
		consoleLog.logMsg(showLogMsg,"->HdsController->deleteAllSumHds","Exiting","...................>>..");
		return deletedSumHds;	
	}
	// -------------------Hds----->DeleteDateHds------------------------------------------------
	@RequestMapping(value="/deleteDateHds", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public long deleteDateHds(@RequestBody Date workingDate) {
		consoleLog.logMsg(showLogMsg,"->HdsController->deleteDateHd","Entering","..>>...................");
		long deleteResult = hdsService.deleteDateHds(workingDate);
		consoleLog.logMsg(showLogMsg,"->HdsController->deleteDateHd","Exiting","...................>>..");
		return deleteResult;
	}
	// -------------------Hds----->deleteMonthHds------------------------------------------------
	@RequestMapping(value="/deleteMonthHds", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public long deleteMonthHds(@RequestBody List<Date> firstAndLastDayOfMonth) {
		consoleLog.logMsg(showLogMsg,"->HdsController->deleteMonthHds","Entering","..>>...................");		
		long deleteResult = hdsService.deleteMonthHds(firstAndLastDayOfMonth);
		consoleLog.logMsg(showLogMsg,"->HdsController->deleteMonthHds","Exiting","...................>>..");
		return deleteResult;
	}
	// -------------------HD TPVS----------------------------------------------------------------------
	
	// -------------------Hds----->getDateHdTpvs------------------------------------------------
	@RequestMapping(value="/getDateHdTpvs", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public List<HdTpvs> getDateHdTpvs(@RequestBody Date workingDate) {
		consoleLog.logMsg(showLogMsg,"->HdsController->getDateHdTpvs","Entering","..>>...................");
		List<HdTpvs> hdTpvsList = hdsService.getDateHdTpvs(workingDate);
		consoleLog.logMsg(showLogMsg,"->HdsController->getDateHdTpvs","Exiting","...................>>..");
		return hdTpvsList;
	}
	// -------------------Hds----->getHdTpvsMonth------------------------------------------------
	@RequestMapping(value="/getHdTpvsMonth", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public List<HdTpvs> getHdTpvsMonth(@RequestBody List<Date> firstAndLastDayOfMonth) {
		consoleLog.logMsg(showLogMsg,"->HdsController->getHdTpvsMonth","Entering","..>>...................");
		List<HdTpvs> hdTpvsList = hdsService.getHdTpvsMonth(firstAndLastDayOfMonth);
		consoleLog.logMsg(showLogMsg,"->HdsController->getHdTpvsMonth","Exiting","...................>>..");
		return hdTpvsList;
	}
	// -------------------HdTpvs----->createDateHdTpvs------------------------------------------------
	@RequestMapping(value = "/createDateHdTpvs", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public List<HdTpvs> createDateHdTpvs(@RequestBody List<HdTpvs> dateHdTpvs) {
		consoleLog.logMsg(showLogMsg,"->HdsController->createDateHdTpvs","Entering","..>>...................");
		List<HdTpvs> savedHdTpvs = hdsService.createDateHdTpvs(dateHdTpvs);
		consoleLog.logMsg(showLogMsg,"->HdsController->createDateHdTpvs","Exiting","...................>>..");
		return savedHdTpvs;	
	}
	// -------------------HdTpvs----->updateDateHdTpvs------------------------------------------------
	@RequestMapping(value = "/updateDateHdTpvs", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public List<HdTpvs>  updateDateHdTpvs(@RequestBody List<HdTpvs> dateHdTpvs) {
		List<HdTpvs>  saveResult;
		consoleLog.logMsg(showLogMsg,"->HdsController->updateDateHdTpvs","Entering","..>>...................");
		saveResult = hdsService.updateDateHdTpvs(dateHdTpvs);
		consoleLog.logMsg(showLogMsg,"->HdsController->updateDateHdTpvs","Exiting","...................>>..");
		return saveResult;	
	}
	// -------------------Hds----->previousDateHdTpvs------------------------------------------------
	@RequestMapping(value="/previousDateHdTpvs", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public List<HdTpvs> readDataPreviousDate(@RequestBody Date workingDate) {
		consoleLog.logMsg(showLogMsg,"->HdsController->readDataPreviousDate","Entering","..>>...................");
		List<HdTpvs> hdTpvsList = hdsService.readDataPreviousDate(workingDate);
		consoleLog.logMsg(showLogMsg,"->HdsController->readDataPreviousDate","Exiting","...................>>..");
		return hdTpvsList;
	}
	// -------------------HD TYPES----------------------------------------------------------------------
	// -------------------HdTypes----->getAllHdTypes-------------------------------------------
	@RequestMapping(value = "/getAllHdTypes", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public List<HdTypes> getAllHdTypes() throws UserAlreadyExistsException, EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->HdsController->getAllHdTypes","Entering","..>>...................");
		List<HdTypes> hdTypesList = hdsService.getAllHdTypes();
		if (hdTypesList == null) throw new EmptyFieldException("No allHdTypes found");
		consoleLog.logMsg(showLogMsg,"->HdsController->getAllHdTypes","Exiting","...................>>..");
		return hdTypesList; 
	}
	// -------------------HdTypes----->createBarHdTypes-------------------------------------------
	@RequestMapping(value = "/createBarHdTypes", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public List<HdTypes> createBarHdTypes(@RequestBody String currentBar) {
		consoleLog.logMsg(showLogMsg,"->HdsController->createBarHdTypes","Entering","..>>...................");
		List<HdTypes> hdTypesList = hdsService.createBarHdTypes(currentBar);
		consoleLog.logMsg(showLogMsg,"->HdsController->createBarHdTypes","Exiting","...................>>..");
		return hdTypesList; 
	}
	// -------------------HdTypes----->getOneHdType------------------------------------------------
	@RequestMapping(value="/getOneHdType", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public HdTypes getOneHdType(@RequestBody HdTypes modHdType) {
		consoleLog.logMsg(showLogMsg,"->HdsController->getOneHdType","Entering","..>>...................");
		HdTypes readhdType = hdsService.getOneHdType(modHdType);
		consoleLog.logMsg(showLogMsg,"->HdsController->getOneHdType","Exiting","...................>>..");
		return readhdType;
	}
	// -------------------HdTypes----->createOneHdType------------------------------------------------
	@RequestMapping(value = "/createOneHdType", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public HdTypes createOneHdType(@RequestBody HdTypes newHdType) {
		consoleLog.logMsg(showLogMsg,"->HdsController->createOneHdType","Entering","..>>...................");
		HdTypes checkHdType = hdsService.createOneHdType(newHdType);
		consoleLog.logMsg(showLogMsg,"->HdsController->createOneHdType","Exiting","...................>>..");
		return checkHdType;	
	}
	// -------------------HdTypes----->updateOneHdType------------------------------------------------
	@RequestMapping(value = "/updOneHdType", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public HdTypes updateOneHdType(@RequestBody HdTypes updHdType) throws EmptyFieldException {
		consoleLog.logMsg(showLogMsg,"->HdsController->updateOneHdType","Entering","..>>...................");
		// Check mandatory parameters in newField
		if ((updHdType.getHdTypeName()=="") | (updHdType==null)) throw new EmptyFieldException("Wrong Field parameters");	
		HdTypes checkHdType = hdsService.updateOneHdType(updHdType);
		consoleLog.logMsg(showLogMsg,"->HdsController->updateOneHdType","Exiting","...................>>..");
		return checkHdType;		
	}
	// -------------------HdTypes----->delOneHdType------------------------------------------------
	@RequestMapping(value="/delOneHdType", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public void delOneHdType(@RequestBody HdTypes delHdType) {
		consoleLog.logMsg(showLogMsg,"->HdsController->delOneHdType","Entering","..>>...................");
		hdsService.deleteOneHdType(delHdType);
		consoleLog.logMsg(showLogMsg,"->HdsController->delOneHdType","Exiting","...................>>..");
		return;
	}
	// -------------------HdTypes----->delAllHdTypes------------------------------------------------
	@RequestMapping(value="/delAllHdTypes", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)	
	public void delAllHdTypes() {
		consoleLog.logMsg(showLogMsg,"->HdsController->delAllHdTypes","Entering","..>>...................");
		hdsService.deleteAllHdTypes();
		consoleLog.logMsg(showLogMsg,"->HdsController->delAllHdTypes","Exiting","...................>>..");
		return;
	}
	
}
