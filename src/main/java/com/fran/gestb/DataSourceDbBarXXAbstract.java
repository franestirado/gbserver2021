package com.fran.gestb;

import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import javax.sql.DataSource;
import java.util.HashMap;

public abstract class DataSourceDbBarXXAbstract {
    protected LocalContainerEntityManagerFactoryBean entityManager(DataSource dataSource, 
    													String packagesToScan, String validate) {
    	//System.out.printf("--->DataSourceDbBarXXAbstract-------->entityManager-------->\n");    
    	
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource);
        em.setPackagesToScan(packagesToScan);
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setGenerateDdl(true);
        em.setJpaVendorAdapter(vendorAdapter);
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", validate);
        //properties.put("hibernate.hbm2ddl.auto", "create");
        //#spring.jpa.generate-ddl=true
        //properties.put("spring.jpa.hibernate.ddl-auto","create");

        properties.put("hibernate.physical_naming_strategy", "org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy");
        //properties.put("hibernate.temp.use_jdbc_metadata_defaults", "false");
        //properties.put("hibernate.jdbc.lob.non_contextual_creation", "false");
        //properties.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQL95Dialect");
        em.setJpaPropertyMap(properties);
        return em;
    }

    protected PlatformTransactionManager transactionManager(LocalContainerEntityManagerFactoryBean entityManager) {
    	//System.out.printf("--->DataSourceDbBarXXAbstract-------->transactionManager-------->\n");    	

        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManager.getObject());
        return transactionManager;
    }
}
