package com.fran.gestb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import com.fran.gestb.config.model.Bares;
import com.fran.gestb.config.repo.BaresRepository;
import com.fran.gestb.config.service.ConfigService;

@Component
public class CommandLineAppStartupRunner implements CommandLineRunner {

	@Autowired
	private BaresRepository baresRepository;
	@Autowired
	private ConfigService configService;


	@Override
    public void run(String...args) throws Exception {

		System.out.printf("<<<<<<<<<<<<...........CommandLineAppStartupRunner.......Starting.......>>>>>>>>>>\n");    	

		System.out.printf("<<<<<<<<<<<<...........CommandLineAppStartupRunner.......DB Connections.......>>>>>>>>>>\n");    	
    	configService.setBaresDbConnections();
    	 
		System.out.printf("<<<<<<<<<<<<...........CommandLineAppStartupRunner.......working BAR.......>>>>>>>>>>\n");    	
    	Bares barEsgo = baresRepository.findByBarName("RUBI");
		configService.changeWorkingBar(barEsgo);
		
/*
		System.out.printf("<<<<<<<<<<<<...........CommandLineAppStartupRunner.......Create Fields and Values.......>>>>>>>>>>\n");    	
		fieldsService.startCreateFieldsAndValues(); */
		/*
    	Bares barRubi = baresRepository.findByBarName("RUBI");
    	Bares barEsgo1 = baresRepository.findByBarName("ESGO");
    	for(int indice = 0;indice<20;indice++) {
    		configService.changeWorkingBar(barEsgo1);
    		configService.changeWorkingBar(barRubi);    	
    	}*/
    	
    	System.out.printf("<<<<<<<<<<<<...........CommandLineAppStartupRunner.......Exiting............>>>>>>>>>>\n"); 
    	//dbMetadata.getMetadata("jdbc:postgresql://localhost:5432/baresdatabase","baresdatabase","baresadmuser","Qhucct01!");
    }  
}
