package com.fran.gestb;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import org.hibernate.mapping.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
//import com.zaxxer.hikari.HikariConfig;
//import com.zaxxer.hikari.HikariDataSource;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

//import static java.lang.String.format;

@Slf4j
@Configuration
public class DSourceDbBarXXMultiTenantManager {

	@Autowired
	private Environment env;
	
	private final ThreadLocal<String> currentTenant = new ThreadLocal<>();
	private final Map<Object, Object> tenantDataSources = new ConcurrentHashMap<>();
	private final DataSourceProperties properties;

	private Function<String, DataSourceProperties> tenantResolver;

	private AbstractRoutingDataSource multiTenantDataSource;

	@Getter private DataSource defaultDatasource;

	private boolean showLogMsg = false;
	private String currentTennantName = "RUBI";
	
	public DSourceDbBarXXMultiTenantManager(DataSourceProperties properties) {
		this.logMsg(showLogMsg,"..DSourceDbBarXXMultiTenantManager..","....Constructor....","....>>>>>....");
		this.properties = properties;
	}

    @Primary
    @Bean
    DataSource tenantDataSource() {
        this.logMsg(showLogMsg, "..DSourceDbBarXXMultiTenantManager..", "....tenantDataSource....", "....>>>>>....");
        multiTenantDataSource = new AbstractRoutingDataSource() {

            @Override
            protected Object determineCurrentLookupKey() {
                //System.out.printf("..DSourceDbBarXXMultiTenantManager......determineCurrentLookupKey........>>>>>....\n");  
                String valueCurrentTenant = currentTenant.get();
                //System.out.printf("....................valueCurrentTenant................."+valueCurrentTenant+".........\n");
                //return currentTenant.get();
                String currentTenantValue = currentTennantName;
                //System.out.printf("....................currentTenantValue................."+currentTenantValue+".........\n");
                return currentTenantValue;
            }
        };
        multiTenantDataSource.setTargetDataSources(tenantDataSources);
        defaultDatasource = defaultDataSource();
        multiTenantDataSource.setDefaultTargetDataSource(defaultDatasource);
        multiTenantDataSource.afterPropertiesSet();
        return multiTenantDataSource;
    }
	public void setTenantResolver(Function<String, DataSourceProperties> tenantResolver) {
		this.logMsg(showLogMsg,"..DSourceDbBarXXMultiTenantManager..","....setTenantResolver....","....>>>>>....");
		this.tenantResolver = tenantResolver;
	}
	public void setCurrentTenant(String tenantId) throws SQLException, TenantNotFoundException, TenantResolvingException {
		this.logMsg(showLogMsg,"..DSourceDbBarXXMultiTenantManager..","....setCurrentTenant....","....>>>>>....");
		this.logMsg(showLogMsg,"..DSourceDbBarXXMultiTenantManager..","....tenantId....",tenantId+"....>>>>>....");
		if (tenantIsAbsent(tenantId)) {
			if (tenantResolver != null) {
				DataSourceProperties properties;
				try {
					properties = tenantResolver.apply(tenantId);
					this.logMsg(showLogMsg,"..DSourceDbBarXXMultiTenantManager..","....Datasource properties resolved for....",tenantId+"....>>>>>....");
				} catch (Exception e) {
					throw new TenantResolvingException(e, "Could not resolve the tenant!");
				}
				String url = properties.getUrl();
				String username = properties.getUsername();
				String password = properties.getPassword();

				addTenant(tenantId, url, username, password);
			} else {
				throw new TenantNotFoundException("Tenant not found! ---> "+tenantId);
			}
		}
		currentTenant.set(tenantId);
		currentTennantName = tenantId;
		//HikariDataSource newBarDataSource = (HikariDataSource) tenantDataSources.get(tenantId);
		/*
		DataSource newBarDataSource = (DataSource) tenantDataSources.get(tenantId);
		if(newBarDataSource.getConnection() == null)
			this.logMsg(showLogMsg,"..DSourceDbBarXXMultiTenantManager..","....setCurrentTenant....","CONNECTION IS CLOSED....>>>>>....");
		multiTenantDataSource.setTargetDataSources(tenantDataSources);
		multiTenantDataSource.setDefaultTargetDataSource(newBarDataSource);
		multiTenantDataSource.afterPropertiesSet();
		*/
		this.logMsg(showLogMsg,"..DSourceDbBarXXMultiTenantManager..","....setCurrentTenant....","SET==>"+tenantId+"....>>>>>....");
	}

	public void addTenant(String tenantId, String url, String username, String password)  {
		this.logMsg(showLogMsg,"..DSourceDbBarXXMultiTenantManager..","....addTenant....","....>>>>>....");
		this.logMsg(showLogMsg,"..DSourceDbBarXXMultiTenantManager..","....addTenant....",tenantId+"....>>>>>....");
		DataSource dataSource = DataSourceBuilder.create()
				.driverClassName(properties.getDriverClassName())
				.url(url)
				.username(username)
				.password(password)
				.build();
		// Check that new connection is 'live'. If not - throw exception
		try(Connection c = dataSource.getConnection()) {
			tenantDataSources.put(tenantId, dataSource);
			multiTenantDataSource.setTargetDataSources(tenantDataSources);
			multiTenantDataSource.afterPropertiesSet();
			currentTenant.set(tenantId);
			currentTennantName = tenantId;
			this.logMsg(showLogMsg,"..DSourceDbBarXXMultiTenantManager..","....addTenant....","....DataSource added for...."+tenantId);    	
			//multiTenantDataSource.setTargetDataSources(tenantDataSources);
			//multiTenantDataSource.setDefaultTargetDataSource(dataSource);
			//multiTenantDataSource.afterPropertiesSet();			
		}catch (Exception e) {
			this.logMsg(showLogMsg,"..DSourceDbBarXXMultiTenantManager..","....addTenant....","....Connection.....>>>>>....SQLexception........");
		}
	}
	public DataSource removeTenant(String tenantId) throws SQLException {
		this.logMsg(showLogMsg,"..DSourceDbBarXXMultiTenantManager..","....removeTenant....","....>>>>>....");
		Object removedDataSource = tenantDataSources.remove(tenantId);
		multiTenantDataSource.afterPropertiesSet();
		return (DataSource) removedDataSource;
	}
	public boolean tenantIsAbsent(String tenantId) {
		this.logMsg(showLogMsg,"..DSourceDbBarXXMultiTenantManager..","....tenantIsAbsent....","....>>>>>....");
		return !tenantDataSources.containsKey(tenantId);
	}
	public Collection<Object> getTenantList() {
		this.logMsg(showLogMsg,"..DSourceDbBarXXMultiTenantManager..","....getTenantList....","....>>>>>....");
		return tenantDataSources.keySet();
	}
	private DataSource defaultDataSource() {
		this.logMsg(showLogMsg,"..DSourceDbBarXXMultiTenantManager..","....defaultDataSource....","....>>>>>....");
        /*
		HikariConfig configHikari = new HikariConfig();
        configHikari.setUsername(env.getProperty("dbbardefault.datasource.username"));
        configHikari.setPassword(env.getProperty("dbbardefault.datasource.password"));
        configHikari.setJdbcUrl(env.getProperty("dbbardefault.datasource.url"));
        configHikari.setDriverClassName(env.getProperty("dbbardefault.datasource.driver-class-name"));
        HikariDataSource dataSource = new HikariDataSource(configHikari);
        return dataSource;*/
        
		DriverManagerDataSource defaultDataSource = new DriverManagerDataSource();
		defaultDataSource.setUrl(env.getProperty("dbbardefault.datasource.url"));
		defaultDataSource.setUsername(env.getProperty("dbbardefault.datasource.username"));
		defaultDataSource.setPassword(env.getProperty("dbbardefault.datasource.password"));
		defaultDataSource.setDriverClassName(env.getProperty("dbbardefault.datasource.driver-class-name"));		
		return defaultDataSource;		
		/*
		DriverManagerDataSource defaultDataSource = new DriverManagerDataSource();
		defaultDataSource.setDriverClassName(properties.getDriverClassName());
		defaultDataSource.setUrl(properties.getUrl());
		defaultDataSource.setUsername(properties.getUsername());
		defaultDataSource.setPassword(properties.getPassword());
		return defaultDataSource;
		*/
	}
	public void logMsg (boolean showMsg, String msg1, String msg2, String msg3) {
		if (showMsg == true) {
			System.out.printf(msg1 + "-->" + msg2 + "-->" + msg3 + "-->\n");
		}
	}
}
