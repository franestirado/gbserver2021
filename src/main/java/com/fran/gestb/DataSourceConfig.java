package com.fran.gestb;

import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "configEntityManagerFactory", transactionManagerRef = "configTransactionManager", 
basePackages = { "com.fran.gestb.config.repo" })

public class DataSourceConfig {

	@Autowired
	private Environment env;
	
	@Bean(name = "configDataSource")
	public DataSource configDatasource() {
	    //System.out.printf("--->DataSourceConfig-------->configDataSource-------->\n");    	

		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setUrl(env.getProperty("config.datasource.url"));
		dataSource.setUsername(env.getProperty("config.datasource.username"));
		dataSource.setPassword(env.getProperty("config.datasource.password"));
		dataSource.setDriverClassName(env.getProperty("config.datasource.driver-class-name"));		
		return dataSource;
	}	

	//@Primary
	@Bean(name = "configEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean configEntityManagerFactory() {
		//System.out.printf("--->DataSourceConfig-------->configEntityManagerFactory-------->\n");    	

		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(configDatasource());
		em.setPackagesToScan("com.fran.gestb.config.model");
		
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		
		Map<String, Object> properties = new HashMap<>();
		properties.put("hibernate.hbm2ddl.auto", env.getProperty("config.jpa.hibernate.ddl-auto"));
        properties.put("hibernate.physical_naming_strategy", env.getProperty("config.jpa.properties.hibernate.physical_naming_strategy"));
		//properties.put("hibernate.show-sql", env.getProperty("config.jpa.show-sql"));
		//properties.put("hibernate.dialect", env.getProperty("config.jpa.database-platform"));
		
		em.setJpaPropertyMap(properties);
		
		return em;
		
	}
	
	//@Primary
	@Bean(name = "configTransactionManager")
	public PlatformTransactionManager configTransactionManager() {
		//System.out.printf("--->DataSourceConfig-------->configTransactionManager-------->\n");    	

		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(configEntityManagerFactory().getObject());
		
		return transactionManager;
	}
	
}
