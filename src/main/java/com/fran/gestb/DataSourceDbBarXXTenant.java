package com.fran.gestb;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import javax.sql.DataSource;


@Configuration
@EnableJpaRepositories(
        basePackages = "com.fran.gestb.dbbarxx.repo",
        entityManagerFactoryRef = "dbbarxxEntityManager",
        transactionManagerRef = "dbbarxxTransactionManager"
)
public class DataSourceDbBarXXTenant extends DataSourceDbBarXXAbstract {

    @Primary
    @Bean("dbbarxxEntityManager")
    public LocalContainerEntityManagerFactoryBean dbbarxxEntityManager(DataSource tenantDataSource) {
    	//System.out.printf("--->DataSourceDbbarXXTenant-------->dbbarxxEntityManager-------->\n");    

        return entityManager(tenantDataSource, "com.fran.gestb.dbbarxx.model", "update");
    }

    @Primary
    @Bean
    public PlatformTransactionManager dbbarxxTransactionManager(@Qualifier("dbbarxxEntityManager") 
    							LocalContainerEntityManagerFactoryBean dbbarxxEntityManager) {
    	//System.out.printf("--->DataSourceDbbarXXTenant-------->dbbarxxTransactionManager-------->\n");    

        return transactionManager(dbbarxxEntityManager);
    }
}
