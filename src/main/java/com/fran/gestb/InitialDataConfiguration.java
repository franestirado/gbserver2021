package com.fran.gestb;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
	public class InitialDataConfiguration {

	    @Bean
	    CommandLineRunner runner() {
	        return args -> {
	        	System.out.printf("<<<<<<<<<<<<...........InitialDataConfiguration.........Testing.............>>>>>>>>>>\n");
	        };
	    }
	}

