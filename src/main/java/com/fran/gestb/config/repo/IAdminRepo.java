package com.fran.gestb.config.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fran.gestb.config.model.Admin;

public interface IAdminRepo extends JpaRepository<Admin, Integer>{

}
